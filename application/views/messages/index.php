<?php
if($messageable) {
    //if the menu items are set start the autocomplete loop
    $autoCompleteArray = '<script>var autoCompleteData = [';

    //for each user this person is following create a JS array elemment
    foreach ($messageable['users'] as $user) {
        $autoCompleteArray .= '{ value: "' . htmlentities($user->followingUser) . '", label: " ' . htmlentities($user->firstName) . ' '. htmlentities($user->lastName) .'", photo: "<img src=\'/user/profileimg/50/' . htmlentities($user->followingUser) .'\' />", co: "0"},';
    }

    //for each menu items start loooping over them to add them to the script
    foreach ($messageable['dispensories'] as $dispensorie) {
        $autoCompleteArray .= '{ value: "' . htmlentities($dispensorie->id) . '", label: "' . htmlentities($dispensorie->name) . '", photo: "<img src=\'/public/images/gst_no_profile.jpg\' width=\'50\' height=\'50\' />", co: "1"},';
    }

    //remove the last comma, if we dont it will break the js
    $autoCompleteArray = substr($autoCompleteArray, 0, -1);

    //close out the JS
    $autoCompleteArray .= '];</script>';

    //output the auto complete string
    echo $autoCompleteArray;

}
?>
<script src="public/js/chat.js" ></script>
<script>$(function () { $('[data-toggle="tooltip"]').tooltip() })</script>


<div class="container">
    <div class="row">

        <!--left bar start -->
        <div class="col-md-4">

            <div class="war">
                <div class="inbox">INBOX</div>
                <a href="#" class="compose_butt">compose</a>
                <div class="left_search">
                    <input name="" type="text" size="29" id="userSearch"/>
                </div>
				
				<span class="chatlist">
					<!--box_inner start-->
					<?php foreach($chat_list as $thread): ?>
					<div class="box_inner" data-chat-id="<?php echo $thread->chat_id; ?>">
						<div class="icon_box-1">
							<img src="<?php echo $thread->profileimg; ?>" width="50" height="50"/>
						</div>

							<div class="message_left">
								<div class="inbox_title"><a href="#" data-chat_id="<?php echo $thread->chat_id; ?>" data-chat-u-id="<?php echo $thread->user_id;?>" class="chat_thread">
                                        <?php
                                            //if this is a company name. We will only print out the first 2 words in its title. If its not then print the entire line

                                            $name = ucfirst($thread->partner_first_name);
                                            $arr = explode(' ', trim($name));
                                            if(count($arr) > 1){
                                                echo $arr[0].' '. $arr[1];
                                            } else {
                                                echo $name;
                                            }

                                        echo ' '.ucfirst($thread->partner_last_name).' ';

                                        //if the thread user_id does not equal the current users id then they are a company. Display the company icon
                                        if($thread->user_id != $this->session->userdata('userid')) {

                                            $nameCo = ucfirst($thread->coName);
                                            $arr = explode(' ', trim($nameCo));
                                            if(count($arr) > 1){
                                                $co = $arr[0].' '. $arr[1];
                                            } else {
                                                $co = $nameCo;
                                            }

                                            echo '&nbsp;<i class="fa fa-building" id="company_message" data-toggle="tooltip" data-placement="bottom" title="You are messaging on behalf of '.$co.'"></i>';
                                        }
                                        ?>

                                    </a></div>
								<div class="inbox_date" data-chat-id-time="<?php echo $thread->chat_id; ?>"><?php echo date("n/j g:ia", strtotime($thread->updated_time)); ?></div>
								<div class="clear"></div>
								<div class="message_text active" data-chat-id-message="<?php echo $thread->chat_id; ?>">
									<?php
										//if the last post is longer than 20 characters, cut it and ad ... to the end
										$final_message = (strlen($thread->last_message) > 20) ? substr($thread->last_message, 0, 35) . ' ...' : $thread->last_message;

										//echo it on the page
										echo $final_message;

                                        //if this is a new message and not from the current user show a dot
                                        if($thread->recieved == '0' && $thread->last_message_sender != $this->session->userdata('userid')) {
                                            echo '<i class="fa fa-circle" id="new_message" data-toggle="tooltip" data-placement="bottom" title="New Message"></i>';
                                        }

									?>
								</div>
							</div>

					</div>
					<?php endforeach ?>
					<!--box_inner end-->
				</span>
            </div>

        </div>
        <!-- left bar end -->

        <!-- right bar start -->
        <div class="col-md-8">
            <div class="war" id="chatarea">
                <h2 class="name_headline"><span class="toName"><?php echo $load_name; ?></span> <span id="newName">To: <input type="text" id="newMessageTo" value=""></span></h2>

                <div id="chatPanel">
                    <?php foreach($messages as $message): ?>
                    <!--message_mid start-->
                    <div class="message_mid">
                        <div class="icon_box-1">
                                            <span class="fa-stack fa-2x">
                                                   <img src="<?php echo $message->profileImg; ?>" width="50" height="50"/>
                                            </span>
                        </div>
                        <div class="mid_message_left">
                            <div class="mid_title"><?php echo ucfirst($message->sender_first_name) .' '. ucfirst($message->sender_last_name) ?></div>
                            <div class="inbox_date"><?php echo date("n/j g:ia", strtotime($message->date)); ?></div>
                            <div class="clear"></div>
                            <div class="mid_message_text"><?php echo $message->message; ?></div>
                        </div>
                    </div>
                    <!--message_mid end-->
                    <?php endforeach; ?>
                </div>
                <form id="chatpage">
                    <div class="reply_box">
                        <textarea name="" cols="" rows="" id="message" class="replay_textarea"></textarea>
                        <input type="hidden" id="cid" value="<?php echo $chat_id; ?>">
                        <input type="hidden" id="to" value="">
                        <input type="hidden" id="csrftoken" name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash()?>" >
                        <input type="hidden" id="sender" value="<?php echo ucfirst($this->session->userdata('firstName')) .' '. ucfirst($this->session->userdata('lastName')); ?>">
						<input type="hidden" id="uid" value="<?php echo $chat_list[0]->user_id; ?>">
                        <input type="hidden" id="co" value="0">
                       <!-- <div class="drag_box"><img src="images/drag.png" /> drag ﬁle to attach</div> -->
                    </div>

                    <input class="replay_send" type="submit" value="send" />
                </form>
            </div>
        </div>

        <!-- right bar end -->
    </div>
</div>
<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <style>
        #wrap{
            background: url(/public/images/splash_bg.jpg) center top no-repeat;
            background-size: cover;
        }
        .footer a {
            color: white;

        }
        .footer a:hover {
            text-decoration: underline;
        }


        .footer {
            color: #FFF;
        }
    </style>

    <!-- four social icons -->
    <div class="container">
    	<div class="col-md-3 social-landing">
	    	<a href="https://www.facebook.com/greennvy" target="_blank"><i class="fa fa-facebook-square"></i></a>
    	</div>
    	
    	<div class="col-md-3 social-landing">
	    	<a href="https://plus.google.com/u/1/b/116042321547209398076/116042321547209398076/about" target="_blank"><i class="fa fa-google-plus-square"></i></a>
    	</div>
    	
    	<div class="col-md-3 social-landing">
	    	<a href="https://twitter.com/greennvy" target="_blank"><i class="fa fa-twitter-square"></i></a>
    	</div>
    	
    	<div class="col-md-3 social-landing">
	    	<a href="http://instagram.com/green_nvy" target="_blank"><i class="fa fa-instagram"></i></a>
    	</div>
    
    </div>
    <!-- four social icons -->

<?php
// if locate $_GET parameter is passed (can be empty, simply needs to be set)
if (isset($_GET['locate'])) {
    $this->load->view('cannabinoid/search/gettingstarted_modal');
}

<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <style>
        #wrap{
            background: url(/public/images/splash_bg.jpg) center top no-repeat;
            background-size: cover;
        }
        .footer a {
            color: white;

        }
        .footer a:hover {
            text-decoration: underline;
        }


        .footer {
            color: #FFF;
        }
    </style>

    <!-- four social icons -->
    <div class="container">
    	<div class="col-md-4 social-landing">
	    	<a href="http://www.hightimes.com/" target="_blank"><img class="img-responsive" src="/public/images/high_times_logo.png"></a>
    	</div>
    	
    	<div class="col-md-4 social-landing">
	    	<a href="http://norml.org/" target="_blank"><img class="img-responsive" src="/public/images/NORML.jpeg"></a>
    	</div>
    	
    	<div class="col-md-4 social-landing">
	    	<a href="http://www.mpp.org/" target="_blank"><img class="img-responsive" src="/public/images/mpp-header.png"></a>
    	</div>
    	
    
    </div>
    <!-- four social icons -->

<?php
// if locate $_GET parameter is passed (can be empty, simply needs to be set)
if (isset($_GET['locate'])) {
    $this->load->view('cannabinoid/search/gettingstarted_modal');
}

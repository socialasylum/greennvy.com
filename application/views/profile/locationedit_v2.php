<input type="hidden" id="locationid" value="<?php echo $id; ?>" />
<div class="container">
    <div class="row">
        <?php $this->load->view('partials/settings_side_menu'); ?>
        <?php $this->load->view('partials/settings_modal'); ?>
        <?php $this->load->view('partials/profile_images_modal'); ?>
        <?php $this->load->view('partials/profile_videos_modal'); ?>
        <?php $this->load->view('partials/profile_deals_modal'); ?>
        <div class="col-md-8" id="ajaxSwap"></div>
    </div>
</div>
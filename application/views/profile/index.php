<?php if (!defined('BASEPATH')) die('Direct access not allowed'); ?>

<?php

$tabActive[$tab] = 'active';

?>

<div class="container">
<div class="row">
<?php $this->load->view('partials/user_settings_side_menu'); ?>

<div class="col-md-8 tab-content">

    <!-- settings -->
    <div class="tab-pane <?= $tabActive[0] ?> form-horizontal" id="settings">

        <div class="war profilefix">

            <div class="profileHeaders">My Account</div>


            <div id="uploadProgressContainer"></div>



            <div class="row">
                <div class="col-md-6" align="center">

                    <img src="/user/profileimg/240/<?php echo $this->session->userdata('userid') ?>" class="current_profile_image">

                </div>
                <div class="col-md-6" align="center">

                    <div class="profile_img_upload">
                        <form>
                            <input type="hidden" id="photoFileName" value="" />
                            <input type="hidden" name="imageid" id="imageid" value="0" />
                            <div class="form-group">
                                <div class="col-sm-9 image_upload_profile">
                                    <div id='img-hidden-container'></div>

                                    <div class='fileUploadContainer' id='fileUploadContainer'>
                                        <div class="drop_spot"><div><i class="fa fa-arrow-down"></i></div><div>Drag Photo Here</div></div>
                                        <div class="dragButton"></div>
                                        <input type='file' name='photoFile' id='photoFile' class='hide' <?= $actDis ?>>
                                    </div>
                                </div> <!-- col-9 -->
                            </div> <!-- .form-group -->
                        </form>
                    </div>

                </div>

            </div>

            <div class='hide' id='fileUploadProgressHtml'>
                <div class='fileProgressBar'>
                    <div class='row'>
                        <div class='col-md-3'><div class='fileNameTxt'></div></div>
                        <div class='col-md-9'>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div> <!-- /.progress-bar -->
                            </div> <!-- /.progress -->
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.fileProgressBar -->
            </div> <!-- /#fileUploadProgressHtml -->
            <?php

            $attr = array
            (
                'name' => 'userForm',
                'id' => 'userForm'
            );

            echo form_open('#', $attr);
            ?>
        <div class="form-group">
            <label for="firstName" class="col-sm-3 control-label">First Name</label>
            <div class="col-sm-9">
                <input type='text' class='form-control' name='firstName' id='firstName' value="<?=$userinfo->firstName?>">
            </div> <!-- col-9 -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label for="lastName" class="col-sm-3 control-label">Last Name</label>
            <div class="col-sm-9">
                <input type='text' class='form-control' name='lastName' id='lastName' value="<?=$userinfo->lastName?>">
            </div> <!-- col-9 -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">E-Mail</label>
            <div class="col-sm-9">
                <input type='text' class='form-control' name='email' id='email' value="<?=$userinfo->email?>">
            </div> <!-- col-9 -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label for="timezone" class="col-sm-3 control-label">Timezone</label>
            <div class="col-sm-9">
                <select id='timezone' name='timezone' class='form-control'>
                    <option value=''></option>
                    <?php
                    if (!empty($timezones))
                    {
                        foreach ($timezones as $zone => $abb)
                        {
                            $sel = ($userinfo->timezone == $zone) ? 'selected' : null;

                            echo "<option {$sel} value=\"{$zone}\">{$abb}</option>" . PHP_EOL;
                        }
                    }
                    ?>
                </select>
            </div> <!-- col-9 -->
        </div> <!-- .form-group -->


        <div class="form-group">
            <label for="" class="col-sm-3 control-label">Website</label>
            <div class="col-sm-9">
                <input type='text' class='form-control' name='website' id='website' value="<?=$userinfo->companyWebsiteUrl?>">
            </div> <!-- col-9 -->
        </div> <!-- .form-group -->

        <div class="form-group">
            <label for="bio" class="col-sm-3 control-label">Bio</label>
            <div class="col-sm-9">
                <textarea class='form-control' name='bio' id='bio' rows='5'><?=$userinfo->bio?></textarea>
            </div> <!-- col-9 -->
        </div> <!-- .form-group -->

        <hr>

        <button type='button' class='btn pull-right formbtn profileSaveBtn' id='saveSettingsBtn'><i class='fa fa-save'></i> Save</button>

        </form>

    </div>
    </div>
    <!-- end settings -->

    <!--home -->
    <div class="tab-pane <?= $tabActive[1] ?>" id="home">
        <div class="war profilefix">
            <div class="profileHeaders">Vendors         <div class="add_new add_new_Vendor"><a href="/profile/vendoredit#profileSettings" data-reveal-id="dealsModal">add new</a></div>
            </div>

        <?php
        if (empty($listings))
        {
            echo $this->alerts->info("You currently have no listings");
        }
        else
        {
            ?>
            <div class='search-results'>
                <?php
                $cnt = 0;
                foreach ($listings as $location)
                {
                    $info = null;

                    $reviews = $avgRating = 0;

                    try
                    {
                        $deleted = $this->dojos->checkLocationDeleted($location);

                        // if location is set to deleted, does not show it
                        if ($deleted) continue;

                        $info = $this->dojos->getLocationInfo($location);

                        //$k = $data->ID;

                        //gets review count
                        $reviews = $this->dojos->getReviewCnt($location);

                        $reviewTxt = ($reviews == 1) ? 'Review' : 'Reviews';

                        $avgRating = $this->dojos->avgReviews($location);
                        // echo "AVG: $avgRating";
                    }
                    catch (Exception $e)
                    {
                        $this->functions->sendStackTrace($e);
                        continue; // skips location if errors out
                    }

                    // loads view with stars for average rating
                    $bodyRating['avg'] = $avgRating;
                    $ratingHtml = $this->load->view('dojos/listavgrating', $bodyRating, true);


                    echo <<< EOS

            <div class='col-md-12 listing'>

                    <div class='listRight pull-right'>

                    <!--<a href="deals/index/{$location}" alt='Events' class='btn btn-default btn-xs listing-cal-btn'><i class='fa fa-pencil'></i></a>-->

                    <!--<button onclick="profile.editEvents({$location});" alt='Events' class='btn btn-default btn-xs listing-cal-btn'><i class='fa fa-gift'></i></button>-->

                    <button onclick="profile.editlisting({$location});" alt='Edit Listing' class='btn btn-default btn-xs listing-edit-btn'><i class='fa fa-pencil'></i></button>


                    {$ratingHtml}

                    <span class='reviewsTxt'>{$reviews} {$reviewTxt}</span>
                </div> <!-- .listRight -->



                <div class='listImg' onclick="profile.viewListing({$location});"><img src='/locations/img/{$location}/100'></div>
                <div class='listContent' onclick="profile.viewListing({$location});">
                    <h3>{$info->name}</h3>

                    <p class='locAddress'><span>Location :</span> {$info->address} {$info->city}, {$info->state} {$info->postalCode}</p>
                    </div> <!-- .listContent -->

                <div class='clearfix'></div>

            </div> <!-- .listing -->

EOS;
                    $cnt++;
                }

                if (empty($cnt)) echo $this->alerts->info("You currently have no listings");

                ?>
            </div> <!-- search-results -->
        <?php
        }
        ?>
    </div>
    </div>
    <!--home end -->

    <?php
    /*
    <!-- facebook -->
    <div class="tab-pane <?= $tabActive[2] ?>" id="facebook">
        <div class="war profilefix">
            <div class="profileHeaders">Facebook</div>
            <?php if (empty($facebookID)) : ?>
                <p>Your account is <strong>not</strong> linked with facebook.</p>

                <button class='btn btn-lg btn-primary' id='connectFBbtn'>Connect Facebook Account</button>
            <?php else: ?>
                <p>Your account is linked with Facebook.</p>

                <button class='btn btn-lg btn-danger' id='unlinkFBbtn'>Unlink Facebook Account</button>

            <?php endif; ?>
        </div>
    </div>
    <!-- end facebook -->
    */
    ?>
    <!-- claim -->
        <?php if($this->session->userdata('userid') == '170' || $this->session->userdata('userid') == '132' || $this->session->userdata('userid') == '155') : ?>
            <div class="tab-pane <?=$tabActive[2]?>" id="claim">
                <div class="war profilefix">
                    <div class="profileHeaders">Claim Requests</div>
                    <?php foreach($requests as $claim) : ?>
                        <div class='col-md-12 listing' id="claim-<?php echo $claim->id ?>-<?php echo $claim->userid; ?>">
                            <div class='listImg'><img src='/user/profileimg/150/<?php echo $claim->userid; ?>'></div>
                            <div class='listContent'>
                                <h3><?php echo $claim->requestName; ?></h3>
                                <p class="locAddress"><span>Location: </span><?php echo $claim->locationName; ?> - <?php echo $claim->city; ?>, <?php echo $claim->state; ?><br />
                                    <span>Phone: </span><?php echo $claim->phone; ?><br />
                                    <span>Position: </span><?php echo $claim->positionInBusiness; ?><br />
                                <div class="pull-right">
                                    <button class="btn btn-primary chat_profile_form_btn" id="<?php if($claim->authorizedBy){ echo 'claimRemove'; } else { echo 'claimAccept'; }?>-<?php echo $claim->id; ?>-<?php echo $claim->userid; ?>" onclick="<?php if($claim->authorizedBy){ echo 'claimRemove'; } else { echo 'claimAccept'; }?>(<?php echo $claim->id; ?>, <?php echo $claim->userid; ?>)"  data-token="<?php echo $this->security->get_csrf_hash()?>" data-user="<?php echo $claim->userid; ?>"><?php if($claim->authorizedBy){ echo 'Remove'; } else { echo 'Accept'; }?></button>
                                    <button class="btn btn-default" id="claimDelete-<?php echo $claim->id; ?>-<?php echo $claim->userid; ?>" onclick="claimDelete(<?php echo $claim->id; ?>, <?php echo $claim->userid; ?>)"  data-token="<?php echo $this->security->get_csrf_hash()?>" data-user="<?php echo $claim->userid; ?>">Delete</button>
                                </div>
                            </div> <!-- .listContent -->

                            <div class='clearfix'></div>
                        </div> <!-- .listing -->
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    <!-- end claim -->
    <!-- claim -->
    <?php if($this->session->userdata('userid') == '170' || $this->session->userdata('userid') == '132' || $this->session->userdata('userid') == '155') : ?>
        <div class="tab-pane <?=$tabActive[3]?>" id="rank">
            <div class="war profilefix">
                <div class="profileHeaders">Featured Rank</div>

                <div class="row">
                    <div class="col-md-12 text-center" id="rankStatus"></div>
                </div>

                <div class="row">
                    <div class="col-md-12"><h4>Search</h4></div>
                </div>

                <div class="row">
                    <div class="col-md-6"><b>Name:</b> <select class="form-control" id="companyQuery" onChange="profile.getCoRank()">
                           <?php foreach($companyLists as $co) :?>
                                <option value =<?php echo $co->id; ?>><?php echo $co->name; ?></option>
                            <?php endforeach ?>

                    </select> </div>
                </div>

                <div class="row">
                    <div class="col-md-12"><h4>Result</h4></div>
                </div>

                <div class="row">
                    <div class="col-md-4"><h4>Name</h4></div><div class="col-md-6"><h4>Address</h4></div><div class="col-md-2"><h4>Rank</h4></div>
                </div>


                <div class="row">
                    <div class="col-md-4"><span class="company_name_fill"></span> </div><div class="col-md-6"><span class="company_address_fill"></span></div><div class="col-md-2"><span class="company_rank_fill"></span></div>
                </div>

                <hr />
                <div class="row">
                    <div class="col-md-12"><h4>Current 20 Highest</h4></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><h4>Name</h4></div><div class="col-md-6"><h4>Address</h4></div><div class="col-md-2"><h4>Rank</h4></div>
                </div>
                <?php foreach ($locRank as $coRank) : ?>

                    <div class="row">
                        <div class="col-md-4"><?php echo $coRank->name; ?></div><div class="col-md-6"><?php echo $coRank->formattedAddress; ?></div>
                        <div class="col-md-2">
                            <select class="form-control" onchange="profile.upDateRank(<?php echo $coRank->id; ?>, $(this).val())">
                                <option <?php if($coRank->rank == '0'){echo 'selected';} ?>>0</option>
                                <option <?php if($coRank->rank == '1'){echo 'selected';} ?>>1</option>
                                <option <?php if($coRank->rank == '2'){echo 'selected';} ?>>2</option>
                                <option <?php if($coRank->rank == '3'){echo 'selected';} ?>>3</option>
                                <option <?php if($coRank->rank == '4'){echo 'selected';} ?>>4</option>
                                <option <?php if($coRank->rank == '5'){echo 'selected';} ?>>5</option>
                                <option <?php if($coRank->rank == '6'){echo 'selected';} ?>>6</option>
                                <option <?php if($coRank->rank == '7'){echo 'selected';} ?>>7</option>
                                <option <?php if($coRank->rank == '8'){echo 'selected';} ?>>8</option>
                                <option <?php if($coRank->rank == '9'){echo 'selected';} ?>>9</option>
                            </select>
                        </div>
                    </div>
                    <hr />
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <!-- end claim -->

</div>
</div>
</div>

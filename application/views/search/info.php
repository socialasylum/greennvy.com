<input type="hidden" id="locationid" value="<?php echo $id; ?>" />
<div class="war_outer_2"  style="background: url('<?php echo $headerBanner; ?>') no-repeat center center fixed; -moz-background-size: cover; -webkit-background-size: cover; -o-background-size: cover; background-size: cover;">
    <div class="container">
        <?php
        $imgsrc = "/public/images/gst_no_profile.jpg";

        if (!empty($defaultImg))
            $imgsrc = "/public/uploads/locationImages/{$id}/{$defaultImg}";
        ?>
        <input type='hidden' id='display-img-src' value="<?= $imgsrc ?>">
        <div class="banner_img_search"><?php if($info->rank >= 6) { echo '<i class="fa fa-star fa-4x" id="goldStarInfoPage"></i>'; } ?><img class="banner_logo" src="/locations/img/<?php echo $id ?>/350" /></div>
        <div class="banner_text <?php echo $name_class; ?>"><?php echo $info->name; ?></div>
        <div class="banner_location">
            <i class="fa fa-map-marker"></i>
            <?php echo $info->city; ?>, <?php echo $info->state; ?>
        </div>
        <div class="banner_review">
            <?php
            $bodyRating['avg'] = 0;
            $bodyRating['largeStar'] = true;
            $ratingHtml = $this->load->view('search/listavgrating', $bodyRating, true);
            foreach ($reviews as $r) {
                $bodyRating['avg'] = $r->rating;
                $bodyRating['largeStar'] = true;
                $ratingHtml = $this->load->view('search/listavgrating', $bodyRating, true);
            }
            ?>
            <?php echo $ratingHtml; ?>
            <br /> <?= count($reviews) ?> REVIEW<?= (count($reviews) == 1) ? null : 'S' ?>
        </div>
        <?php if (!is_null($isFollowing)): ?>
            <div class="follow_btn hidden-xs"><a class="followBtn" id="followBtn" href="#"><?php echo (($isFollowing) ? 'unfollow' : 'follow'); ?></a></div>
        <?php endif; ?>
    </div>

</div>
<!--war_outer end-->

<!--middle_box start-->
<div class="middle_box hidden-xs">
    <div class="container">
        <div class="col-md-8 box">
            <?php echo $menu_userlist; ?>
        </div>
        <div class="col-md-4 box">
            <div class="user_icon_list">
                <ul>
                    <li><a data-toggle="modal" data-target="#messageModal" id="sendMessage"><i class="fa fa-envelope fa-stack-2x icon_list"></i></a></li>
                    <li><a href="#about" data-link-command="#uploads" rel="/search/about_us/<?php echo $id; ?>" class="active"><i class="fa  fa-user fa-stack-2x icon_list"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--middle_box end-->

<!--middle_box start for mobile-->
<div class="middle_box visible-xs">
    <div class="container">
        <div class="center-block">
            <div class="user_list">
                <ul>
                    <li>
                        <?php if($this->uri->segment(1)!='search'):?>
                            <a href="#/search/info/<?php echo $info->id;?>">Location<span><?php echo $info->name; ?></span></a>
                        <?php endif;?>
                    </li>
                    <li>
                        <a href="#menu" class="menu_button menu" id="menu" data-link-command="#menu" rel="/search/menu_content/<?php echo $info->id;?>">menu<span><?php echo count($menu);?></span></a>
                    </li>
                    <li>
                        <?php if($info->id):?>
                            <a id="dealsBtn" rel="/deals/view/<?php echo $info->id;?>" class="menu_button" data-link-command="#deals" href="#deals">deals<span><?php echo count($deals);?></span></a>
                        <?php else:?>
                            <a  id="dealsBtn2" rel="/deals" href="#deals" data-link-command="#deals">deals<span><?php echo count($deals);?></span></a>
                        <?php endif;?>
                    </li>
                    <li>
                        <a id="reviewsBtn" rel="/reviews/info/<?php echo $info->id;?>" class="menu_button" data-link-command="#reviews" href="#reviews">reviews<span><?php echo count($reviews);?></span></a>
                    </li>
                    <li>
                        <a id="uploadsTabButton" class="menu_button" data-link-command="#uploads" rel="" href="#uploads">uploads<span><?php echo $uploadsTotal; ?></span></a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#messageModal" id="sendMessage" ><i class="fa fa-envelope vendor_icon_item"></i></a>
                    </li>
                    <li>
                        <a href="#about" data-link-command="#uploads" rel="/search/about_us/<?php echo $id; ?>" ><i class="fa fa-user vendor_icon_item"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--middle_box end-->

<div id="search_info_main_container">
    <!--container start-->
    <div class="container">
        <div class="row" id="ajaxSwap">
            <?php $this->load->view('partials/about_us', array('hours' => $hours, 'info' => $info)); ?>
        </div>
    </div>
    <!--container end-->
</div> <!-- end main_container -->

<!-- message modal start -->
<div class="modal fade" id='messageModal'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body bluespa-modal">

                <div id='getStartedAlert'></div>

                <form id='messageForm'>

                    <h2>To: <?php echo $info->name; ?></h2>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" id="message" placeholder="" rows="6"></textarea>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>" name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash()?>">
                    <input type="hidden" id="to" value="<?php echo $id; ?>">
                    <input type="hidden" id="co" value="1">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary chat_profile_form_btn" onclick="sendMessage()">Submit</button>
                    </div>
                </form>

            </div> <!-- modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- message modal end -->

<!-- claim location modal -->
<div class="modal fade" id='claimLocation'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body bluespa-modal">

                <div id='getStartedAlert'></div>

                <form id='claimForm'>

                    <h2>Claim This Location</h2>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Full Name</label>
                        <input type="text" class="form-control" id="claim_name" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Contact Number</label>
                        <input type="text" class="form-control" id="claim_phone" placeholder="(999) 999-9999">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Position In Business</label>
                        <input type="text" class="form-control" id="claim_position" placeholder="Clerk">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Comments</label>
                        <textarea class="form-control" id="claim_comment" placeholder=""></textarea>
                    </div>
                    <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>" name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash()?>">
                    <input type="hidden" id="id" value="<?php echo $id; ?>">

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary chat_profile_form_btn" id="submitClaim">Submit</button>
                    </div>
                </form>

            </div> <!-- modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




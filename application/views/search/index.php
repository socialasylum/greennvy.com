<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!--war_outer start-->
<div class="war_outer">
    <div class="container">
        <div class="banner_img img-responsive"><img src="/public/images/cheese-big.png" /></div>
        <div class="banner_text">Find dispensaries, deals,<br />reviews, and strain info.</div>
        <div class="row">
            <div class="sign_btn"><a href="javascript:global.loadSignup();" class="big-link" id="signupBigButton">SIGN ME UP</a></div>
        </div>
        <?php /*
        <div class="row">
            <div class="face_btn"><a id="bigFBbtn" href="#">sign up with facebook</a></div>                         
        </div>
        */ ?>
    </div>
</div>
<!--war_outer end-->

<!--container start-->
<div class="container">
    <h1 class="big_title">Dispensaries</h1>
    <h4 class="listed">Are you a doctor/dispensary? <a href="javascript:global.loadSignup();">Sign up to get listed!</a></h4>
    <div class="war_landing">
        <div class="col-md-8 box" id="mapWell">
            <div id='previewMap' class="map" style="height: 600px;"></div>
        </div>
        <?php ?>
        <div class="col-md-4 box" id="mapListings">
            <?php echo $initListings; ?>
        </div>
    </div>
    <h4 class="listed">Browse our curated database of approved vendors. <br />Get unadulterated reviews and ratings from other users just like you.</h4>
    <div class="brow_btn"><a href="<?php echo $location_search_link; ?>">browse dispensaries</a></div> 
</div>
<!--container end-->

<!--war_outer start-->
<div class="war_outer">
    <div class="container">
        <div class="reviews_title">Reviews</div>
        <div class="row">
            <?php foreach ($reviews as $r): ?>
                <div class="col-md-4"> 
                    <div class="review_box">
                        <div class="review_img"><img src="/user/profileimg/300/<?php echo $r->userid; ?>/<?php echo $r->profile_image; ?>" /></div>
                        <p class="review_text"><i class="fa  fa-quote-left review_icon"></i><?php echo ((strlen($r->comment) > 200) ? substr($r->comment, 0, strpos($r->comment, ' ', 200)) . '...' : $r->comment); ?></p>
                        <div class="reviews_title_bott">-<?php echo $r->name; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>    
        </div>
        <div class="reviews_butt_text">We share your thoughts in person, <br />now you can share online.</div>
        <div class="brow_btn"><a href="/reviews">more reviews</a></div> 
    </div>
</div>
<!--war_outer end-->

<!--container start-->
<div class="container">
    <h1 class="big_title">Deals</h1>
    <?php foreach ($deals as $r): ?>
    <a href="/search/info/<?php echo $r->location_id; ?>#deals">
        <div class="col-md-4">
            <div class="home_deals">
                <div class="deals_img">
                    <div class="home_deals_offer"><span>save</span><br />$<?php echo number_format($r->retail_price - $r->discount_price, 0); ?></div>
                    <img src="/deals/dealimg/700/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" />
                </div>
                <div class="deals_location_name">
                    <?php echo $r->location_name; ?>
                </div>
                <h4 class="deals_title"> <?php echo $r->deal_name; ?></h4>
                <div class="deals_ends"><a href="#"><?php echo number_format($r->distance, 2); ?> mi away</a></div>
                <p><?php echo $r->deal_description; ?></p>
            </div>
        </div>
    </a>
    <?php endforeach; ?>

    <div class="clear"></div>
    <h4 class="listed">Instantly save money with real-time notifications.</h4>
    <div class="brow_btn"><a href="/deals">browse deals</a></div> 

</div>
<!--container end-->

<?php
// if locate $_GET parameter is passed (can be empty, simply needs to be set)
if (isset($_GET['locate'])) {
    $this->load->view('cannabinoid/search/gettingstarted_modal');
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php

if (count($places) == 0) {
echo $this->alerts->alert("No results found");
} elseif (count($places) > 0) {
$mapPointers = array();

echo <<< EOS
<div id="listContent"> 
EOS;
$x = 0;
foreach ($places as $r)
{
    $distance = number_format($r->distance, 2);
    $ratingHtml = $this->load->view('search/listavgrating', array('avg' => $r->avgRating), true);
echo <<< EOS

        <div class="map_addr" id="mapMarker_{$x}">       
            <div class="addr_title"><a href="/search/info/{$r->id}">{$r->name}</a><br /><span>{$distance} mi. away</span></div>
            <div class="addr_reviews">{$ratingHtml}</a><br />{$r->numReviews} reviews</div>
            <div class="clear"></div>
            <div class="addr">{$r->formattedAddress}</div>
            <input type='hidden' name='listingID' value='{$r->id}' />
            <input type="hidden" id="numReviews-{$r->id}" value="{$r->numReviews}" />
        </div>

EOS;
    $x++;
    $markerName = "<div class='map_addr_box'>
    <div class='map_img row'>
        <img src='/locations/img/{$r->id}/240/' />
    </div>
    <div class='row'>
    <div class='col-md-7 col-xs-12'>
        <div class='user_addr_3'>
            <span>{$r->name}</span><br />
            {$r->formattedAddress}<br />
        </div>
    </div>
        <div class='col-md-5 hidden-xs'>
            <div class='user_addr_3'>
                $ratingHtml {$r->numReviews} reviews
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='user_addr_3'
             <a href='/search/info/{$r->id}#menu' class='get_butt hide-fix'>menu</a>
             <a href='/search/info/{$r->id}' target='_blank' class='dialer_butt'>profile</a>
        </div>
    </div>
</div>";
            // hidden marker for google map
            echo "<input type='hidden' class='gmMarker' lat='{$r->lat}' lng='{$r->lng}' title=\"{$r->name}\" contentString=\"{$markerName}\" loaded='0'>" . PHP_EOL;
    }
    echo '</div>';
}
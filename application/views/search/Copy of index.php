<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class='container' id='content-container'>

    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-5 pull-right' id='mapWell'>
            <div class='well affix'>
                <div id='previewMap'></div>
            </div>
        </div>

        <div class='col-xs-12 col-sm-12 col-md-7'>
            <div class="row" id="searchListings">
            <?php if (empty($_GET['q'])) : ?>
                <h3 class="resultstitle"><span class="resultstext">All locations in </span><?= urldecode($_GET['location']) ?></h3>
            <?php else: ?>
                <h3 class="resultstitle"><?= urldecode($_GET['q']) ?><span class="resultstext"> in </span><?= urldecode($_GET['location']) ?></h3>
            <?php endif; ?>
                <div class="pull-right">
                    <a href="#" class="btn btn-default sort">Distance</a>
                    <a href="#" class="btn btn-default sort">Rating</a>
                    <a href="#" class="btn btn-default sort">Reviews</a>
                </div>
            <?php
            if ($this->config->item('bmsCompanyID') == 41 && !$allowedState) {
                echo $this->alerts->info("Not available in your state!");
            }
            ?>
            </div>
            <div class='row <?php if ($places->status == 'OK' && !empty($places)) echo 'search-results'; ?>'>
                <div class='col-md-12' id='listContent'>
                    
                    <?= $initListings ?>

                </div> <!-- col-12 -->

            </div> <!-- .row -->

           
            <div class='row' id='loadingPanel' style="display:none;">
                <div class='col-md-12 well'>
                    <h3>Locating more listings...</h3>
                    <i class='fa fa-spin fa-refresh'></i>
                </div> <!-- .col-12 -->
            </div> <!-- .row -->


        </div> <!-- col-8 -->

    </div> <!-- .row -->
    <div class='row'>
<?php if($deals):?>
        <div class='col-md-7' id='dealsContent'>
<?php foreach ($deals as $r): ?>

	<div class='row blog-row' style="font-weight:bold;padding-left:20px;cursor:pointer;" onclick="location.href='/search/info/<?php echo $r->location_id; ?>';">
       
          <p style="background-color:silver;">
			<h2><?php echo $name; ?></h2>
			<img src="/deals/dealimg/50/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" align="left" />
			<h3><?php echo $r->deal_name; ?></h3>

			<p><?php echo $r->deal_description ?></p>
			
			Retail Price: $<?php echo number_format($r->retail_price,2); ?> - Discount Price: $<?php echo number_format($r->discount_price,2); ?>

            <p>Distance: <?php echo number_format($r->distance, 2); ?> miles</p>
          </p>  
    </div>
<?php endforeach; ?>
<?php else: ?>
<?php echo $this->alerts->info("Not available in your state!");?>
<?php endif;?>
           <a href="/deals">More Deals Here...</a>
        </div> 
    </div> <!-- .row -->
    
</div>

<?php
// if locate $_GET parameter is passed (can be empty, simply needs to be set)
if (isset($_GET['locate'])) {
    $this->load->view('cannabinoid/search/gettingstarted_modal');
}
?>
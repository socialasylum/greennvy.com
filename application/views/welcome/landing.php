<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>

    <style>
        #wrap{
            background: url(/public/images/splash_bg.jpg) center top no-repeat;
            background-size: cover;
        }

        .footer a {
            color: white;

        }
        .footer a:hover {
            text-decoration: underline;
        }


        .footer {
            color: #FFF;
        }
        .splash_circles a:hover {
            color: white;
            text-decoration: underline;
        }
        .landing-map {
            height: 550px;
        }
    </style>
    <!-- <style>
        @media (min-width: 768px) {
    a:hover {
        color: white;
        text-decoration: underline;
    }
    body {
        padding-top: 0px;
        }

    .square {
        border-width: 0px;
        background-color: transparent;
        color: white;
    }
    .main-nav>a, .btn-link {
        color: #FFFFFF;
    }

    #headerLoginBtn:hover {
        color: white;
    }
    .navbar-brand {
        display: none;
    }
    .splash {
        display: block !important;
    }

    #nav_control_menu:hover {
        color: white;
    }

    </style>
    -->
    <!--container start-->
    <div>
        <div class="container-fluid" id="map_no_padding">
            <div class="col-md-12 box" id="mapWell">
                <div id='previewMap' class="map landing-map"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4 box" id="map_listings" style="display: none">
        <?php echo $initListings; ?>
    </div>
    <!--container end-->
    <!--- HEADLINE AREA -->
    <!-- <div class="container">
        <h1 class="splash_text_head">Free Personalized MMJ Dashboard</h1>
        <h2 class="splash_text_sub">Join the movement and become part of the newest mmj community!</h2>
        <div class="row">
            <div class="sign_btn"><a href="javascript:global.loadSignup();" class="big-link" id="signupBigButton">SIGN ME UP</a></div>
        </div>
    </div> -->
    <!-- HEDLINE AREA END -->

    <!-- three circles area -->
    <div class="container">
        <div class="row splash_circles">
            <div class="col-md-4">
                <a href="http://mail.greennvy.com/" target="_blank">
                    <img src="/public/images/splash_circle_1v2.jpg" class="splash_circle_images">
                    <h3 class="splash_text_sub">Secured Email & Messaging</h3>
                </a>
            </div>
            <div class="col-md-4">
                <a href="/social">
                    <img src="/public/images/splash_circle_2.jpg" class="splash_circle_images">
                    <h3 class="splash_text_sub">Social Media</h3>
                </a>
            </div>
            <div class="col-md-4">
                <a href="/deals">
                    <img src="/public/images/splash_circle_3.jpg" class="splash_circle_images">
                    <h3 class="splash_text_sub">Get Great Deals</h3>
                </a>
            </div>
        </div>
    </div>
    <!-- three circles area end -->

<?php
// if locate $_GET parameter is passed (can be empty, simply needs to be set)
if (isset($_GET['locate'])) {
    $this->load->view('cannabinoid/search/gettingstarted_modal');
}

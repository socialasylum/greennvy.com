<div class="resetPwOuterDiv">
    <?php if ($requestCheck): ?>
    <h2>Reset Password</h2>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Email</label>
        </div>
        <div class="col-md-6">
            <?php echo $email; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Password</label>
        </div>
        <div class="col-md-6">
            <input class="form-control" type="password" name="password" id="password" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Confirm Password</label>
        </div>
        <div class="col-md-6">
            <input class="form-control" type="password" name="password2" id="password2" />
        </div>
    </div>
    <div class="row">
        <a id="saveBtn" class="dialer_butt" href="#">Save</a>
    </div>
    <input type="hidden" id="userid" value="<?php echo $userid; ?>" />
    <input type="hidden" id="requestID" value="<?php echo $requestID; ?>" />
    <?php else: ?>
    <h2>Incorrect Request Information. Please Try Again.</h2>
    <?php endif; ?>
</div>
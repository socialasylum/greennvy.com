<div class="container">
    <h2 style="text-align: center; padding-top: 10px;"><?php echo ($result > 0) ? 'Your account has been successfully activated.<br />Being redirected to dashboard now...or click <a href="/dashboard">here.</a>' : 'There was an error. Please try again.'; ?></h2>
</div>
<?php
if ($result > 0)
{
?>
<script>
setTimeout(function() {
    window.location = '/dashboard';
}, 2000);
</script>
<?php }


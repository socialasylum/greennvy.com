<!--container start-->
<div class="fill">
    <div class="row" id="dashboard_row">

        <?php if ($this->session->flashdata('SUCCESS')): ?>
            <div class='row'>
                <h3 class="alert alert-success"><?php echo $this->session->flashdata('SUCCESS'); ?></h3>
            </div>
        <?php elseif ($this->session->flashdata('FAILURE')): ?>
            <div class='row'>
                <h3 class="alert alert-danger"><?php echo $this->session->flashdata('FAILURE'); ?></h3>
            </div>
        <?php elseif ($this->session->flashdata('NOTICE')): ?>
            <div class='row'>
                <h3 class="alert alert-notice"><?php echo $this->session->flashdata('NOTICE'); ?></h3>
            </div>
        <?php endif; ?>

        <div class="col-md-4" id="recent_activity">
            <?php $this->load->view('partials/dashboard_recent_activity', $activity); ?>
        </div>

        <div class="col-md-8">

            <div class="row">
                <div class="war" id="map_area">
                    <!-- <h3>Dispensaries</h3> -->
                    <div class="col-md-12 box" id="mapWell">
                        <div id='previewMap' class="map"></div>
                    </div>


                    <div class="col-md-4 box" id="map_listings" style="display: none">
                        <?php echo $initListings; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="war" id="carouselDashboardBottom">
                    <!-- <h3>DEALS</h3> -->
                    <div class="clear"></div>
                    <div id="carouselDashboard">
                        <?php foreach ($deals as $deal): ?>
                            <a href="/search/info/<?php echo $deal->location_id; ?>">
                                <div>
                                    <div class="deals_item">
                                        <div class="deals_wrapper">
                                            <img src="/deals/dealimg/245/<?php echo $deal->userid; ?>/<?php echo $deal->deal_image; ?>" />
                                            <span class="deals_wrapper_overlay"></span>
                                            <span class="deals_wrapper_text">
                                                <?php /*
                                                  <div class="save_ammount"><span>save</span><br />$<?php echo $deal->retail_price - $deal->discount_price; ?></div>
                                                 */ ?>
                                                <div class="deals_title"><?php echo $deal->deal_name ?> -  <span class="deals_ends"><?php echo number_format($deal->distance, 2); ?> mi away</span></div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row visible-xs">&nbsp</div>
<!--container end-->


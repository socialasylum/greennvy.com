<div class="container">
    <h2>DEALS NEAR YOU</h2>
    <div class="row">
        <div class="col-md-8">
            <script>
            var curSlide = 0;
            $(document).ready(function() {
                advanceSlide();
                setInterval(function() { advanceSlide(); }, 3000);
            });
            
            function advanceSlide()
            {
                if (curSlide == 0)
                {
                    curSlide = 1;
                    $('#shownImg-1').fadeIn('slow');
                }
                else
                {
                    var nextSlide = (curSlide >= 3) ? 1 : curSlide + 1;
                    $('#shownImg-' + curSlide).fadeOut('slow', function() {
                        $('#shownImg-' + nextSlide).fadeIn('slow');
                        curSlide = nextSlide;
                    });
                }
            }
            </script>
            <?php
            $x = 1;
            foreach ($deals as $deal): ?>
            <div id="shownImg-<?php echo $x++; ?>" style="display: none;">
                <img src="/public/uploads/deals/<?php echo $deal->userid; ?>/<?php echo $deal->deal_image; ?>" style="width: 100%;" />
                <div class="slider_sick">
                    <span>save</span>
                    $<?php echo $deal->retail_price - $deal->discount_price; ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="col-md-4" id="dealsSide">
            <?php foreach ($deals as $deal): ?>
            <div class="row">
                <div class="title"><?php echo $deal->deal_name; ?></div>
                <div class="subtitle"><span><?php echo number_format($deal->distance, 2); ?> mi away</span></div>
                <div class="desc"><span>This deal only lasts for a certain <br />amount of time and cannot be used <br />with any other coupons.</span></div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div> 
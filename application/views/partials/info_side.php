<div class="col-md-4">
    <div class="war_bottom">
        <div class="user_addr">
            <span><?php echo $info->name; ?></span><br /><br /><?php echo $info->address; ?><br /><?php echo $info->city; ?>, <?php echo $info->state; ?> <?php echo $info->postalCode; ?><br /><?php echo $info->phone; ?>
        </div>
        <div class='info-map-col'>					
            <div id='previewMap'></div>

            <!-- saves all the markers to load onto the google map -->
            <div id='savedMarkers'>
                <?php
                $markerName = "<div class='map_addr_box'><div class='map_img row'><img src='/locations/img/{$info->id}/120/' /></div><div class='row'><div class='col-md-7'><div class='user_addr_3'><span>{$info->name}</span><br />{$info->formattedAddress}<br /></div></div><div class='col-md-5'><div class='user_addr_3'>$ratingHtml {$info->numReviews} reviews</div></div></div><div class='row'><a href='/search/info/{$info->id}#menu' class='get_butt'>menu</a><a href='/search/info/{$info->id}' target='_blank' class='dialer_butt'>profile</a></div></div>";
                ?>
                <input type='hidden' class='gmMarker' lat='<?php echo $info->lat; ?>' lng='<?php echo $info->lng; ?>' title="<?php echo $info->name; ?>" contentString="<?php echo $markerName; ?>" loaded='0'>
            </div>
        </div>

        <!--col-md-4 end-->
    </div>
</div>
<div class="war" id="recentList">
   <!-- <h3>RECENT ACTIVITY</h3>-->
    <div class="scroll_activity">
        <!--border_box start-->
        <div class="border_box" id="activityPost">
            <div class="col-sm-12 col-md-12">
                <div class="recnt_box">
                    <div class="share">
                        <input name="share_status" type="text" class="share_input" id="share_status" placeholder="Share an update or drag and drop an image..." />
                        <input type="hidden" id="userid" value="<?php echo $this->session->userdata('userid'); ?>" />
                        <input type="file" name="imageManual" id="imageManual" style="display: none;" />
                        <span class="text-right">
                            <a id="imageAdd" href="javascript:;"><i class="fa fa-paperclip"></i></a>
                            <a id="submitPost" href="javascript:;"><i class="fa fa-paper-plane"></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--border_box end-->
        <?php foreach ($activity as $act)
        {
            if ($act->datatype == 1)
            {
                $images = $this->wall->getPostPhotos($act->postID);
                $act->images = array();
                foreach ($images as $image)
                {
                    $act->images[] = $image->fileName;
                }
            }
            $this->load->view('partials/single_activity', $act);
        }
        ?>
    </div>
</div>

<div class="container">
    <div class="war_bottom">
        <h2>PICTURES</h2>
        <div class="slider-border-top">
            <?php if ($images): ?>
            <div class="left-nav-btn-uploads" id="bottom-left-nav"></div>
            <div id="carousel2" class='outerWrapper'>
                <?php foreach ($images as $image): ?>
                <div class="item">
                    <div class="force-center">
                        <div class="image"><a href="/public/uploads/locationImages/<?php echo $image->locationid; ?>/<?php echo $image->fileName; ?>" data-title="<?php echo $image->caption; ?>" onclick='$(this).ekkoLightbox();return false;'><img src="/search/img/<?php echo $image->locationid; ?>/320/<?php echo $image->id; ?>" /></a></div>
                        <div class="title"><?php echo $image->caption; ?></div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="right-nav-btn-uploads" id="bottom-right-nav"></div>
            <?php else: ?>
            <li class="align-center list-unstyled">
                <div class="alert alert-warning" style="text-align:center">Sorry this company has no images.</div>
            </li>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="war_bottom">
        <h2>Videos</h2>
        <div class="slider-border-top">
            <?php if ($videos): ?>
            <div class="left-nav-btn-uploads" id="top-left-nav"></div>
            <div id="carousel3" class='outerWrapper hidden-xs'>
                <?php foreach ($videos as $video): ?>
                    <div class="item">
                        <div class="force-center">
                            <div class="video"><iframe id="player<?php echo $id; ?>" width='420' height='315' src="https://youtube.com/embed/<?php echo $video->videoID; ?>?enablejsapi=1&origin=http://greenstandardtechnologies.com" frameborder="0" allowfullscreen></iframe></div>
                            <div class="title"><?php echo $video->title; ?></div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>

<div id="carousel4" class='outerWrapper visible-xs'>
                    <?php foreach ($videos as $video): ?>
                        <div class="item">
                            <div class="force-center visible-xs">
                                <div class="video"><iframe id="player<?php echo $id; ?>" width="250" height="188" src="https://youtube.com/embed/<?php echo $video->videoID; ?>?enablejsapi=1&origin=http://greenstandardtechnologies.com" frameborder="0" allowfullscreen></iframe></div>
                                <div class="title"><?php echo $video->title; ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            <div class="right-nav-btn-uploads" id="top-right-nav"></div>
            <?php else: ?>
            <li class="align-center list-unstyled">
                <div class="alert alert-warning" style="text-align:center">Sorry this company has no videos.</div>
            </li>
            <?php endif; ?>
        </div>
    </div>
</div>

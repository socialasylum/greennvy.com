<div class="col-md-4">
    <div class="war_bottom">
        <h2>SETTINGS</h2>
        <div class="left_list">
            <ul>
                <?php if ($id > 0): ?>
                <li><a rel="/profile/dealSettings/<?php echo $id; ?>" href="#dealSettings" id="dealSettings" class="active">Deals</a></li>
                <li><a rel="/profile/menuSettings/<?php echo $id; ?>" href="#menuSettings" id="menuSettings">Menu</a></li>
                <?php endif; ?>
                <li><a rel="/profile/profileSettings/<?php echo $id; ?>"<?php echo ($id > 0) ? '' : ' class="active"'; ?> href="#profileSettings" id="profileSettings">Proﬁle</a></li>
                <?php if ($id > 0): ?>
                <li><a rel="/profile/hourSettings/<?php echo $id; ?>" href="#hourSettings" id="hourSettings">Hours</a></li>
                <li><a rel="/profile/profileImages/<?php echo $id; ?>" href="#profileImages" id="profileImages">Images</a></li>
                <li><a rel="/profile/profileVideos/<?php echo $id; ?>" href="#profileVideos" id="profileVideos">Videos</a></li>
                <?php endif; ?>
            </ul>
        </div>
     </div>
</div>
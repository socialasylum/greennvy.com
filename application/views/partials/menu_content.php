<?php $this->load->view('partials/info_side', $info); ?>

<?php

    if($menu_items) {
        //if the menu items are set start the autocomplete loop
        $autoCompleteArray = '<script>var autoCompleteData = [';

        //for each menu items start loooping over them to add them to the script
        foreach ($menu_items as $item) {
            $autoCompleteArray .= '{ value: "' . htmlentities($item->description) . '", label: "' . htmlentities($item->description) . '"},';
        }

        //remove the last comma, if we dont it will break the js
        $autoCompleteArray = substr($autoCompleteArray, 0, -1);

        //close out the JS
        $autoCompleteArray .= '];</script>';

        //output the auto complete string
        echo $autoCompleteArray;

        //echo out the JS file since it is needed. Putting this in a area when its not needed will break the page. This is a safer method
        echo '<script type="text/javascript" src="/public/js/menu_page.js"></script>';
    }
?>

<!--col-md-8 start-->
<div class="col-md-8">
    <div class="war_bottom">
		<div class="col-md-8"><h2>MENU</h2></div>
		<div class="clear"></div>
        <?php if(!$menu_items) : ?>
            <div class="col-md-12"><div class="alert alert-warning" style="text-align:center">This locations menu is not available.</div></div>
        <?php else: ?>
		<div class="col-md-9 col-md-offset-3">
			<form class="form-inline menu_search_form pull-right">
				<div class="form-group search">
                    <label class="sr-only" for="productsearchtext">Search By Name</label>
					<input type="text" class="form-control search" id="productSearchText" placeholder="Search By Name"/>
				</div>
				<div class="form-group search">
                    <label class="sr-only" for="productSearchType">Search By Name</label>
                    <select id="productSearchType" class="form-control">
						<option value="0">Search By Type</option>
						<?php
							foreach ($product_types as $types) {
								echo '<option value="'. $types->itemid .'">'. $types->title .'</option>';
							}
						?>
					</select>	
				</div>
				<button type="submit" class="btn btn-default" id="resetSearch">Reset</button>
			</form>
		</div>
		<div class="clear"></div>
		<div class="col-md-4 menu_name hidden-xs">Name</div>
        <div class="col-md-8 hidden-xs">
            <div class="col-md-2 menu_price g_headline">g</div>
            <div class="col-md-2 menu_price eighth_headline">1/8</div>
            <div class="col-md-2 menu_price quarter_headline">1/4</div>
            <div class="col-md-2 menu_price half_headline">1/2</div>
            <div class="col-md-2 menu_price oz_headline">Oz.</div>
            <div class="col-md-2 menu_price each_headline">Each</div>
        </div>
        <div class="product_list_item">
        <?php   if (!empty($menu_items)) {
                    foreach ($menu_items as $item)
                    {
                        echo <<<EOT
<div class="menu_border_top menu_items" data-item-type="{$item->item_type}">
    <div class="col-md-4 col-xs-12 menu_title">{$item->description} <br /><sup><i>{$item->title}</i></sup></div>
    <div class="col-md-8 col-xs-12">
        <div class="col-md-2 col-xs-6 menu_price g_price visible-xs">Gram:</div>
        <div class="col-md-2 col-xs-6 menu_price g_price">{$item->per_g}</div>
        <div class="col-md-2 col-xs-6 menu_price g_price visible-xs">Eighth:</div>
        <div class="col-md-2 col-xs-6 menu_price eighth_price">{$item->per_eighth}</div>
        <div class="col-md-2 col-xs-6 menu_price g_price visible-xs">Quarter:</div>
        <div class="col-md-2 col-xs-6 menu_price quarter_price">{$item->per_quarter}</div>
        <div class="col-md-2 col-xs-6 menu_price g_price visible-xs">Half:</div>
        <div class="col-md-2 col-xs-6 menu_price half_price">{$item->per_half}</div>
        <div class="col-md-2 col-xs-6 menu_price g_price visible-xs">Oz:</div>
        <div class="col-md-2 col-xs-6 menu_price oz_price">{$item->per_oz}</div>
        <div class="col-md-2 col-xs-6 menu_price g_price visible-xs">Each:</div>
        <div class="col-md-2 col-xs-6 menu_price each_price">{$item->per_each}</div>
    </div>
</div>                      
EOT;
                    }
                }
?>
        </div>
    </div>
    <?php endif; ?>
</div>
<!--col-md-8 end-->
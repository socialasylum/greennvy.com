<div id="imageModal" class="reveal-modal medium">
    <form id="modal-form" action="#" method="post">
        <input type="hidden" id="photoFileName" value="" />
        <input type="hidden" name="imageid" id="imageid" value="0" />
        <div class="row">
            <div id='img-hidden-container'></div>
            <div id='uploadProgressContainer'></div>
            <div class='fileUploadContainer' id='fileUploadContainer'>
                <div class="dragButton"></div>
                <input type='file' name='photoFile' id='photoFile' class='hide' <?= $actDis ?>>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Caption</label>
                <input type="text" class="form-control" name="caption" id="caption" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                Make Cover Photo <input type="checkbox" name="isCover" id="isCover" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                Make Profile Image <input type="checkbox" name="isProfileImage" id="isProfileImage" />
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <input type="button" class="sign_save" id="saveImage" value="Save" />
            <input type="button" class="sign_cancel" value="Cancel" />
        </div>
    </form>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div class='hide' id='fileUploadProgressHtml'>
    <div class='fileProgressBar'>
        <div class='row'>
            <div class='col-md-3'><div class='fileNameTxt'></div></div>
            <div class='col-md-9'>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div> <!-- /.progress-bar -->
                </div> <!-- /.progress -->
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.fileProgressBar -->
</div> <!-- /#fileUploadProgressHtml -->

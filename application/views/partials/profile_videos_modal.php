<div id="videoModal" class="reveal-modal medium">
    <form id="modal-video-form" action="#" method="post">
        <input type="hidden" name="videoid" id="videoid" value="0" />
        <div class="row">
            <div class="col-md-12">
                <label>URL</label>
                <input type="text" class="form-control" name="url" id="url" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Title</label>
                <input type="text" class="form-control" id="title" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Description</label>
                <textarea class="form-control" id="description"></textarea>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <input type="button" class="sign_save" id="saveVideo" value="Save" />
            <input type="button" class="sign_cancel" value="Cancel" />
        </div>
    </form>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div class="war" id="profileSettings">
    <?php
    $attr = array(
        'name' => 'locationForm',
        'id' => 'locationForm'
    );
    ?>
    <?php echo form_open_multipart('/profile/savelisting', $attr);?>
    <?php if ($id > 0): ?>
    <div class="outer_image clearfix">
        <div class="drag_file"><img src="/locations/img/<?php echo $id; ?>/250" /></div>
        <div class="drop_spot"><div><i class="fa fa-arrow-down"></i></div><div>Drag Photo Here</div></div>
        <input type="hidden" name="imageName" id="imageName" value="" />
        <input type="file" name="image" id="profileImageUpload"/>
    </div>
    <?php endif; ?>
    <div class="repeat_box">
        <div class="profil_name">Business Name</div>
        <div class="profil_input"><input name="name" id="name" type="text" value="<?php echo $info->name; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Address</div>
        <div class="profil_input"><input name="address" id="address" type="text" value="<?php echo $info->address; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Address Line 2</div>
        <div class="profil_input"><input name="address2" type="text" value="<?php echo $info->address2; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">City</div>
        <div class="profil_input"><input name="city" id="city" type="text" value="<?php echo $info->city; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">State</div>
        <div class="profil_input">
            <div class="slect_outer">
                <select name='state' id='state' class='form-control'>
            	<option value=''></option>
            	<?php
            	if (!empty($states))
            	{
	            	foreach ($states as $k => $v)
	            	{
	            		$sel = ($k == $info->state) ? 'selected' : null;

		            	echo "<option {$sel} value='{$k}'>{$v}</option>" . PHP_EOL;
	            	}
            	}
            	?>
            	</select>
            </div>
        </div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Postal Code</div>
        <div class="profil_input"><input name="postalCode" id="postalCode" type="text" value="<?php echo $info->postalCode; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Phone</div>
        <div class="profil_input"><input name="phone" type="text" value="<?php echo $info->phone; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Website</div>
        <div class="profil_input"><input name="website" type="text" value="<?php echo $info->websiteurl; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">E-Mail</div>
        <div class="profil_input"><input name="email" type="text" value="<?php echo $info->email; ?>" /></div>
    </div>
    <?php if ($id > 0): ?>
    <div class="repeat_box">
        <div class="profil_name">Facebook</div>
        <div class="profil_input"><input class="validateURL" name="facebook" type="text" value="<?php echo $info->facebook; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Twitter</div>
        <div class="profil_input"><input class="validateURL" name="twitter" type="text" value="<?php echo $info->twitter; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Instagram</div>
        <div class="profil_input"><input class="validateURL" name="instagram" type="text" value="<?php echo $info->instagram; ?>" /></div>
    </div>
    <div class="repeat_box">
        <div class="profil_name">Description</div>
        <div class="profil_input"><textarea name="description" cols="" rows="6" class="textarea-1"><?php echo $info->description; ?></textarea></div>
    </div>
    <?php endif; ?>
    <div class="repeat_box" style="display: block;">
        <div class="profil_name">Dispensary?</div>
        <input type="radio" name="docdisp" id="dispensary"<?php echo (($id == 0) || ($info->dispensaryid > 0)) ? ' CHECKED' : '' ?> value="0" />
    </div>
    <div class="repeat_box" style="display: block;">
        <div class="profil_name">Doctor</div>
        <input type="radio" name="docdisp" id="doctor"<?php echo ($info->doctorid > 0) ? ' CHECKED' : '' ?> value="1" />
    </div>
    <div class="profil_butt">
        <input type="button" class="replay_send" value="save" id="saveBtn" />
    </div>
    <input type="hidden" name="id" value="<?php echo $id; ?>" />
</form>
</div>
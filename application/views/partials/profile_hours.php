<div class="war" id="hoursDiv">
    <form id="saveHoursForm" method="post" action="/profile">
        <input type="hidden" name="karateToken" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <input type="hidden" name="location_hours_id" value="<?php echo (isset($hours->location_hours_id)) ? $hours->location_hours_id : 0; ?>" />
        <input type="hidden" name="location_id" value="<?php echo $id; ?>" />
        <div class="row">
            <h3>Monday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="monday_opening_time" value="<?php echo $hours->monday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="monday_closing_time" value="<?php echo $hours->monday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
        <div class="row">
            <h3>Tuesday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="tuesday_opening_time" value="<?php echo $hours->tuesday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="tuesday_closing_time" value="<?php echo $hours->tuesday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
        <div class="row">
            <h3>Wednesday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="wednesday_opening_time" value="<?php echo $hours->wednesday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="wednesday_closing_time" value="<?php echo $hours->wednesday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
        <div class="row">
            <h3>Thursday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="thursday_opening_time" value="<?php echo $hours->thursday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="thursday_closing_time" value="<?php echo $hours->thursday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
        <div class="row">
            <h3>Friday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="friday_opening_time" value="<?php echo $hours->friday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="friday_closing_time" value="<?php echo $hours->friday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
        <div class="row">
            <h3>Saturday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="saturday_opening_time" value="<?php echo $hours->saturday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="saturday_closing_time" value="<?php echo $hours->saturday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
        <div class="row">
            <h3>Sunday</h3>
            <div class="col-md-3">
                <input type="text" class="form-control" name="sunday_opening_time" value="<?php echo $hours->sunday_opening_time; ?>" placeholder="Open" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="sunday_closing_time" value="<?php echo $hours->sunday_closing_time; ?>" placeholder="Close" />
            </div>
        </div>
    </form>
    <div class="row">
        <button class="sign_save" id="saveHours">Save</button>
    </div>
</div>


<tr>
    <td valign="middle" align="left"><?php echo $description; ?></td>
    <td valign="middle" align="left"><?php echo $per_g; ?></td>
    <td valign="middle" align="left"><?php echo $per_eighth; ?></td>
    <td valign="middle" align="left"><?php echo $per_quarter; ?></td>
    <td valign="middle" align="left"><?php echo $per_half; ?></td>
    <td valign="middle" align="left"><?php echo $per_oz; ?></td>
    <td valign="middle" align="left"><?php echo $per_each; ?></td>
    <td valign="middle" align="right" class="icon">
        <a href="#" class="delete-item"><i class="fa fa-trash-o fa-2x profile-delete"></i></a>
        <a href="#" class="big-link" data-reveal-id="myModal"><i class="fa fa-pencil fa-2x profile-edit"></i></a>
    </td>
    <input type="hidden" name="menuid" value="<?php echo $menuid; ?>" />
    <input type="hidden" name="item_type" value="<?php echo $item_type; ?>" />
</tr>
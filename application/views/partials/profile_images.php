<div class="add_new" id="add_new_image"><a href="#" data-reveal-id="imageModal">add new</a></div>
<div class="war" id="imagesDiv">
    <?php
    $x = 0;
    $cols = array();
    foreach ($images as $image)
    {
        if ($x >= 3)
        {
            $x = 0;
        }
        $cols[$x++] .= $this->load->view('partials/location_single_image', array('image' => $image, 'id' => $id), true);
    }
    foreach ($cols as $col):
    ?>
    <div class="col-md-4">
        <?php echo $col; ?>
    </div>
    <?php endforeach; ?>
</div>
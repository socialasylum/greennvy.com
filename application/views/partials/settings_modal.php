<div id="myModal" class="reveal-modal medium">
    <form id="modal-form" action="#" method="post">
        <div class="row">
            <div class="col-md-12">
                <label>Item Type</label>
                <select class="form-control" name="item_type">
                    <option value="0">Choose Type</option>
                    <?php
                    foreach ($menu_options as $optionid => $option) {
                        echo "<option value='$optionid'>$option</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Name</label>
                <input type="text" class="form-control" name="description" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>Per g</label>
                <input type="text" class="form-control" name="per_g" />
            </div>
            <div class="col-md-4">
                <label>Per 1/8</label>
                <input type="text" class="form-control" name="per_eighth" />
            </div>
            <div class="col-md-4">
                <label>Per 1/4</label>
                <input type="text" class="form-control" name="per_quarter" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>Per 1/2</label>
                <input type="text" class="form-control" name="per_half" />
            </div>
            <div class="col-md-4">
                <label>Per oz</label>
                <input type="text" class="form-control" name="per_oz" />
            </div>
            <div class="col-md-4">
                <label>Each</label>
                <input type="text" class="form-control" name="per_each" />
            </div>
        </div>
        <input type="hidden" name="menuid" value="" />
        <input type="hidden" name="userid" value="<?php echo $userid; ?>" />
        <input type="hidden" name="strainid" value="0" />
        <input type="hidden" name="active" value="1" />
        <input type="hidden" name="locationid" value="<?php echo $id; ?>" />
        <input type="hidden" name="karateToken" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <div class="row" style="margin-top: 10px;">
            <input type="button" class="sign_save" id="saveMenuItem" value="Save" />
            <input type="button" class="sign_cancel" value="Cancel" />
        </div>
    </form>
    <a class="close-reveal-modal">&#215;</a>
</div>
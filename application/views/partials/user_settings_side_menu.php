<div class="col-md-4">
    <div class="war_bottom">
        <h2>ACCOUNT SETTINGS</h2>
        <div class="left_list">
            <ul class="nav nav-pills nav-stacked">
                <li class="<?=$tabActive[0]?>"><a href="#settings" data-toggle="tab"><i class="fa fa-cog"></i> My Account</a></li>
                <li class="<?=$tabActive[1]?>"><a href="#home" data-toggle="tab"><i class="fa fa-list"></i> Vendors</a></li>
                <?php /*<li class="<?=$tabActive[2]?>"><a href="#facebook" data-toggle="tab"><i class="fa fa-facebook-square"></i> Facebook</a></li> */ ?>
                <?php if($this->session->userdata('userid') == '170' || $this->session->userdata('userid') == '132' || $this->session->userdata('userid') == '155') : ?>
                    <li class="<?=$tabActive[2]?>"><a href="#claim" data-toggle="tab"><i class="fa fa-comments"></i> Claim Requests</a></li>
                <?php endif; ?>
                <?php if($this->session->userdata('userid') == '170' || $this->session->userdata('userid') == '132' || $this->session->userdata('userid') == '155') : ?>
                    <li class="<?=$tabActive[3]?>"><a href="#rank" data-toggle="tab"><i class="fa fa-level-up"></i> Featured Rank</a></li>
                <?php endif; ?>
        </div>
    </div>
</div>

<tr>
    <td valign="middle" align="left" class="deal_name"><?php echo $deal->deal_name; ?></td>
    <td valign="middle" align="left" class="discount_value">$<?php echo $deal->discount; ?></td>
    <td valign="middle" align="left" class="expiration_date"><?php echo $deal->expiration_date; ?></td>
    <td valign="middle" align="right" class="icon">
        <a href="#" class="delete-deal"><i class="fa fa-trash-o fa-2x delete-deal"></i></a>
        <a href="#" class="big-link" data-reveal-id="dealsModal"><i class="fa fa-pencil fa-2x profile-edit"></i></a>
    </td>
    <input type="hidden" name="dealid" value="<?php echo $deal->dealid; ?>" />
    <input type="hidden" name="userid" value="<?php echo $deal->userid; ?>" />
    <input type="hidden" name="deal_description" value="<?php echo $deal->deal_description; ?>" />
    <input type="hidden" name="retail_price" value="<?php echo $deal->retail_price; ?>" />
    <input type="hidden" name="discount_price" value="<?php echo $deal->discount_price; ?>" />
    <input type="hidden" name="deal_image" value="<?php echo $deal->deal_image; ?>" />
    <input type="hidden" name="start_date" value="<?php echo $deal->start_date; ?>" />
    <input type="hidden" name="end_date" value="<?php echo $deal->end_date; ?>" />
    <input type="hidden" name="repeat" value="<?php echo $deal->repeat; ?>" />
    <input type="hidden" name="featured" value="<?php echo $deal->featured; ?>" />
    <input type="hidden" name="is_running" value="<?php echo $deal->is_running; ?>" />
    <input type="hidden" name="created" value="<?php echo $deal->created; ?>" />
    <input type="hidden" name="hits" value="<?php echo $deal->hits; ?>" />
    <input type="hidden" name="active" value="<?php echo $deal->active; ?>" />
</tr>
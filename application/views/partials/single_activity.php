<?php if($dashboardStyle) :?>

    <!--border_box start-->
    <div class="border_box">
        <div class="col-sm-2 col-md-2 col-xs-2">
            <div class="icon_box-1">
                <img class="user_icon_dash" src='<?php echo (($datatype != 4) ? "/user/profileimg/112/{$userid}" : "/search/img/{$locationid}/112/"); ?>' />
            </div>
        </div>


        <div class="col-sm-10 col-md-10 col-xs-10">
            <div class="recnt_box">
                <h4 class="recnt-text"><a href="<?php echo (($datatype != 4) ? "/user/index/" : "/search/info/") . $userid; ?>"><?php echo ($datatype != 4) ? $username : $location_name; ?></a>
                    <?php
                    if ($datatype == 6)
                    {
                        echo ' has uploaded a new image:';
                    }
                    else if ($datatype == 4)
                    {
                        echo ' has added a new menu item.';
                    }
                    ?>
                </h4>
                <?php
                echo $data_text;
                ?>
                <?php if ($images): ?>
                    <br />
                    <?php foreach ($images as $image): ?>
                         <a data-toggle="lightbox" href="#img_lightbox"><img src="/public/uploads/userimgs/<?php echo $userid; ?>/<?php echo $image; ?>" style="max-width: 48%; padding: 1%;"/></a>
                    <?php endforeach; ?>
                    <?php if(gettype($images) == 'string') : ?>
                        <img src="/public/uploads/userimgs/<?php echo $userid; ?>/<?php echo $images; ?>" style="max-width: 48%; padding: 1%;"/>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


<?php else : ?>

    <!--border_box start-->
    <div class="border_box" data-id="<?php echo $postId ?>">
        <div class="icon_box-1" id="poster_pic_<?php echo $postId ?>">
            <img src='<?php echo (($datatype != 4) ? "/user/profileimg/112/{$userid}" : "/search/img/{$locationid}/112/"); ?>' width="56" height="56" />
        </div>
        <div class="recnt_box">
            <h4 class="recnt-text"><a href="<?php echo (($datatype != 4) ? "/user/index/" : "/search/info/") . $userid; ?>"><span id="comment_header_name" class="comment_header_<?php echo $postId ?>"><?php echo ($datatype != 4) ? $username : $location_name; ?></span></a>
                <?php if($userid == $this->session->userdata('userid')) : ?>
                   <span class="pull-right"><a href="#" id="deletePost" onclick="wall.deletePost(<?php echo $postId ?>); return false" data-id="<?php echo $postId ?>" ><i class="fa fa-trash-o"></i></a></span>
                <?php endif ?>
            <?php
                if ($datatype == 6)
                {
                    echo ' has uploaded a new image:';
                }
                else if ($datatype == 4)
                {
                    echo ' has added a new menu item.';
                }
            ?>
            </h4>
            <span class="post_text_<?php echo $postId ?>">
            <?php
            echo $data_text;
            ?>
            </span>
            <?php if ($images): ?>
            <br />
            <?php foreach ($images as $image): ?>
                    <a data-toggle="modal" data-target="#myLightBox" onclick="wall.pictureInfo('<?php echo $userid; ?>/<?php echo $images; ?>', '<?php echo $postId ?>')"><img src="/public/uploads/userimgs/<?php echo $userid; ?>/<?php echo $image; ?>" style="max-width: 48%; padding: 1%;" /></a>
            <?php endforeach; ?>
            <?php if(gettype($images) == 'string') : ?>
                    <a data-toggle="modal" data-target="#myLightBox" onclick="wall.pictureInfo('<?php echo $userid; ?>/<?php echo $images; ?>', '<?php echo $postId ?>')"><img src="/public/uploads/userimgs/<?php echo $userid; ?>/<?php echo $images; ?>" style="max-width: 48%; padding: 1%;"/></a>
            <?php endif; ?>
            <?php endif; ?>
            <br />
    <?php if($this->session->userdata('userid')) : ?>
            <a href="#" id="like_<?php echo $postId ?>" onclick="<?php if($usrLiked) { echo 'wall.unlike('.$postId.')'; } else { echo 'wall.like('.$postId.')'; } ?>; return false;"><i class="fa fa-thumbs-o-up"></i></a> <span id="like_string_<?php echo $postId ?>">
        <?php
        if($usrLiked && $likecnt > '1') {
            $likecnt = $likecnt - 1;

            if($likecnt == '1'){
                $people = "other person";
            } else {
                $people = 'other people';
            }

            echo 'You and '.$likecnt.' '.$people.' liked this.' ;
        } else if($usrLiked && $likecnt == '1') {
            echo 'You liked this.';
        } else if($likecnt > 0){
            echo $likecnt.' people liked this';
        } else {
            echo 'Like';
        }
        ?>

        </span>
    <?php endif; ?>
        </div>
    </div>
    <?php if($this->session->userdata('userid')) : ?>
    <div id="comment_area" class="comment_area_<?php echo $postId ?>" data-id="<?php echo $postId ?>">
        <span class="comment_container_<?php echo $postId ?>">
        <?php foreach($comments as $comment) : ?>
            <div class="comment_row" id="com_<?php echo $comment->id ?>_row">
                <div class="row">
                    <div class="col-md-1">
                        <div class="icon_img">
                             <img src='/user/profileimg/112/<?php echo $comment->userid ?>' width="56" height="56" />
                        </div>
                    </div>
                    <div class="col-md-11">
                        <?php if($comment->userid == $this->session->userdata('userid')) : ?>
                            <span class="pull-right"><a href="#" id="deletePost" onclick="wall.deleteComment(<?php echo $comment->id ?>); return false" data-id="<?php echo $comment->id ?>" ><i class="fa fa-trash-o"></i></a></span>
                        <?php endif ?>
                        <span class="comment_user_name"><?php echo $comment->username ?></span><br />
                        <span class="comment_user_comment"><?php echo $comment->body ?></span>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
            <span class="comment_container_end_<?php echo $postId ?>"></span>
        </span>
        <textarea class="form-control" placeholder="Write A Comment" id="com_<?php echo $postId ?>" data-id="<?php echo $postId ?>" onkeypress="wall.commentEnter(document.getElementById('com_<?php echo $postId ?>').value, <?php echo $postId ?>)"></textarea>

    </div>
    <?php endif; ?>
<!--border_box end-->

<?php endif; ?>
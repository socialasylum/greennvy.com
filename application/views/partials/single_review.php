<div class="border-top">
    <div class="review_title"><a href="/search/info/<?php echo $r->location; ?>"><?php echo $r->name; ?></a></div>
    <div class="clear visible-xs"></div>
    <div class="inbox_date"><?php echo $r->created; ?> hours</div>
    <div class="clear"></div>
    <div class="star">
        <?php
        $bodyRating['avg'] = $r->rating;
        $bodyRating['largeStar'] = true;
        $ratingHtml = $this->load->view('dojos/listavgrating', $bodyRating, true);
        ?>
        <?php echo $ratingHtml; ?>
    </div>
    <div class="clear"></div>
    <p class="review_text_p"><?php echo $r->comment; ?></p>
</div>
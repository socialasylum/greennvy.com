<!--col-md-4 start-->
<?php $this->load->view('partials/info_side', array('info', $info)); ?>
<!--col-md-8 start-->
<div class="col-md-8">
    <div class="war">
        <div class="col-md-12">
            <h2>ABOUT US</h2>
            <p><?php echo $info->description; ?></p>
        </div>
        <div class="col-md-6">
            <h2>HOURS</h2> 
            <div class="hour_box">
                <div class="hour_day">Mon</div>
                <div class="hour_time"><?php echo $hours->monday_opening_time; ?> - <?php echo $hours->monday_closing_time; ?><?php echo (date('l') == 'Monday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
            <div class="hour_box">
                <div class="hour_day">Tue</div>
                <div class="hour_time"><?php echo $hours->tuesday_opening_time; ?> - <?php echo $hours->tuesday_closing_time; ?><?php echo (date('l') == 'Tuesday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
            <div class="hour_box">
                <div class="hour_day">Wed</div>
                <div class="hour_time"><?php echo $hours->wednesday_opening_time; ?> - <?php echo $hours->wednesday_closing_time; ?><?php echo (date('l') == 'Wednesday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
            <div class="hour_box">
                <div class="hour_day">Thu</div>
                <div class="hour_time"><?php echo $hours->thursday_opening_time; ?> - <?php echo $hours->thursday_closing_time; ?><?php echo (date('l') == 'Thursday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
            <div class="hour_box">
                <div class="hour_day">Fri</div>
                <div class="hour_time"><?php echo $hours->friday_opening_time; ?> - <?php echo $hours->friday_closing_time; ?><?php echo (date('l') == 'Friday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
            <div class="hour_box">
                <div class="hour_day">Sat</div>
                <div class="hour_time"><?php echo $hours->saturday_opening_time; ?> - <?php echo $hours->saturday_closing_time; ?><?php echo (date('l') == 'Saturday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
            <div class="hour_box">
                <div class="hour_day">Sun</div>
                <div class="hour_time"><?php echo $hours->sunday_opening_time; ?> - <?php echo $hours->sunday_closing_time; ?><?php echo (date('l') == 'Sunday' && !is_null($open)) ? ' - ' . $open : null; ?></div>
            </div>
        </div>
        <div class="col-md-6">
            <h2>LINKS</h2>
            <?php if (!empty($info->facebook)): ?>
            <div class="socialLink"><i class="fa fa-facebook"></i><a href="<?php echo $info->facebook; ?>">Facebook</a></div>
            <?php endif; ?>
            <?php if (!empty($info->twitter)): ?>
            <div class="socialLink"><i class="fa fa-twitter"></i><a href="<?php echo $info->twitter; ?>">Twitter</a></div>
            <?php endif; ?>
            <?php if (!empty($info->instagram)): ?>
            <div class="socialLink"><i class="fa fa-instagram"></i><a href="<?php echo $info->instagram; ?>">Instagram</a></div>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <p>
            <br><br>
            </p>
            <h2>LAST UPDATED</h2>
            <div class="hour_box">
                <?php
                $n = strtotime(date('Y-m-d h:i:s'));

                $l = strtotime($info->lastUpdated);

                $d = $n - $l;

                $lastUpdate = floor($d / 60 / 60 / 24);


                if ($lastUpdate == 1) {
                    $text = ' day ago';
                } elseif ($lastUpdate == 0) {
                    $lastUpdate = null;
                    $text = ' today';
                } else {
                    $text = 'days ago';
                }
                ?>
                <div class="hour_time"><?php echo $lastUpdate; ?> <?php echo $text; ?></div>
                <div class="clear"></div>
                <?php if ($this->session->userdata('logged_in')): ?>
                <div class="pull-right"><a href="#" data-toggle="modal" data-target="#claimLocation" class="claim_link">Claim Location</a></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!--col-md-8 end-->
<div class="add_new" id="add_new_deal"><a href="#" data-reveal-id="dealsModal">add new</a></div>
<div class="war" id="dealsDiv">
    <table width="100%" border="0" class="table_1">
        <tr>
            <th valign="middle" align="left">Title</th>
            <th valign="middle" align="left">Discount</th>
            <th valign="middle" align="left">Expiration</th>
            <th valign="middle" align="left">&nbsp;</th>
        </tr>
        <?php
        foreach($deals as $deal)
        {
            $deal->discount = $deal->retail_price - $deal->discount_price;
            $this->load->view('partials/profile_single_deal', array('deal' => $deal));
        }
        ?>
    </table>
</div>
<div class="location-single-image pull-left">
    <img src="/locations/img/<?php echo $id; ?>/205/<?php echo $image->id; ?>" />
    <h4><?php echo $image->caption; ?></h4>
    <input type="hidden" name="isCover" value="<?php echo $image->isCover; ?>" />
    <i class="fa fa-pencil fa-2x profile-edit pull-left"></i>
    <i class="fa fa-trash-o fa-2x profile-delete pull-right"></i>
</div>
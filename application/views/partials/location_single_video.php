<div class="videoRow">
    <input type="hidden" name="videoid" value="<?php echo $video->id; ?>" />
    <iframe src="<?php echo $this->locations->getYouTubeEmbedURL($video->videoID); ?>?origin=http://greenstandardtechnologies.com" frameborder="0" allowfullscreen></iframe>
    <div class="textFields">
        Title: <span class="videoTitle"><?php echo $video->title; ?></span>
    </div>
    <div class="textFields">
        Description: <span class="videoDescription"><?php echo $video->description; ?></span>
    </div>
    <div class="icons">
        <i class="fa fa-pencil fa-2x profile-edit pull-left"></i>
        <i class="fa fa-trash-o fa-2x profile-delete pull-right"></i>
    </div>
</div>

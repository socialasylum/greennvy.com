<div id="dealsModal" class="reveal-modal medium">
    <form id="modal-deals-form" action="#" method="post" enctype="multipart/form-data">
        <input type="hidden" name="dealid" id="dealid" value="0" />
        <input type="hidden" name="karateToken" value="<?php echo $this->security->get_csrf_hash(); ?>" />
        <input type="hidden" name="location_id" value="<?php echo $id; ?>" />
        <input type="hidden" name="userid" value="<?php echo $this->session->userdata('userid'); ?>" />
        <div class="row">
            <div class="col-md-12">
                <label>Title</label>
                <input type="text" class="form-control" name="deal_name" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Description</label>
                <textarea class="form-control" name="deal_description" maxlength="180"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Retail Price</label>
                <input type="text" class="form-control" name="retail_price" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Discount Price</label>
                <input type="text" class="form-control" name="discount_price" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Start Date</label>
                <input type="date" class="form-control" name="start_date" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>End Date</label>
                <input type="date" class="form-control" name="end_date" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Repeat</label>
                <select name="repeat" class="form-control">
                    <option value="weekly">Weekly</option>
                    <option value="monthly">Monthly</option>
                    <option value="yearly">Yearly</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Expiration Date</label>
                <input type="date" class="form-control" name="expiration_date" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="imageDealDiv">
                <label>Image</label>
                <input type="file" name="userfile" />
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <input type="button" class="sign_save" id="saveDeal" value="Save" />
            <input type="button" class="sign_cancel" value="Cancel" />
        </div>
    </form>
    <a class="close-reveal-modal">&#215;</a>
</div>
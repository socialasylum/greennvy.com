<div class="container-fluid" id="searchContainer">
	<div id="searchFilters" class="pull-left">
        <div class="input-group col-md-4 pull-left">
            <div class="input-group">
                <input type="text" id="searchTerm" class="form-control" placeholder="Search Term" value="<?php echo $searchterm; ?>" />
            <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="locationSearchButton">Search</button>
            </span>
            </div>
        </div>
        <div class="col-md-8 pull-left" id="filtersRight">
            <div class="pull-left map-filter">
                <input type="checkbox" id="dispensaries"<?php echo (($dispensarieschecked) ? ' CHECKED="CHECKED"' : ''); ?>  /> Dispensaries
            </div>
            <div class="pull-left map-filter">
                <input type="checkbox" id="doctors"<?php echo (($doctorschecked) ? ' CHECKED="CHECKED"' : ''); ?>  /> Doctors
            </div>
            <div class="btn-group pull-left">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="searchDropDown">
                Sort <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" id="searchSort">
                    <li><a href="distance-asc">Distance (Closest First)</a></li>
                    <li><a href="distance-desc">Distance (Farthest First)</a></li>
                    <li><a href="alphabetical-asc">Alphabetical (A to Z)</a></li>
                    <li><a href="alphabetical-desc">Alphabetical (Z to A)</a></li>
                    <li><a href="avgrating-desc">Avg. Rating (Highest)</a></li>
                    <li><a href="avgrating-asc">Avg. Rating (Lowest)</a></li>
                    <li><a href="reviews-desc">Reviews (Most)</a></li>
                    <li><a href="reviews-asc">Reviews (Least)</a></li>
                </ul>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-9 col-sm-9 col-sm-push-3 clearfix" id="searchMap">
            <div id='previewMap' class="map"></div>
        </div>
        <div class="col-md-3 col-sm-3 col-sm-pull-9" id="mapListings">
            <?php echo $initListings; ?>
        </div>
    </div>
</div>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php

if (count($places) == 0) {
echo $this->alerts->alert("No results found");
} elseif (count($places) > 0) {
$mapPointers = array();

echo <<< EOS
<div id="listContent"> 
EOS;
$x = 0;
$avgLat = $avgLng = 0;
foreach ($places as $r)
{
    $avgLat += floatval($r->lat);
    $avgLng += floatval($r->lng);
    $distance = number_format($r->distance, 2);
    $ratingHtml = $this->load->view('search/listavgrating', array('avg' => $r->avgRating), true);
    if($r->rank >= 6){
        $featuredSign = '<i class="fa fa-star" id="goldStar"></i>';
    } else {
        $featuredSign = '';
    }

echo <<< EOS

        <div class="map_addr" id="mapMarker_{$x}">
            <div class="addr_title"><a href="/search/info/{$r->id}">{$r->name} {$featuredSign}</a><br /><span>{$distance} mi. away</span></div>
            <div class="addr_reviews">{$ratingHtml}</a><br />{$r->numReviews} reviews</div>
            <div class="clear"></div>
            <div class="addr">{$r->formattedAddress}</div>
            <input type='hidden' name='listingID' value='{$r->id}' />
            <input type="hidden" id="numReviews-{$r->id}" value="{$r->numReviews}" />
        </div>

EOS;
    $x++;
    $markerName = "<div class='map_addr_box'><div class='row'><div class='col-md-12'> <div class='row'><div class='col-md-4'><img src='/locations/img/{$r->id}/150/' /></div><div class='col-md-8'><div class='user_addr_3_headline'><span>{$r->name}</span></div><div class='row'><div class='col-md-8'>{$r->formattedAddress} </div><div class='col-md-4'>$ratingHtml {$r->numReviews} Reviews</div></div><div class='row'><div class='user_addr_3_btns'><div class='col-md-6'><a href='/search/info/{$r->id}#menu' class='get_butt'>menu</a></div><div class='col-md-6'><a href='/search/info/{$r->id}' target='_blank' class='dialer_butt'>profile</a></div></div></div></div></div></div></div></div>";

            // hidden marker for google map
            echo "<input type='hidden' class='gmMarker' lat='{$r->lat}' lng='{$r->lng}' title=\"{$r->name}\" rank='{$r->rank}' doctor='{$r->doctor}' contentString=\"{$markerName}\" loaded='0'>" . PHP_EOL;
    }
    echo '</div>';
}
if (empty($searchterm))
{
    echo '<input type="hidden" id="avgLat" value="' . $lat . '" />';
    echo '<input type="hidden" id="avgLng" value="' . $lng . '" />';
}
else
{
    echo '<input type="hidden" id="avgLat" value="' . ($avgLat / count($places)) . '" />';
    echo '<input type="hidden" id="avgLng" value="' . ($avgLng / count($places)) . '" />';
}
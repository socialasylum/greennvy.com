<?php
if (!defined('BASEPATH'))
    die('Direct access not allowed');

$a[$nav] = 'active';

include_once 'headinclude.php';
?>

<?= $headscript ?>

<script>
    $(document).ready(function () {
        if ($(".close")[0]) {
            $('.close').on('click', function () {
                location.href = '/';
            });
        }
    });
</script>
<style>
    .followBtn{display:none;}
</style>
</head>

<body<?= (empty($onload)) ? null : " onload=\"{$onload}\"" ?> class="landing">

    <!--header start-->

    <div id='totalNav'>

        <nav id='fixedTopNav' class="navbar navbar-default navbar-fixed-top square" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <?php
                    /*
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      </button>
                     */
                    ?>
                    <?php
                    $logoLink = ($this->session->userdata('logged_in')) ? '/dashboard' : '/';
                    ?>

                    <a class="navbar-brand" href="<?= $logoLink ?>"><img src='/public/images/greenstandardtech_logo.png'></a>
                </div> <!-- .navbar-header -->

                <div class='hidden-xs hidden-sm'>


                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse main-nav" id="bs-example-navbar-collapse-1">



                        <div class="navbar-form" id='main-search'>

                            <?php if ($showLocator !== false) : ?>

                                <div class="form-group">


                                    <div class='input-group searchInputGroup'>

                                        <input type="text" class="form-control pull-left" id='q' name='q' value="<?php echo urldecode($_GET['q']); ?>" placeholder="Search Dispensary Name">

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <?php
                                                try {
                                                    $styles = $this->functions->getCodes(29, $this->config->item('bmsCompanyID'));

                                                    if (!empty($styles)) {
                                                        foreach ($styles as $r) {
                                                            echo " <li><a href='javascript:void(0);' onclick=\"global.styleSelect(this);\">{$r->display}</a></li>" . PHP_EOL;
                                                        }
                                                    }
                                                } catch (Exception $e) {
                                                    $this->functions->sendStackTrace($e);
                                                }
                                                ?>
                                            </ul>
                                        </div><!-- /btn-group -->
                                    </div>

                                    <div class='input-group'>



                                        <div class='location-group'>



                                            <?php
                                            /*
                                              <!-- original search -->
                                              <input type="text" class="form-control" id='q' name='q' value="<?=urldecode($_GET['q'])?>" placeholder="Find Dojo">
                                             */
                                            ?>



                                            <span class='input-group-addon'><a class="findme" href='javascript:void(0);' onclick="global.getLocation();" id='locatorLink'><i class='fa fa-crosshairs'></i></a></span>
                                            <input type="text" id='location' name='location' class="form-control location" value="<?= urldecode($_GET['location']) ?>" placeholder="Your Zipcode, City, State...">

                                            <span class='input-group-btn'>
                                                <a href='#' class="btn btn-default" id='searchlink'><i class='fa fa-search'></i> <span class='findme'>Search</span></a>
                                            </span>

                                        </div> <!-- location-group -->

                                    </div> <!-- .input-group -->

                                </div> <!-- form-group -->

                            <?php endif; ?>

                            <?php if ($this->session->userdata('logged_in') == true) : ?>

                                <?php
                                $display_name = "{$this->session->userdata('firstName')} {$this->session->userdata('lastName')}";
                                ?>

                                <div class='btn-group pull-right'>
                                    <button type='button' class='btn btn-link dropdown-toggle' data-toggle='dropdown'><img src='/user/profileimg/20/<?= $this->session->userdata('userid') ?>'> <?= $display_name ?> <span class='caret'></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="/dashboard"><i class='fa fa-dashboard'></i> Dashboard</a></li>
                                        <li><a href="/user/index/<?= $this->session->userdata('userid') ?>"><i class='fa fa-user'></i> View Profile</a></li>
                                        <li><a href="/profile"><i class='fa fa-gears'></i> My Account</a></li>
                                        <li class="divider"></li>
                                        <li><a href="/welcome/logout"><i class='fa fa-sign-out'></i> Log Out</a></li>
                                    </ul>
                                </div> <!-- btn-group -->


                            <?php else: ?>
                                <button type='button' class='btn btn-link pull-right' id='headerLoginBtn'><i class='fa fa-sign-in'></i> Login</button>
                            <?php endif; ?>


                        </div>


                    </div> <!-- /.navbar-collapse -->

                </div> <!-- .hidden-xs .hidden-sm -->

                <div class='visible-xs visible-sm navSmBtns'>
                    <?php if ($this->session->userdata('logged_in')) : ?>
                        <button class='btn btn-default pull-right' id='navUserIcon'><i class='fa fa-user'></i></button>
                    <?php else: ?>
                        <button class='btn btn-default pull-right' id='loginXSBtn'><i class='fa fa-sign-in'></i></button>
                    <?php endif; ?>
                    <button class='btn btn-default pull-right' id='navSearchIcon'><i class='fa fa-search'></i></button>

                </div>

            </div> <!-- /.container -->
        </nav>


        <div class='smAccountInfo' id='smAccountInfo' style="display: none;">
            <div class='locRow'>
                <?php if ($this->session->userdata('logged_in')) : ?>
                    <button class='btn btn-default btn-sm' id='profileXSBtn'><i class='fa fa-user'></i> View Profile</button>
                    <button class='btn btn-default btn-sm' id='accountXSBtn'><i class='fa fa-gears'></i> My Account</button>
                    <button class='btn btn-default btn-sm' id='logoutXSBtn'><i class='fa fa-sign-out'></i> Log Out</button>

                <?php endif; ?>

                <div class='clearfix'></div>
            </div> <!-- col-12 -->
        </div> <!-- smLocation -->

        <div class='smSearch' id='smSearch' style="display: none;">

            <div class='locRow'>
                <form id='xs-search' role="search" action='/'>
                    <input type='text' class='form-control' id='q' name='q' value="<?= urldecode($_GET['q']) ?>" placeholder="Find dojo">
                    <input type="text" id='location' name='location' class="form-control location" value="<?= urldecode($_GET['location']) ?>" placeholder="Your Zipcode, City, State...">
                    <button type='button' class='btn btn-default btn-sm pull-left' onclick="global.getLocation();" id='xsLocatorBtn'><i class='fa fa-crosshairs'></i></button>
                    <button type='submit' class='btn btn-primary btn-sm pull-right'><i class='fa fa-search'></i> Search</button>
                    <div class='clearfix'></div>
                </form>
            </div> <!-- col-12 -->

        </div>


    </div> <!-- #totalNav -->


    <!--  <div class='contentbg'>
  
          <div class='container main-content'> -->


    <?php /*
      <div class="main-container">
     */ ?>

    <input type='hidden' name='lastLng' id='lastLoc' value="<?= urldecode($_GET['location']) ?>">

    <input type='hidden' name='lat' id='lat' value='<?= (!empty($_GET['lat'])) ? urldecode($_GET['lat']) : 0 ?>'>
    <input type='hidden' name='lng' id='lng' value='<?= (!empty($_GET['lng'])) ? urldecode($_GET['lng']) : 0 ?>'>


    <input type='hidden' id='token' value='<?php echo $this->security->get_csrf_hash(); ?>'>
    <input type='hidden' id='bmsUrl' value='<?php echo $this->config->item('bmsUrl'); ?>'>

    <?php include_once 'alert.php'; ?>

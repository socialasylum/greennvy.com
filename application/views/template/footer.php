<?php if (!defined('BASEPATH')) die('Direct access not allowed'); ?>
<?php /*
  </div> <!-- .container -->
 */ ?>
</div> <!-- main-content -->

</div> <!-- /.contentbg -->

<!--footer start-->
<div class="footer">
    <span>&copy;</span> greenNVY 2014 | <a href="https://www.google.com/finance?q=OTCMKTS:BHNN" target="_blank">Investor Relations</a> | <a href="mailto:support@greennvy.com?Subject=Support%20Question" target="_blank">Support</a> | <a href="#" target="_blank">Privacy Policy</a> | <a href="http://mail.greennvy.com/intranet/login">Whisper Mail</a> | <a href="/news">News</a>
</div>
<!--footer end-->  

<?php
$attr = array
    (
    'id' => 'loginform',
    'name' => 'loginform'
);

echo form_open('#', $attr);
?>

<div class="modal fade" id='loginModal'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">LOG IN</h3>
            </div> <!-- modal-header -->

            <div class="modal-body">

                <div id='loginAlert'></div>

                <p id='forgotPWText' style='display:none'>Please enter your E-Mail address associated with your account.</p>

                <div class="form-group">
                    <input type='text' class='form-control' name='user_email' id='user_email' value="" placeholder='E-MAIL ADDRESS'>
                </div> <!-- .form-group -->

                <div class="form-group" id='passwordFormGroup'>
                    <input type='password' class='form-control' name='user_pass' id='user_pass' value="" placeholder='PASSWORD'>
                </div> <!-- .form-group -->

            </div>
            <div class="modal-footer">
                <div class='form-group'>
                    <button type="button" class="btn btn-red form-control" id='submitLoginBtn'>LOG IN</button>
                </div>
             </div> <!-- modal-footer -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</form>

<?php
$attr = array
    (
    'id' => 'regform',
    'name' => 'regform'
);

echo form_open('#', $attr);
?>

<div class="modal fade" id='regModal'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">SIGN UP WITH EMAIL</h3>
            </div> <!-- modal-header -->

            <div class="modal-body">

                <div id='loginAlert'></div>
                <div class="form-group">
                    <input type='text' class='form-control' name='firstName' id='firstName_reg' value="" placeholder='FIRST NAME'>
                </div> <!-- .form-group -->
                <div class="form-group">
                    <input type='text' class='form-control' name='lastName' id='lastName_reg' value="" placeholder='LAST NAME'>
                </div> <!-- .form-group -->
                <div class="form-group">
                    <input type='text' class='form-control' name='email' id='email_reg' value="" placeholder='EMAIL ADDRESS'>
                </div> <!-- .form-group -->
                <div class="form-group">
                    <input type='password' class='form-control' name='user_pass' id='user_pass_reg' value="" placeholder='PASSWORD'>
                </div> <!-- .form-group -->

            </div>
            <div class="modal-footer">
                <div class='form-group'>
                    <button type="button" class="btn btn-red form-control" id='submitRegBtn'>REGISTER</button>
                </div>
            </div> <!-- modal-footer -->

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</form>

<div class="modal fade" id='getStartedModal'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body bluespa-modal">

                <div id='getStartedAlert'></div>

                <form id='modalSearch' method='get' action='/dojos'>


                    <input type='hidden' name='lat' id='lat' value='0'>
                    <input type='hidden' name='lng' id='lng' value='0'>

                    <input type='hidden' name='location' id='location' value=''>


                    <h2><i class='fa fa-globe'></i> Finding Your Location</h2>

                    <p id='unableTxt' style='display:none;'>We were unable to find your location. Please enter your location below.</p>

                    <div class='locator'>
                        <i id='locatorIcon' class='fa fa-spinner fa-spin'></i>

                        <input type='text' class='form-control' name='location' id='location' value='' placeholder="Your Zipcode, City, State..." style="display:none;">
                    </div> <!-- .locator -->




                    <button type='button' class='btn btn-default btn-lg searchBtn' disabled='disabled' onclick="welcome.bluespaModalSubmit(this);" style="display:none;"><i class='fa fa-search'></i> Lets Go</button>

                </form>
            </div> <!-- modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript" src="/public/js/retina-1.3.min.js"></script>
<?php if ($redirect): ?>
    <script>
                        global.getLocation();
    </script>
<?php endif; ?>
</div> <!-- /.main -->
</div> <!-- /.wrap -->
</body>

</html>

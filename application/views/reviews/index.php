<!--container start-->
<div class="container">
    <div class="war_bottom" id="reviewDiv">
        <h2>REVIEWS</h2>
            <?php if ($reviews): ?>
                <?php foreach ($reviews as $r)
                {
                    $this->load->view('partials/single_review', array('r' => $r));
                }
                ?>
            <?php else: ?>
                <div class="alert alert-warning text-center">There are no Reviews Currently.</div>
            <?php endif; ?>
    </div>
</div>

<?php if(!$reviewBoxDisable) : ?>
<!--container end-->
<div class="container">
    <div class="war_bottom">
        <h2>Post a Review</h2>
        <div class="border-top">
            <?php
            $attr = array
                (
                'id' => 'reviewForm'
            );

            echo form_open('#', $attr);

            $disabled = ($this->session->userdata('logged_in') == true) ? "disabled='disabled'" : null;
            ?>

            <input type='hidden' name='rating' id='rating' value='0'>

            <input type='hidden' name='location' id='location' value='<?php echo $location_id; ?>'>

            <div class='row name-email-review'>
                <div class='col-md-6'>
                    <div class="form-group">
                        <label for='reviewName'>NAME*:</label>
                        <input type='text' class='form-control' id='reviewName' name='reviewName' <?= $disabled ?> value="<?= ($this->session->userdata('logged_in') == true) ? "{$this->session->userdata('firstName')} {$this->session->userdata('lastName')}" : null ?>">
                    </div> <!-- .form-group -->
                </div>

                <div class='col-md-6'>

                    <div class="form-group">
                        <label for='reviewEmail'>E-MAIL*:</label>
                        <input type='text' class='form-control' id='reviewEmail' name='reviewEmail' <?= $disabled ?> value="<?= ($this->session->userdata('logged_in') == true) ? $this->session->userdata('email') : null ?>">
                    </div> <!-- .form-group -->

                </div>
            </div> <!-- row -->

            <div class="form-group">
                <label for='ratings'>RATING:</label>

                <div id='reviewStars' class='reviewStars'>
                    <i class='fa fa-star' id='rating_star_1' value='1'></i>
                    <i class='fa fa-star' id='rating_star_2' value='2'></i>
                    <i class='fa fa-star' id='rating_star_3' value='3'></i>
                    <i class='fa fa-star' id='rating_star_4' value='4'></i>
                    <i class='fa fa-star' id='rating_star_5' value='5'></i>
                </div>
            </div> <!-- .form-group -->

            <div class="form-group">
                <label  for='reviewDesc'>DESCRIPTION:</label>
                <textarea class='form-control' name='reviewDesc' id='reviewDesc' rows='5'></textarea>
            </div> <!-- .form-group -->
            
            <button type="button" class="green_btn col-md-2 col-md-offset-10" id="reviewBtn">Submit</button>
            
            </form>
        </div> <!-- reviews-results -->

    </div>
</div>
<?php endif; ?>

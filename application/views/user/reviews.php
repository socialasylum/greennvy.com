<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>REVIEWS</h2>

        <?php if ($reviews): ?>

            <div class="follower_list">

                <ul>
                    <?php foreach ($reviews as $review) { ?>
                        <li>
                            <span><?php
                                echo <<< EOS
                    <div class="border-top">
                    	<div class="review_title">{$review->locationName}</div>
                        <div class="inbox_date">{$review->datestamp}</div>
                        <div class="clear"></div>
                        <div class="star"><a href="#"><img src="/public/images/star-{$review->rating}.png" /></a></div>
                        <div class="clear"></div>
                        <p class="review_text_p">{$review->comment}</p>
                    </div>
EOS;
?>
                            </span>
                        </li>
                    <?php } //endforeach;?>                              
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>
</div>
<!--container end-->

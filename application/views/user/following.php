<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>FOLLOWING</h2>
        <div class="follower-border-top">
            <?php if ($following): ?>
                <div class="follower_list">
                <?php foreach ($following as $r): ?>
                    <a href="/user/index/<?php echo $r->followingUser; ?>" target="_blank">
                        <div style="float: left; margin: 5px;">
                            <img src="/user/profileimg/50/<?php echo $r->followingUser; ?>" />
                            <span> <?php echo $r->firstName; ?> <?php echo $r->lastName; ?></span>
                        </div>
                    </a>
                <?php endforeach; ?>                              
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--container end-->

<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>UPLOADS</h2>
        <div class="follower-border-top">
            <?php if ($uploads): ?>
                <div class="follower_list">
                <?php foreach ($uploads as $upload): ?>
                    <div class="col-md-3" style="padding: 10px;">
                        <a href="/public/uploads/userimgs/<?php echo $id; ?>/<?php echo $upload->fileName; ?>" onclick="$(this).ekkoLightbox();return false;" data-title="<?php echo ($upload->caption) ? $upload->caption : 'No Title'; ?>">
                            <img src="/user/albumphoto/<?php echo $id; ?>/<?php echo $upload->fileName; ?>/250" />
                        </a>
                    </div>
                <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--container end-->


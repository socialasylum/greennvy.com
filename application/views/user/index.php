<?php if (!defined('BASEPATH')) die('Direct access not allowed'); ?>

<input type='hidden' name='id' id='id' value='<?= $id ?>'>
<input type='hidden' name='name' id='name' value="<?= $info->firstName ?> <?= $info->lastName ?>">
<input type='hidden' name='token' id="token" value='<?= $this->security->get_csrf_hash() ?>'>

<!--war_outer start-->
<div class="war_outer_3">
    <div class="container">
        <div class="banner_img_all"><img id="profileimg" src="/user/profileimg/300/<?php echo $info->id; ?>" />
            <svg:svg>
                <svg:filter id="profileimg">
                    <svg:feGaussianBlur stdDeviation="2"/>
                </svg:filter>
            </svg:svg>
        </div>
        <div class="banner_text"><?= $info->firstName ?> <?= $info->lastName ?></div>
        <div class="banner_location">
            <i class="fa fa-map-marker"></i>
            <?php echo $info->city; ?>, <?php echo $info->state; ?>
        </div>

        <?php if ($this->session->userdata('logged_in')): ?>
            <div class="follow_btn"><a class="followBtn" id="followBtn" href="#" onclick="<?php echo (($following) ? 'user.unfollow()' : 'user.follow()'); ?>"><?php echo (($following) ? 'unfollow' : 'follow'); ?></a></div>
        <?php endif; ?>
    </div>
</div>
<!--war_outer end-->

<!--middle_box start-->
<div class="middle_box">
    <div class="container">
        <div class="col-md-8 col-xs-8 box">
            <div class="user_list">
                <ul>
                    <li><a href="/user/activityfeed/<?php echo $id; ?>" id="activityBtn" class="tabs active">activity<span><?php echo $activityCnt; ?></span></a></li>
                    <li><a href="/user/reviews/<?php echo $id; ?>" id="reviewsBtn" class="tabs">reviews<span><?php echo $reviewCnt; ?></span></a></li>
                    <li><a href="/user/uploads/<?php echo $id; ?>" class="tabs">uploads<span><?php echo $uploadCnt; ?></span></a></li>
                    <?php /*<li><a href="/user/followers/<?php echo $id; ?>" class="tabs">followers<span><?php echo $followersCnt; ?></span></a></li>
                    <li><a href="/user/following/<?php echo $id; ?>" class="tabs">following<span><?php echo $followingCnt; ?></span></a></li>*/?>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-xs-4 box">
            <div class="user_icon_list">
                <ul>
                    <?php if ($this->session->userdata('logged_in')): ?>
                        <li><a data-toggle="modal" data-target="#sendMessage"><i class="fa fa-envelope icon_list"></i></a></li>
                    <?php endif; ?>
                   <!--  <li><a href="#"><i class="fa fa-user fa-stack-2x icon_list"></i></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!--middle_box end-->

<div id="ajaxSwap">
    
</div>
</div>
</div> <!-- /.user-tab-container -->

<!--message modal-->
<div class="modal fade" id='sendMessage'>
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-body bluespa-modal">
                
                                <div id='messageAlert'></div>
                
                                <form id='messageForm'>
                    
                                        <h2>To: <?= $info->firstName ?> <?= $info->lastName ?></h2>
                                        <div class="form-group">
                                                <label for="message">Message</label>
                                                <textarea class="form-control" id="message" placeholder="" rows="6"></textarea>
                                            </div>
                                        <input type="hidden" id="<?php echo $this->security->get_csrf_token_name()?>" name="<?php echo $this->security->get_csrf_token_name()?>" value="<?php echo $this->security->get_csrf_hash()?>">
                                        <input type="hidden" id="to" value="<?php echo $id; ?>">
                                        <input type="hidden" id="co" value="0">
                    
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary chat_profile_form_btn" onclick="sendMessage()">Submit</button>
                                            </form>
                
                                </form>
                            </div> <!-- modal-body -->
                    </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<!--message modal end-->






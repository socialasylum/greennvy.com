<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>FOLLOWERS</h2>
        <div class="follower-border-top">
            <?php if ($followers): ?>
                <div class="follower_list">
                <?php foreach ($followers as $r): ?>
                    <a href="/user/index/<?php echo $r->userid; ?>" target="_blank">
                        <div style="float: left; margin: 5px;">
                            <img src="/user/profileimg/50/<?php echo $r->userid; ?>" />
                            <span> <?php echo $r->firstName; ?> <?php echo $r->lastName; ?></span>
                        </div>
                    </a>
                <?php endforeach; ?>                              
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--container end-->

<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>ACTIVITY FEED</h2>
        <?php if ($activities): ?>
            <div class="follower_list">
            <?php foreach ($activities as $activity): ?>
                <?php $this->load->view('partials/single_activity', $activity); ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<!--container end-->


<!--message modal-->
<div class="modal fade" id='myLightBox'>
    <div class="modal-dialog" style="width: 80%; height: 80%;">
        <div class="modal-content" style="height: 100%;">
            <div class="row" style="height: 100%;">
                <div class="col-md-8" style="height: 100%; max-height: 100%;">
                    <table style="vertical-align: middle; text-align: center; width: 100%; height: 100%;">
                         <tr><td><img class="boxed_img" style="max-height: 700px; max-width: 700px;" ></td></tr>
                    </table>
                </div>
                <div class="col-md-4" style="height: 100%;">
                    <div style="min-height: 100%; max-height: 100%; padding: 5px 5px 5px 5px; background-color: #f5f5f5; position: relative; overflow-y: auto;">
                        <div class="icon_box-1" style="margin-right: 35px;"><div class="boxed_poster_img" style="margin-top: 5px;"></div></div>
                        <div class="boxed_headline" style="font-family: 'LatoRegular'; font-size: 25px; color: #7C9F03; "></div>
                        <div class="boxed_body" style="font-family: 'LatoRegular'; font-size: 18px;"></div>
                        <div class="comment_area" style="width: 95%; overflow-x: hidden;"></div>
                        <div class="comment_post" style="margin: 10px 0px 10px 0px;"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--message modal end-->

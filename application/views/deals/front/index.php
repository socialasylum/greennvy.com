<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<script type="text/javascript" src="/public/js/jquery-ui-1.9.0.custom.min.js" ></script>
<script type="text/javascript" src="/public/js/jquery-ui-tabs-rotate.js" ></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#featured").tabs({fx: {opacity: "toggle"}}).tabs("rotate", 5000, true);
    });
</script>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->
<style>
    #featured {border-color:#eee;}
</style>
<!--container start-->
<div class="container">
    <div class="war">
        <h2>DEALS</h2>
        <div class="clear"></div>
        <?php if($deals) : ?>
            <?php foreach ($deals as $deal): ?>
            <a href="/search/info/<?php echo $deal->location_id; ?>#deals">
                <div class="col-md-3 col-sm-6">
                    <div class="deals_inner">
                        <div class="deals_offer"><span>save</span><br />$<?php echo $deal->retail_price - $deal->discount_price; ?></div>
                        <div class="deals_img"><img src="/deals/dealimg/245/<?php echo $deal->userid; ?>/<?php echo $deal->deal_image; ?>" /></div>
                        <h4 class="deals_title"><?php echo $deal->deal_name ?></h4>
                        <div class="deals_ends"><?php echo number_format($deal->distance, 2); ?> mi away</div>
                    </div>
                </div>
            </a>
            <?php endforeach; ?>

        <?php else: ?>
                <div class="alert alert-warning text-center">There are no Deals Currently.</div>
        <?php endif; ?>

    </div>
</div>
<!--container end-->

<?php
/*
<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>NEWEST DEALS</h2>                   
        <?php if ($newest_deals): ?>
            <div class="slider-border-top">
                <div class="left-nav-btn-uploads" id="top-left-nav"></div>
                <div id="carousel6" class='outerWrapper'>
                    <?php foreach ($newest_deals as $r): ?>  
                        <div class="deals_item">
                            <div class="deals_inner">
                                <div class="deals_offer"><span>save</span><br />$<?php echo $r->retail_price - $r->discount_price; ?></div>
                                <div class="deals_img"><a href="#"><img src="/deals/dealimg/300/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" /></a></div>
                                <h4 class="deals_title"><a href="#"><?php echo $r->deal_name; ?></a></h4>
                                <div class="deals_ends"><a href="#"><?php echo $r->distance; ?> mi away</a><br/><?php echo $r->time_remaining; ?></div>
                                <p><?php echo $r->deal_description; ?></p>
                            </div>
                        </div> 
                    <?php endforeach; ?>
                </div> 
                <div class="right-nav-btn-uploads" id="top-right-nav"></div>                                               
            </div> 
        <?php else: ?>
            <li class="align-center list-unstyled">
                <div class="alert alert-warning" style="text-align:center">Sorry this company currently has no new deals.</div>
            </li>
        <?php endif; ?> 
    </div>              
</div>
<!--container end-->

<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>MOST POPULAR</h2>
        <div id="carouse2" class='dealsWrapper'>
            <?php if ($popular_deals): ?>
                <div class="slider-border-top">
                    <div class="left-nav-btn-uploads" id="top-left-nav"></div>
                    <div id="carousel7" class='outerWrapper'>
                        <?php foreach ($popular_deals as $r): ?>  
                            <div class="deals_item">
                                <div class="deals_inner">
                                    <div class="deals_offer"><span>save</span><br />$<?php echo $r->retail_price - $r->discount_price; ?></div>
                                    <div class="deals_img"><a href="#"><img src="/deals/dealimg/300/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" /></a></div>
                                    <h4 class="deals_title"><a href="#"><?php echo $r->deal_name; ?></a></h4>
                                    <div class="deals_ends"><a href="#"><?php echo $r->distance; ?> mi away</a><br/><?php echo $r->time_remaining; ?></div>
                                    <p><?php echo $r->deal_description; ?></p>
                                </div>
                            </div> 
                        <?php endforeach; ?> 
                    </div> 
                    <div class="right-nav-btn-uploads" id="top-right-nav"></div>                                               
                </div> 
            <?php else: ?>
                <li class="align-center list-unstyled">
                    <div class="alert alert-warning" style="text-align:center">Sorry no popular deals.</div>
                </li>
            <?php endif; ?>  
        </div>
    </div>               
</div>
<!--container end-->


<!--container start-->
<div class="container">
    <div class="war_bottom">
        <h2>ENDING SOON</h2>
        <div id="carouse3" class='dealsWrapper'>
            <?php if ($expiring_deals): ?>
                <div class="slider-border-top">
                    <div class="left-nav-btn-uploads" id="top-left-nav"></div>
                    <div id="carousel8" class='outerWrapper'>
                        <?php foreach ($expiring_deals as $r): ?>  
                            <div class="deals_item">
                                <div class="deals_inner">
                                    <div class="deals_offer"><span>save</span><br />$<?php echo $r->retail_price - $r->discount_price; ?></div>
                                    <div class="deals_img"><a href="#"><img src="/deals/dealimg/300/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" /></a></div>
                                    <h4 class="deals_title"><a href="#"><?php echo $r->deal_name; ?></a></h4>
                                    <div class="deals_ends"><a href="#"><?php echo $r->distance; ?> mi away</a><br/><?php echo $r->time_remaining; ?></div>
                                    <p><?php echo $r->deal_description; ?></p>
                                </div>
                            </div> 
                        <?php endforeach; ?> 
                    </div> 
                    <div class="right-nav-btn-uploads" id="top-right-nav"></div>                                               
                </div> 
            <?php else: ?>
                <li class="align-center list-unstyled">
                    <div class="alert alert-warning" style="text-align:center">No deals are about to expire.</div>
                </li>
            <?php endif; ?>  
        </div>
    </div>             
</div>
<!--container end-->
 */
?>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="container">
    <div class="war_bottom">
        <h2>DEALS NEAR YOU</h2>
        <div class="row">
            <div class="col-md-8">
                <script>
                    var isMobile = {
                        Android: function() {
                            return navigator.userAgent.match(/Android/i);
                        },
                        BlackBerry: function() {
                            return navigator.userAgent.match(/BlackBerry/i);
                        },
                        iOS: function() {
                            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                        },
                        Opera: function() {
                            return navigator.userAgent.match(/Opera Mini/i);
                        },
                        Windows: function() {
                            return navigator.userAgent.match(/IEMobile/i);
                        },
                        any: function() {
                            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                        }
                    };

                    var curSlide = 0;
                    $(document).ready(function() {
                        if( !isMobile.any()) {
                            advanceSlide();
                            setInterval(function () {
                                advanceSlide();
                            }, 3000);
                        }
                    });

                    function advanceSlide()
                    {
                        $('.deal-texts').removeClass('dealBG');
                        if (curSlide == 0)
                        {
                            curSlide = 1;
                            $('#shownImg-1').fadeIn('slow');
                            $('#deal-text-1').addClass('dealBG');
                        }
                        else
                        {
                            var nextSlide = (curSlide >= 3) ? 1 : curSlide + 1;
                            $('#shownImg-' + curSlide).fadeOut('slow', function() {
                                $('#shownImg-' + nextSlide).fadeIn('slow');
                                $('#deal-text-' + nextSlide).addClass('dealBG');
                                curSlide = nextSlide;
                            });
                        }
                    }
                </script>
                <?php
                $x = 1;
                foreach ($deals as $deal): ?>
                    <div id="shownImg-<?php echo $x++; ?>" style="display: none;" class="hidden-xs">
                        <a href="/search/info/<?php echo $deal->location_id; ?>#deals">
                            <?php if(strlen($deal->deal_image) > 1){ ?>
                                <img class="deal_rotate_img" src="/public/uploads/deals/<?php echo $deal->userid; ?>/<?php echo $deal->deal_image; ?>" style="width: 100%;" />
                            <?php } else { ?>
                                <img src="/deals/dealimg/300/170/" width="100%">
                            <?php } ?>
                            <div class="slider_sick">
                                <span>save</span>
                                $<?php echo $deal->retail_price - $deal->discount_price; ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-md-4 col-sm3" id="dealsSide">
                <?php
                $x = 1;
                foreach ($deals as $deal): ?>
                    <a href="/search/info/<?php echo $deal->location_id; ?>#deals">
                        <div class="row deal-texts" id="deal-text-<?php echo $x++; ?>">
                            <div class="title"><?php echo $deal->deal_name; ?></div>
                            <div class="subtitle"><span><?php echo $deal->location_name; ?> </span> (<?php echo number_format($deal->distance, 2); ?> mi.)</div>
                            <div class="desc"><span><?php echo $deal->deal_description ?></span></div>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <!--container start-->
    <div class="war_bottom">
        <h2>NEWEST DEALS</h2>
        <div class="row" id="dealsPageRow">
            <div class="col-md-12">
                <?php if ($newest_deals): ?>
                    <div class="slider-border-top">
                        <div class="left-nav-btn-uploads" id="top-left-nav"></div>
                        <div id="carousel6" class='outerWrapper'>
                            <?php foreach ($newest_deals as $r): ?>
                                <div class="deals_item">
                                    <div class="deals_inner">
                                        <div class="deals_offer"><span>save</span><br />$<?php echo $r->retail_price - $r->discount_price; ?></div>
                                        <div class="deals_img"><a href="/search/info/<?php echo $r->location_id; ?>#deals"><img src="/deals/dealimg/230/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" /></a></div>
                                        <h4 class="deals_title"><a href="/search/info/<?php echo $r->location_id; ?>#deals"><?php echo $r->deal_name; ?></a></h4>
                                        <div class="deals_ends"><a href="/search/info/<?php echo $r->location_id; ?>#deals"><?php echo $deal->location_name; ?> (<?php echo number_format($r->distance, 2); ?> mi.)</a></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="right-nav-btn-uploads" id="top-right-nav"></div>
                    </div>
                <?php else: ?>
                    <li class="align-center list-unstyled">
                        <div class="alert alert-warning" style="text-align:center">Sorry this company currently has no new deals.</div>
                    </li>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <!--container end-->

    <!--container start-->

    <div class="war_bottom">
        <h2>ENDING SOON</h2>
        <div class="row" id="dealsPageRow">
            <div class="col-md-12">
                <div id="carouse3" class='dealsWrapper'>
                    <?php if ($expiring_deals): ?>
                        <div class="slider-border-top">
                            <div class="left-nav-btn-uploads" id="top-left-nav"></div>
                            <div id="carousel8" class='outerWrapper'>
                                <?php foreach ($expiring_deals as $r): ?>
                                    <div class="deals_item">
                                        <div class="deals_inner">
                                            <div class="deals_offer"><span>save</span><br />$<?php echo $r->retail_price - $r->discount_price; ?></div>
                                            <div class="deals_img"><a href="/search/info/<?php echo $r->location_id; ?>#deals"><img src="/deals/dealimg/300/<?php echo $r->userid; ?>/<?php echo $r->deal_image; ?>" /></a></div>
                                            <h4 class="deals_title"><a href="/search/info/<?php echo $r->location_id; ?>#deals"><?php echo $r->deal_name; ?></a></h4>
                                            <div class="deals_ends"><a href="/search/info/<?php echo $r->location_id; ?>#deals"><?php echo $deal->location_name; ?> (<?php echo number_format($r->distance, 2); ?> mi.)</a></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="right-nav-btn-uploads" id="top-right-nav"></div>
                        </div>
                    <?php else: ?>
                        <li class="align-center list-unstyled">
                            <div class="alert alert-warning" style="text-align:center">No deals are about to expire.</div>
                        </li>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!--container end-->

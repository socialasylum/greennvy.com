<?php if(!defined('BASEPATH')) die('Direct access not allowed'); ?>
<div class="container">
	<h1>Privacy Policy</h1>
	<p>We take steps where possible to limit the personal data we collect. The following are the ways in which we may collect personal data:</p>
	<ol class="list-group">
                <li class="">
                    <p><strong>Visiting our website.</strong> We keep records of the activity that takes place on our website, including a record of Internet Protocol Addresses (IP addresses) used by website visitors and account holders. We use this information to analyze market trends, gather broad demographic information, and to prevent abuse of our services.</p>
                </li>
                <li class="">
                    <p>Creating an account. When you create an account, we do not require you to provide any personal data. As part of the account creation process, your IP address will be recorded. We use this information to analyze market trends, gather broad demographic information, and to prevent abuse of our services.</p>
                </li>
                <li class="">
                    <p>Making a purchase. When you make a purchase through the GreenNVY website, you will provide us with data that we use to process your payment such as your name, the account you are upgrading, the domain you wish to use for your email, alternate email address, your billing address and your credit card information. Additionally, we will record the IP address from where the payment is made. When we process your payment transaction, this payment information will be transmitted to our payment processor. We use third-party PCI compliant services to process your payment transaction. When we process your payment, we share your IP address, city, country, and postal code with a third – party anti-fraud service to determine the likelihood of the purchase being a fraudulent transaction. We do not store your credit card number on our servers.</p>
                </li>
                <li class="">
                    <p>Signing in to your account and our record of your activity. When you sign into your account, either by using a web browser or using other software, we will record certain information about your activity. When you perform actions such as reading or moving an email, we will also record these actions. We record this information to help resolve customer support queries, maintain services, and for the purpose of preventing abuse. Information we record may include your IP address, your browser type, browser language, date and time of the action, account usernames, sender and recipient email addresses, file names of attachments, subjects of emails, URLs in the bodies of unencrypted email, and any other information that we deem necessary to record for the purposes of maintaining the system and preventing abuse.</p>
                </li>
                <li class="">
                    <p>Communicating with us. When you communicate with us, you may provide us with personal data about yourself. Your communication with us may be retained in our system.</p>
                </li>
            </ol>
   <h2>How we store your data</h2>         
   	<ol class="list-group">
                <li class="">
                    <p>When you sign up for an account, you consent to your account and any other account data being stored on the GreenNVY servers.</p>
                </li>
                <li class="">
                    <p>If you have an encrypted email in your account, it will be stored on the GreenNVY servers encrypted. If you have an unencrypted email in your account, it will be stored on the GreenNVY servers unencrypted.</p>
                </li>
                <li class="">
                    <p>We do not store your passphrase on the GreenNVY servers. Instead, a hashed value is stored for authentication. The original passphrase cannot be determined from that hashed value. As a result, we are unable to recover a forgotten passphrase. Please note, we may be required to store a passphrase for an account identified in an order enforceable in British Columbia, Canada. (See the Disclosure of account data section below.)</p>
                </li>
    </ol>
	<h2>How we use your data</h2>
		<ol class="list-group">
                <li class="">
                    <p>We do not analyze the email in your account for the purpose of displaying advertisements.</p>
                </li>
                <li class="">
                    <p>We do not and will never share your account data with any third-party except as specified in this policy. We will never sell your account data under any circumstances.</p>
                </li>
                <li class="">
                    <p>You have the option to report email you receive as spam. Doing so will transmit a copy of the message you are reporting to the provider of the software we use on our servers to filter spam. Reporting email as spam improves the filter’s ability to detect email as spam.</p>
                </li>
                <li class="">
                    <p>If you send an email using Whisper Mail, your IP address will not appear in the headers of the email. The IP address that will appear in the headers of the email will be that of our servers. We keep a record of your IP address when you sign in to your account and send an email.</p>
                </li>
                <li class="">
                    <p>When you are signed into your account, GreenNVY displays your recent sign-in activity including the time, date, approximate geographic location, and the IP address of the ISP you used to access the Internet. We do this to assist you in identifying any unauthorized access to your account by a third party. The information we use to display this is gathered from our records; we do not track your actual location.</p>
                </li>
                <li class="">
                    <p>We use third-party services for some parts of our website such as our help system. When you use these services your account name and your name will be come part of your user account on that service.</p>
                </li>
                
                
    </ol>
    
    <h2>How long do we retain your data?</h2>
    <p>The following outlines how our data retention policy affects the email in your account and your account data:</p>
		<ol class="list-group">
                <li class="">
                    <p>Email in your account will stay in your account as long as your account is active. If you delete an email, or the entire contents of your account, it will be removed from your account at that time.</p>
                </li>
                <li class="">
                    <p>If you delete your account or request we delete your account for you, your account and the email in the account will be removed from our servers at the time of deletion. Deleting your account will not delete records of your activities.</p>
                </li>
                <li class="">
                    <p>The records we keep of your activities are permanently deleted after approximately 18 months. Records that are stored for statistical purposes may be kept indefinitely.</p>
                </li>
                <li class="">
                    <p>Your email and data may reside in our backups for a period of approximately three weeks subsequent to an email or an account being deleted.</p>
                </li>
                <li class="">
                    <p>Free accounts are deactivated if unused for a period of three weeks. Any email in the account will be deleted approximately 12 months after the account has been deactivated. Accounts that have been created and are never used may be deleted sooner. You can reactivate your account, and recover your email any time within 12 months by purchasing a subscription.</p>
                </li>
                <li class="">
                    <p>If you let your paid subscription lapse then your account will be downgraded to free account status approximately one week after your subscription has expired and will then be subject to the data retention rules for free accounts.</p>
                </li>
                <li class="">
                    <p>Whisper Mail Business customers are encouraged to delete their user accounts prior to canceling. If you do not, GreenNVY may delete your user accounts three weeks after cancellation.</p>
                </li><li class="">
                    <p>Whisper Mail Business customers whose Whisper Mail Business accounts have been deactivated for non-payment will have their user accounts deleted approximately six months from the date of deactivation.</p>
                </li>
                
    </ol>
    
    
	<h2>Cookies</h2>
	<p>We use cookies to manage your sessions, to remember your essential settings, and to help us better understand how people are using our services. This helps us provide a better experience to visitors of our website and to people using the service. In some cases where we use third-party services, such as our help system, these services may also use cookies.</p>
	<p>We do not include any personal data in cookies. We do not share cookie information with other websites under any circumstances.</p>
	<p>We take reasonable steps to ensure that the third-party services we use do not share information with other third parties. You should, however, verify this yourself by reviewing each site’s Privacy Policy.</p>
	<p>Most web browsers give you the ability to accept or decline the use of cookies through the browser preferences. If you disable the use of cookies you will be able to browse our website. You will, however, be unable to sign in to your account. If you have enabled your browser’s Do Not Track option, you will still be able to sign in to your account.</p>
	
	
	<h2>Advertising</h2>
	<p>We do not use any third-party advertising providers on our website.</p>
	
		<h2>Content analysis</h2>
	<p>We do not analyze your email for the purpose of displaying advertisements. Our spam filters analyze email for the purpose of preventing spam, viruses, and abuse.</p>
	
		<h2>Access to your personal data</h2>
	<p>We only collect personal data from users that are relevant to the purposes outlined above. We take reasonable steps to ensure that the personal data we collect is reliable, accurate, and complete. Users have the ability to access the personal data held in their customer record by logging into the billing section of our website. To request corrections or deletions of inaccurate data, contact us: info@greennvy.com</p>
	
<h2>Constant improvement</h2>
	<p>This privacy policy is under constant review and may be modified and updated in the future.</p>
	
	<h2>Contact us</h2>
	<p>We value your opinions and appreciate your comments. If you have any questions or concerns, please contact us: info@greennvy.com.</p>
</div>
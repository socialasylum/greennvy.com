<div class="container">
	<h1>GreenNVY Terms of Service</h1>
	<p>The following are the terms and conditions for use of the GreenNVY service including present and future features offered by GreenNVY and other services which may be offered from time to time by Whisper Mail Communications (“Whisper Mail”) for use with your GreenNVY account (each feature individually and collectively referred to as the “Service”). Please read them carefully. This Service is provided to individuals who are at least 18 years old or to minors who have parental consent to open and maintain an account.</p>
	<p>Each user is solely responsible for his or her transmissions through GreenNVY. As a condition of your use of the Service, you affirm to Whisper Mail, that you will not use the Service for any purpose that is unlawful or prohibited by these terms, conditions, and notices. The Service is provided to individuals only and for personal use only. Any unauthorized commercial use of the Service, or the resale of its services, is expressly prohibited. By registering an address with GreenNVY, you agree to be bound by the terms outlined in this Service Agreement. GreenNVY may, at its sole discretion, immediately terminate service without cause or notice, should user’s conduct fail to conform to these conditions.</p>
	<ol class="list-group">
                <li class="">
                    <p>We’re an Internet-based business and we use the Internet to send and receive certain types of communications. You agree to be bound by Internet regulations, policies and procedures.</p>
                </li>
                <li class="">
                    <p>You agree to comply with all applicable local, state, national and international laws and regulations. You also agree to not use GreenNVY for illegal purposes; to not interfere or disrupt networks connected to this service; and to comply with all the rules, policies and procedures connected to this service.</p>
                </li>
                <li class="">
                    <p>You will not use GreenNVY to send junk email, spam, chain letters or use email lists that contain any person that has not specifically agreed to be included on that list.</p>
                </li>
                <li class="">
                    <p>You agree not to transmit, or allow others to use your address to transmit, through GreenNVY, any objectionable material including, but not limited to, unlawful or harassing, libelous, abusive, threatening, harmful, vulgar or obscene material that encourages conduct that could constitute a criminal offence, give rise to civil liability or otherwise violate items discussed in #2 above.</p>
                </li>
                <li class="">
                    <p>You agree to not interfere with another member’s use and enjoyment of GreenNVY or another similar service.</p>
                </li>
                <li class="">
                    <p>You agree not to harvest or otherwise collect information about others, including email addresses, without their consent.</p>
                </li>
                <li class="">
                    <p>You will not create a false identity for the purpose of misleading others as to the identity of the sender or the origin of a message.</p>
                </li>
            </ol>
   <h2>GREENNVY SPAM POLICY</h2>        
   <p>Our spam policy at GreenNVY is “zero-tolerance”. Anyone found to be generating spam from within our system will have their user privileges revoked immediately. Persons attempting to forward spam email messages inbound to our system will be blocked and prevented from sending further email to any GreenNVY users. Please note, however, that many messages appearing to originate from Whisper Mail (or any other) mail servers could contain “forged headers”, written by the party originating the messages to make them appear to come from somebody else. This is done to avoid action normally taken against those abusing email systems and to cause damage to the reputation of the parties appearing to have sent the messages. Experienced investigators can tell immediately whether headers have been forged and where the message actually originated; thus, it is important that any mail forwarded to us for investigation contains the complete headers from the original message.</p> 
   
   <h2>ACCOUNT TERMINATION</h2>        
   <p>Whisper Mail may terminate your access to the Service and any related service(s) at any time, with or without cause, with or without notice, effective immediately, for any reason whatsoever. Whisper Mail has no obligation to store or forward the contents of your account.
If there is any indication that you are using your account for illegal activity, your account will be terminated immediately and without notice. Activities that are absolutely not tolerated include the purchase or sale of substances that are illegal in many jurisdictions, purchase or sale of stolen goods, making threats to person or property, possession or distribution of child pornography, and fraud.
</p> 
   
   <h2>SIGNING UP AND MAINTAINING AN ACCOUNT</h2>        
   <p>GreenNVY offers two types of accounts. Our regular account allows users to choose their own address (eg. contact@greenNVY.com). This account option requires that the user fill out a New Address form. The second type of account offered by GreenNVY is our auto-generated account, which automatically generates an address for the user (eg. auto555@greenNVY.com). The user is not required to give any information to GreenNVY or fill out a New Address form. You are entirely responsible for maintaining the confidentiality of your passphrase and account. Furthermore, you are entirely responsible for any and all activities that occur under your account.</p> 
   
   <h2>LIABILITY</h2>        
   <p>The information and services included in or available through the Service may include inaccuracies or errors. Whisper Mail may make improvements and/or changes to the Service at any time without notice. Whisper Mail does not guarantee that GreenNVY will be uninterrupted or error-free, that bugs or malfunctions will be corrected, or that the Service and its servers are free of harmful components. Whisper Mail does not guarantee that the uses of its Service, or the materials provided within the Service, are accurate, without error, or reliable. Whisper Mail provides its Service “as is”. The Company makes no warranty or covenant in regards to the availability, reliability, or timeliness of the Service. Whisper Mail is not responsible for the appropriateness of the Service for any one purpose. Whisper Mail will not be held responsible for any and all damages whatsoever as a result of the loss of use, data or profits connected to the performance of GreenNVY. The user is solely responsible for any and all information that passes through the GreenNVY servers. Whisper Mail is not responsible or liable for any product or service acquired or requested using the Service. The sole remedy is for users to discontinue their use of GreenNVY and its related Web sites.</p> 
   
   <h2>INDEMNIFICATION</h2>        
   <p>You agree that Whisper Mail, its parents, subsidiaries, officers and employees, cannot be held responsible for any third party claim, demand, or damage, including reasonable attorneys’ fees, arising out of your use of the Service.</p> 
   
   <h2>MESSAGE STORAGE, SENDING MESSAGES AND MESSAGING LIMITATIONS</h2>        
   <p>The amount of email storage space per member is limited; please visit our FAQ for more information. Some email messages may not be processed due to space constraints or outbound message limitations. You agree that Whisper Mail is not responsible or liable for the deletion or failure to store messages or other information.</p> 
   
   <h2>THIRD PARTY SITES, ADVERTISERS, AND PROMOTIONS</h2>        
   <p>The links included within this Web site may allow you leave the GreenNVY site and visit third party sites. These third party sites are not under the control of Whisper Mail. Whisper Mail is not responsible for the contents of any third party site nor is Whisper Mail responsible for the functionality of any third party site. Whisper Mail provides links to our service to you strictly as a convenience. A link’s inclusion in a Whisper Mail site does not endorse the site or their operators. Any promotions sponsored by Advertisers to the Service or dealings with the Advertisers directly are solely between you and that third party. Whisper Mail is not responsible in any part of any such connections or dealing with advertisers or their promotions.</p> 
   
   <h2>MODIFICATIONS TO TERMS OF SERVICE, MEMBER POLICIES</h2>        
   <p>Whisper Mail reserves the right to review and change this agreement regarding the use of the Service at any time and to notify you by posting an updated version of the agreement on this Web site. You are responsible for regularly reviewing Whisper Mail policies. Continued use of the Service after any such changes shall constitute your consent to such changes.</p> 
   
   <p>Any rights not expressly granted herein are reserved.</p>
   
   </div>
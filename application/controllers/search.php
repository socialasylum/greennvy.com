<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends CI_Controller {

    function Search() {
        parent::__construct();

        $this->load->driver('cache');
        $this->load->model('search_model', 'search', true);
        $this->load->model('profile_model', 'profile', true);
        $this->load->model('menu_model', 'menu', true);
        $this->load->model('deals_model', 'deals', true);
        $this->load->model('location_hours_model', 'location_hours', true);
        $this->load->model('locations_model', 'locations', true);
        $this->load->model('reviews_model', 'reviews', true);
        $this->load->model('user_model', 'user', true);
        $this->load->model('dojos_model', 'dojos', true);
    }

    public function index()
    {
        $header['headscript'] = $this->functions->jsScript('search.js welcome.js dashboard.js');
        $header['googleMaps'] = true;
        try {
            $geoip = $this->search->grabGeoIp();
            $body['lat'] = $bodyListings['lat'] = $lat = $geoip->lat;
            $body['lng'] = $bodyListings['lng'] = $lng = $geoip->lng;
            $header['onload'] = "search.indexInit(" . urldecode($lat) . ", " . urldecode($lng) . ");";
            $bodyListings['places'] = $this->search->searchLocations();
            $body['initListings'] = $this->load->view('locations/listings', $bodyListings, true);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
        
        $this->load->view('template/header', $header);
        $this->load->view('welcome/landing', $body);
        $this->load->view('template/footer');
    }

    public function checkGeo()
    {
        $result = ($this->session->userdata('location') !== false);
        $this->functions->jsonReturn($result);
    }
    
    public function listings() {
        set_time_limit(60); // allow 1 min to download content
        try {
            $places = $this->places->getNextPage(urldecode($_GET['next_page_token']));
            var_dump($places);
            exit;
            $body['lat'] = urldecode($_GET['lat']);
            $body['lng'] = urldecode($_GET['lng']);
            $body['places'] = $places;
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
        $this->load->view('search/listings', $body);
    }

    public function info($id)
    {
        $header['headscript'] = $this->functions->jsScript('search.js');
        $header['locationsjs'] = true;
        $header['justgal'] = true;
        $header['googleMaps'] = true;
        $header['lightbox'] = true;
        $header['resizeimage'] = true;
        $header['message'] = true;
        $header['canonical'] = "/search/info/{$id}";
        try {
            $body['id'] = $id;
            $body['info'] = $info = $this->locations->getLocationById($id);
            $body['name_class'] = 'lg';
            if (strlen($info->name) > 20)
            {
                $body['name_class'] = 'md';
            }
            if (strlen($info->name) > 35)
            {
                $body['name_class'] = 'sm';
            }
            if (strlen($info->name) > 50)
            {
                $body['name_class'] = 'xsm';
            }
            $img = $this->locations->getCoverImage($id);
            if($img){
                $body['headerBanner'] = '/locations/coverImg/'.$id;
            }else{
                $body['headerBanner'] = '/public/images/default_loc_cover.jpg';
            }
            $body['defaultImg'] = $this->search->getLocationMainImage($id);
            $body['menu'] = $this->menu->getMenu($id);
            $body['menuOptions'] = $this->menu->getMenuOptions();
            $body['deals'] = $this->deals->getDealsByLocation($id);
            $body['followersCnt'] = $this->user->getFollowersCnt($id);
            $body['followingCnt'] = $this->user->getFollowingCnt($id);
            if ($this->session->userdata('userid') > 0)
            {
                $body['isFollowing'] = $this->locations->isFollowing($id);
            }
            else
            {
                $body['isFollowing'] = null;
            }
            //get videos
            //$body['videos'] = $this->search->getLocationVideos($id);
            $body['reviews'] = $this->search->getReviews($id);
            $body['assigned'] = $this->search->checkLocationAssigned($id);
            //$body['images'] = $this->search->getLocationImages($id);
            $body['uploadsTotal'] = $this->locations->getUploadCount($id);
            $location_hours = $this->location_hours->getLocationHoursByLocationId($id);
            foreach ($location_hours as $key => $val) {
                $body['open'] = $location_hours->day_open;
                if (stristr($key, 'time') && $val != 'closed')
                    $location_hours->$key = trim(date('h:i a', strtotime($val)), '0');
            }
            $times['locationHoursLoop'][] = time();
            $body['hours'] = $location_hours;
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
        $header['title'] = $body['info']->name;
        $header['onload'] = "search.infoInit({$info->lat}, {$info->lng});";
        $videoBody['showDelete'] = false;
        $body['menu_userlist'] = $this->load->view('partials/menu_userlist', $body, true);
        $footer['photoAdjust'] = true;
        $this->load->view('template/header', $header);
        $this->load->view('search/info', $body);
        $this->load->view('template/footer', $footer);
    }

    public function followers($id) {
        $body['info'] = $this->dojos->getLocationInfo($id);
        //var_dump($body); exit;
        $body['followers'] = $this->dojos->getFollowers($id);
        $body['followings'] = $this->dojos->getFollowing($id);
        $body['followersCnt'] = count($followers);
        $body['followingCnt'] = count($followings);

        $this->load->view('template/header', $header);
        $this->load->view('user/followers', $body);
        $this->load->view('template/footer');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savereview() {
        if ($_POST) {
            try {
                // saves comment
                $this->search->insertComment($_POST);
                $review['r'] = new StdClass();
                $reviewInfo['r']->name = $_POST['reviewName'];
                $reviewInfo['r']->rating = intval($_POST['rating']);
                $reviewInfo['r']->comment = $_POST['reviewDesc'];
                $reviewInfo['r']->created = $this->search->getCreatedHours(time());
                $singleReview = $this->load->view('partials/single_review', $reviewInfo, true);
                $this->functions->jsonReturn('SUCCESS', 'Comment has been submitted!', true, 0, array('reviewHTML' => $singleReview));
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function uploads_content($id) {
        $body['videos'] = $this->locations->getLocationVideos($id);
        $body['images'] = $this->locations->getImages($id);
        $this->load->view('partials/uploads_content', $body);
    }

    public function menu_content($id) {
        $body['info']['info'] = $info = $this->locations->getLocationById($id);
        $body['location_info'] = $this->search->getLocationInfo($id);
        $body['menu_items'] = $this->menu->getMenu($id);

        //loop over menu items, if they have a value of 0 convert them to 'n/a'
        foreach($body['menu_items'] as $item) {
            $item->per_g = ($item->per_g == 0) ? 'n/a' : '$'.$item->per_g;
            $item->per_eighth = ($item->per_eighth == 0) ? 'n/a' : '$'.$item->per_eighth;
            $item->per_quarter = ($item->per_quarter == 0) ? 'n/a' : '$'.$item->per_quarter;
            $item->per_half = ($item->per_half == 0) ? 'n/a' : '$'.$item->per_half;
            $item->per_oz = ($item->per_oz == 0) ? 'n/a' : '$'.$item->per_oz;
            $item->per_each = ($item->per_each == 0) ? 'n/a' : '$'.$item->per_each;
        }

        $body['product_types'] = $this->search->getProductTypes();
        $this->load->view('partials/menu_content', $body);
    }

    public function img($location, $size = 50, $id = 0) {

        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $location . DS;

        try {
            if (empty($location))
                throw new Exception("Location ID is empty");


            if (empty($id))
                $img = $this->search->getLocationMainImage($location);
            else
                $img = $this->search->getImageByID($location, $id);

            //$img = $this->users->getTableValue('profileimg', $userid);
            // no profile image is set - loads no profile img
            if (empty($img)) {
                $path = $_SERVER["DOCUMENT_ROOT"] . DS . 'public' . DS . 'images' . DS;
                $img = 'gst_no_profile.jpg';
            }

            $is = getimagesize($path . $img);

            if ($is === false)
                throw new exception("Unable to get image size for ({$path}{$img})!");

            $ext = $this->functions->getFileExt($img);

            list ($width, $height, $type, $attr) = $is;

            if ($width == $height) {
                $nw = $nh = $size;
            } elseif ($width > $height) {
                $scale = $size / $height;
                $nw = $width * $scale;
                $nh = $size;
                $leftBuffer = (($nw - $size) / 2);
            } else {
                $nw = $size;
                $scale = $size / $width;
                $nh = $height * $scale;
                $topBuffer = (($nh - $size) / 2);
            }

            $leftBuffer = $leftBuffer * -1;
            $topBuffer = $topBuffer * -1;

            if ($ext == "JPG")
                $srcImg = imagecreatefromjpeg($path . $img);
            if ($ext == "GIF")
                $srcImg = imagecreatefromgif($path . $img);
            if ($ext == "PNG")
                $srcImg = imagecreatefrompng($path . $img);
            $destImg = imagecreatetruecolor($size, $size); // new image
            #imagecopyresized($destImg, $srcImg, 0, 0, $leftBuffer, $topBuffer, $nw, $nh, $width, $height);
            imagecopyresized($destImg, $srcImg, $leftBuffer, $topBuffer, 0, 0, $nw, $nh, $width, $height);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        #echo "NW: {$nw} | NH: {$nh} | Scale: {$scale} | ";
        #echo "topBuffer: {$topBuffer}";
        #echo "leftBuffer: {$leftBuffer}";
        header('Content-Type: image/jpg');
        imagejpeg($destImg);

        imagedestroy($destImg);
        imagedestroy($srcImg);
    }

    public function imgresize($location) {
        try {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $_SERVER['DOCUMENT_ROOT'] . 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $location . DS . urldecode($_GET['fileName']);
            //$config['create_thumb'] = true;
            $config['maintain_ratio'] = true;
            $config['width'] = 730;
            $config['dynamic_output'] = true;
            $config['height'] = 300;

            $this->load->library('image_lib', $config);

            $this->image_lib->resize();
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
    }

    public function getGeoIP($show = false) {
        $this->session->set_userdata('location', $this->search->grabGeoIP());
        if ($show)
        {
            echo ($this->session->userdata('location')) ? json_encode($this->session->userdata('location')) : false;
        }
        return $this->session->userdata('location');
    }

    public function deactiveLocation() {
        if ($_POST) {
            try {
                if (!$this->session->userdata('admin'))
                    throw new Exception("Action not allowed!");

                $this->search->deactivateLocation($_POST['location']);

                $this->functions->jsonReturn('SUCCESS', 'Location has been deactivated!');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function about_us($id)
    {
        $location_hours = $this->location_hours->getLocationHoursByLocationId($id);
        foreach ($location_hours as $key => $val)
        {
            $body['open'] = $location_hours->day_open;
            if (stristr($key, 'time') && $val != 'closed')
            {
                $location_hours->$key = trim(date('h:i a', strtotime($val)), '0');
            }
        }
        $body['hours'] = $location_hours;
        $body['info'] = $this->locations->getLocationById($id);
        $this->load->view('partials/about_us', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function saveclaim() {
        if ($_POST) {
            try {
                $this->search->insertclaim($_POST);
                $this->functions->jsonReturn('SUCCESS', 'Claim has been submitted!');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function upatefromgoogle($location) {
        try {
            $this->search->updateLocationFromGooglePlaces($location);
            $this->functions->jsonReturn('SUCCESS', 'Location Updated from Google Places!');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function searchResults()
    {
        if ($_POST)
        {
            $dispensaries = ($_POST['dispensaries'] == 'true') ? true : false;
            $doctors = ($_POST['doctors'] == 'true' ? true : false);
            $results['places'] = $this->search->searchLocations($_POST['searchTerm'], $dispensaries, $doctors, $_POST['orderby'], $_POST['order']);
            $results['lat'] = $this->session->userdata('location')->lat;
            $results['lng'] = $this->session->userdata('location')->lng;
            $results['searchterm'] = $_POST['searchTerm'];
            echo $this->load->view('locations/listings', $results, true);
        }
    }
    
    public function saveGeoCode() {
        $results = false;
        try {
            $lng = floatval($_GET['lng']);
            $lat = floatval($_GET['lat']);
            $results = $this->search->mapQuestReverseGeoCode($lat, $lng);
            $this->session->set_userdata('location', $results);
            $citystate = strtolower(str_replace(' ', '-', $results->city . '-' . $results->state));
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
        echo $citystate;
    }
    
    public function test()
    {
        $results = $this->search->searchLocations('Los Angeles');
        $lat = $lng = 0;
        foreach ($results as $result)
        {
            $lat += floatval($result->lat);
            $lng += floatval($result->lng);
        }
        echo "Average Lat: " . ($lat / count($results));
        echo "Average Lng: " . ($lng / count($results));
    }
}

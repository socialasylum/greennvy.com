<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . 'third_party' . DS . 'bms' . DS . 'index.php';

class User extends CI_Controller {

    //if (trait_exists('Chat')) use Chat;
    //use chat;

    function User() {
        parent::__construct();

        $this->load->driver('cache');

        $this->load->model('user_model', 'user', true);
        $this->load->model('blog_model', 'blog', true);
        $this->load->model('profile_model', 'profile', true);
        $this->load->model('dojos_model', 'dojos', true);
        $this->load->model('events_model', 'events', true);
        $this->load->model('wall_model', 'wall', true);
        $this->load->library('library');
        $this->load->library('users');
        //$this->load->library('chat');
    }

    public function index($id = 0, $page = 1, $tab = 0, $month = null, $year = null) {

        if (empty($id) && $this->session->userdata('logged_in') == true)
            $id = $this->session->userdata('userid');

        $header['headscript'] = $this->functions->jsScript('user.js');
        $header['headscript'] .= $this->functions->jsScript('wall.js');
        $header['ckeditor'] = true;
        $header['slimscroll'] = true;
        $header['autosize'] = true;
        $header['nailthumb'] = true;
        $header['message'] = true;
        $header['spin'] = true;
        $header['justgal'] = true;
        $header['lightbox'] = true;
        $header['onload'] = "user.indexInit();";
        $body['page'] = $page;
        $body['month'] = $month = (empty($month)) ? date("n") : $month;
        $body['year'] = $year = (empty($year)) ? date("Y") : $year;
        $body['tab'] = $tab;
        $body['id'] = $id;

        try {
            $body['info'] = $this->functions->getUserInfo($id);
            $body['blogs'] = $this->user->getBlogs($id, $page);
            $body['userStyles'] = $this->dojos->getUserCodes($id, 26);
            //$body['followersCnt'] = $this->user->getFollowersCnt($id);
            //$body['followingCnt'] = $this->user->getFollowingCnt($id);
            $body['uploadCnt'] = $this->user->getUploadCnt($id);
            $body['reviewCnt'] = $this->user->getReviewCnt($id);
            $body['activityCnt'] = $this->user->getActivityCnt($id);
            $body['albums'] = $this->user->getAlbums($id);
            if ((int) $this->session->userdata('userid') !== $id)
            {
                $body['following'] = $this->user->checkFollowingUser($id);
            }
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $header['title'] = $body['info']->firstName.' '.$body['info']->lastName;
        $body['followers']['followers'] = $this->user->getFollowers($id);
        $body['followings']['followings'] = $this->user->getFollowing($id);
        $body['reviews']['reviews'] = $this->user->getReviews($id);
        $this->load->view('template/header', $header);
        $this->load->view('user/index', $body);
        $this->load->view('template/footer');
    }

    public function followers($id)
    {
        $body['followers'] = $this->user->getFollowers($id);
        $this->load->view('user/followers', $body);
    }
    
    public function following($id)
    {
        $body['following'] = $this->user->getFollowing($id);
        $this->load->view('user/following', $body);
    }
    
    public function reviews($id)
    {
        $body['reviews'] = $this->user->getReviews($id);
        $this->load->view('user/reviews', $body);
    }

    public function uploads($id)
    {
        $body['uploads'] = $this->user->getUploads($id);
        $body['id'] = $id;
        $this->load->view('user/uploads', $body);
    }
    
    public function activityfeed($id)
    {
        $body['activities'] = $this->wall->recentActivity($id);

        foreach($body['activities'] as $act) {
            if($this->session->userdata('userid')) {
                $act->likecnt = $this->wall->getLikeCnt($act->postId);
                $act->usrLiked = $this->wall->checkPostLiked($this->session->userdata('userid'), $act->postId);
                $act->comments = $this->wall->getComments($act->postId);
            }
        }
        $this->load->view('user/activityfeed', $body);
    }
    
    public function blog_feature_img($blog)
    {
        try {
            $logo = $this->companies->getTableValue('logo', $company);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $path = $this->config->item('bmsUrl') . '/public/uploads/blog/';
        $config['image_library'] = 'gd2';
        $config['source_image'] = './' . $path . $logo;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 100;
        $config['height'] = 50;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function profileimg($size = 50, $userid = 0, $file = null)
    {
        if (empty($userid))
            $userid = $this->session->userdata('userid');

        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'avatars' . DS . $userid . DS;

        if (!empty($file))
            $file = urlencode($file);

        try {

            if (!empty($userid))
                $img = $this->users->getTableValue('profileimg', $userid);

            if (!empty($file))
                $img = $file;

            // checks if file exists
            if (!file_exists($path . $img))
                $img = null;

            // no profile image is set - loads no profile img
            if (empty($img)) {
                $path = $_SERVER["DOCUMENT_ROOT"] . DS . 'public' . DS . 'images' . DS;
                $img = 'dude.gif';
            }

            $is = getimagesize($path . $img);

            if ($is === false)
                throw new exception("Unable to get image size for ({$path}{$img})!");

            $ext = PHPFunctions::getFileExt($img); 

            list ($width, $height, $type, $attr) = $is;

            if ($width == $height) {
                $nw = $nh = $size;
            } elseif ($width > $height) {
                $scale = $size / $height;
                $nw = $width * $scale;
                $nh = $size;
                $leftBuffer = (($nw - $size) / 2);
            } else {
                $nw = $size;
                $scale = $size / $width;
                $nh = $height * $scale;
                $topBuffer = (($nh - $size) / 2);
            }

            $leftBuffer = $leftBuffer * -1;
            $topBuffer = $topBuffer * -1;

            if ($ext == "JPG")
                $srcImg = imagecreatefromjpeg($path . $img);
            if ($ext == "GIF")
                $srcImg = imagecreatefromgif($path . $img);
            if ($ext == "PNG")
                $srcImg = imagecreatefrompng($path . $img);

            $destImg = imagecreatetruecolor($size, $size); // new image
            imagecopyresized($destImg, $srcImg, $leftBuffer, $topBuffer, 0, 0, $nw, $nh, $width, $height);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }

        header('Content-Type: image/jpg');
        imagejpeg($destImg);
        imagedestroy($destImg);
        imagedestroy($srcImg);
    }

    public function viewblog($blog) {
        try {
            $body['info'] = $this->blog->getBlogInfo($blog);
            $body['comments'] = $this->blog->getComments($blog);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('user/viewblog', $body);
    }

    public function follow() {
        if ($_POST) {  
            try {
                $followID = $this->user->followUser($this->session->userdata('userid'), $_POST['userid']);
                $this->functions->jsonReturn('SUCCESS', $followID);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function unfollow() {
        if ($_POST) {
            try {
                $this->user->unfollowUser($this->session->userdata('userid'), $_POST['userid']);
                $this->functions->jsonReturn('SUCCESS', "You are no longer following this user!");
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function createalbum() {
        $logged_in = $this->functions->checkLoggedIn(false);

        if (!$logged_in)
            $this->functions->jsonReturn('ERROR', "You are not logged in!");

        if ($_POST) {
            try {
                $albumID = $this->user->createPhotoAlbum($this->session->userdata('userid'), $_POST['albumName']);
                $this->functions->jsonReturn('SUCCESS', $albumID);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function viewalbum($album, $userid) {
        $body['album'] = $album;
        $body['userid'] = $userid;
        try {
            $body['photos'] = $this->user->getAlbumPhotos($album);

            $body['info'] = $this->user->getAlbums($userid, $album);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('user/viewalbum', $body);
    }

    public function photoinfo($photoID) {
        $body['photoID'] = $photoID;

        try {
            $body['info'] = $info = $this->wall->getPhotoInfo($photoID);
            // gets name of user who uploaded the photo
            $body['usersname'] = $this->functions->getUserName($info->uploadedBy);
            $tzUserid = ($this->session->userdata('logged_in') == true) ? $this->session->userdata('userid') : $info->uploadedBy;
            $body['date'] = $this->functions->convertTimezone($tzUserid, $info->datestamp, "F j Y g:iA");
            $body['comments'] = $this->user->getPhotoComments($photoID);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('user/photo_modal_info', $body);
    }

    public function albumphoto ($user, $file, $size = 80)
    {
    	$file = str_replace('_t.', '.', $file);
    	$file = str_replace('_m.', '.', $file);    
    	$file = str_replace('_n.', '.', $file);
    	$file = str_replace('_z.', '.', $file);
    	$file = str_replace('_c.', '.', $file);
    	$file = str_replace('_b.', '.', $file);
        $path = $_SERVER['DOCUMENT_ROOT'] . 'public' . DS . 'uploads' . DS . 'userimgs' . DS . $user . DS;
        $config['image_library'] = 'gd2';
        $config['source_image']= $path . $file;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['dynamic_output'] = true;
        $config['width'] = $size;
        $config['height'] = $size;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }    
    
    public function uploadimgs() {
        $logged_in = $this->functions->checkLoggedIn(false);

        if (!$logged_in)
            $this->functions->jsonReturn('ERROR', "You are not logged in!");

        if ($_FILES)
        {
            try {

                $path = 'public' . DS . 'uploads' . DS . 'userimgs' . DS . $_POST['userid'] . DS;

                // ensures company uploads directory has been created
                $this->functions->createDir($path);
                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "gif|jpg|png";
                $config['max_size'] = "5120";
                $config['encrypt_name'] = true;

                // loads upload library
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file')) {
                    throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                }

                $uploadData = $this->upload->data();

                // will ensure stream folder is available
                $streamAlbumCheck = $this->user->checkAlbumType($_POST['userid'], 'stream');

                // create album if it does not exist
                if (!$streamAlbumCheck) {
                    $streamAlbum = $this->user->createPhotoAlbum($_POST['userid'], 'Stream Photos', 1);
                } else {
                    $streamAlbum = $this->user->getAlbumTypeID($_POST['userid'], 'stream');
                }

                // save user img
                $this->user->insertAlbumPhoto($_POST['userid'], $streamAlbum, $uploadData['file_name']);

                // now send image to BMS
                //$this->user->sendImgToBMS($path, $uploadData['file_name'], $_POST['userid']);

                $this->functions->jsonReturn('SUCCESS', $uploadData['file_name']);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function savephotocomment() {
        if ($_POST) {
            try {
                $commentID = $this->user->savePhotoComment($_POST);

                $this->functions->jsonReturn('SUCCESS', $commentID);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function test($id)
    {
        var_dump($this->wall->getWallPosts($id));
    }
    
    public function chat($method) {
        $args = func_get_args();
        unset($args[0]);
        $this->chat->$method($args);
    }
}

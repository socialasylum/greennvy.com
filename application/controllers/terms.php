<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Terms extends CI_Controller {

    function Terms()
    {
        parent::__construct();
    }

    public function index()
    {
	    $header['title'] = "Terms and Conditions";
        $this->load->view('template/header', $header);
        $this->load->view('legal/terms');
        $this->load->view('template/footer');
    }
}
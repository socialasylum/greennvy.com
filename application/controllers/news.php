<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller {

    function News()
    {
        parent::__construct();
    }

    public function index()
    {
	    $header['title'] = "News";
	    $this->load->view('template/header', $header);
        $this->load->view('social/news');
        $this->load->view('template/footer');
    }
}
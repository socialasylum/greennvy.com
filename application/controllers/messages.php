<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messages extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('messages_model', 'messages', true);

    }

    /*
     * UI and front end controller functions
     */

    //////////////////////UI START///////////////////////////
    public function index(){

        //check for a logged in user
        $this->functions->checkLoggedIn();

        //get the current user id
        $user = $this->session->userdata('userid');

        //get the chat threads and the one that was updated recently to show on the page when its loaded
        $chat['chat_list'] = $this->messages->getChatThreads($user);

        //we need to decrypt the message for display
        foreach($chat['chat_list'] as $lastMessage) {
            $lastMessage->last_message = (strlen($lastMessage->last_message) > 0) ? trim($this->decryptMessage($lastMessage->last_message)) : '';

            $lastMessage->profileimg = $this->processPictureUrl($lastMessage, 1);

        }

        //load the full name of the newest chat partner on the UI for when the page opens
        $chat['load_name'] = ucfirst($chat['chat_list'][0]->partner_first_name) .' '. ucfirst($chat['chat_list'][0]->partner_last_name);

        //we need to get the first list of chats based on what ever was updated closest to the current time, get the first item from that chat list and query for its information
        //we dont want to do more queries than we half to so get it from the $chat['chat_list'] array value
        $chat['chat_id'] = $chat['chat_list'][0]->chat_id;
		$chat['messages'] = $this->messages->getChat($chat['chat_id']);

        //set messages as received
        $this->messages->setRecived($chat['chat_id'], $user);

        //we need to decrypt the chat messages for display do it in a for each loop
        foreach($chat['messages'] as $message) {
            $message->message = trim($this->decryptMessage($message->message));
            $message->sender_first_name = $this->formatFirstName($message->sender_first_name);

            //format the image
            $message->profileImg = $this->processPictureUrl($message);
        }

        //get the list that this user can message
        $chat['messageable'] = $this->messages->getFollowersAndDispensaries($user);

        //load the view for a logged in user
        $this->load->view('template/header', $header);
        $this->load->view('messages/index', $chat);
        $this->load->view('template/footer');

    }
    //////////////////////UI END////////////////////////////

    /*
     * Encryption Methods
     */

    ///////////////////////Encryption Start/////////////////

    /*
     * encrypts a message
     */
    protected function encryptMessage($text){
        $encryption_key = READ_LINE;
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, $text, MCRYPT_MODE_ECB, $iv);
        return $encrypted_string;
    }

    /*
     * decrypts a message
     */
    protected function decryptMessage($text)
    {
        $encryption_key = READ_LINE;
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $text, MCRYPT_MODE_ECB, $iv);
        return $decrypted_string;
    }

    ////////////////Encryption End///////////////////////

    /*
     * Messaging Functions
     */
    ///////////////Messaging Start//////////////////////


     /*
      * If the message is not in a popup
      */
    public function sendchat() {

        //get the post items
        $data['message'] = $this->encryptMessage(htmlentities($this->input->post('message')));
        $data['chat_id'] = $this->input->post('cid');
        $data['sender'] = $this->session->userdata('userid');
        $data['reciever'] = $this->input->post('reciever');
        $data['company'] = $this->input->post('co');

        
        //send the query and update the time in the chat_ids table if successful if not fail. No need to make a new data array, the chat id is already contained in $data.
        if($this->messages->sendmessage($data)){
            $this->messages->updateChatThread($data);

            //get the information of the user for the return for correct labeling
            $info = $this->messages->getInfo($data);

            if($info->company){
                $name = $this->formatFirstName($info->sender_first_name);
                if($info->profileImg){
                    $img = '/public/uploads/locationImages/' . $data->sender . '/' . $data->profileImg;
                } else{
                    $img = '/public/images/gst_no_profile.jpg';
                }
            } else {
                $name = $info->sender_first_name .' '. $info->sender_last_name;
                $img = '/user/profileimg/50/'.$data['sender'];
            }

            //get the company info. this should only happen once (new chat string)
            if($data['company'] == '1') {
                $companyImg = $this->messages->coImg($data['reciever']);

                if($companyImg){
                    $recImg = '/public/uploads/locationImages/' . $data['reciever'] . '/' . $companyImg->fileName;
                } else{
                    $recImg  = '/public/images/gst_no_profile.jpg';
                }

            }



            //create a return array
            $return = array (
                'result' => 'success',
                'timestamp' => date("n/j g:ia"),
                'senderId' => '',
                'name' => $name,
                'img' => $img,
                'recimg' => $recImg
            );

            //encode it to json for the front end
            echo json_encode($return);
        } else {
            echo 'fail';
        }
    }

    /*
     * gets the new messages from another user.
     */

    public function getNewChatMessages() {

        //get needed variables
        $data['chat_id'] = $this->input->post('id');
        $data['current_user'] = $this->session->userdata('userid');

        //decode the messages
        $messages = $this->messages->get_new_messages($data);

        //decode the message, change the date time format and make sure that the message has been marked as set
        foreach($messages as $decode) {

            if($decode->senderCompany && !$decode->profileImg) {
                $decode->profileImg = '/public/images/gst_no_profile.jpg';
            } else if($decode->senderCompany && $decode->profileImg) {
                $decode->profileImg = '/public/uploads/locationImages/' . $decode->senderCompany . '/' . $decode->profileImg;
            } else {
                $decode->profileImg = '/user/profileimg/50/'.$decode->sender;
            }
            unset($decode->sender);
            unset($decode->senderCompany);
            $decode->firstname = $this->formatFirstName($decode->firstname);
            $decode->message = trim($this->decryptMessage($decode->message));
            $decode->date = date("n/j g:ia", strtotime($decode->date));

        }


       echo json_encode($messages);
    }

    /*
     * Gets the messages when a new thread is selected
     */

    protected function formatFirstName($data) {

        //format the name so that the first letter is capitalised
        $name = ucwords(strtolower($data));
        $arr = explode(' ', trim($name));
        if(count($arr) > 1){
            return $arr[0].' '. $arr[1];
        } else {
            return $data;
        }
    }

    /*
     * Process the picture to the right location. data should include the id of the user or company and a file name if it is a company. Method determines or the right chat  panel ($method = 0 : default) or the left side bar ($method = 1)
     */

    protected function processPictureUrl($data, $method = 0)
    {
        if ($method == '0') {

            if ($data->profileImg && !$data->sender_last_name) {
                return '/public/uploads/locationImages/' . $data->sender . '/' . $data->profileImg . '.jpg';
            } else if (!$data->profileImg && !$data->sender_last_name) {
                return '/public/images/gst_no_profile.jpg';
            } else {
                return '/user/profileimg/50/' . $data->sender;
            }

        } else if($method == '1'){

            if ($data->profileimg && !$data->partner_last_name) {
                return '/public/uploads/locationImages/' . $data->chat_partner . '/' . $data->profileimg . '.jpg';
            } else if (!$data->profileimg && !$data->partner_last_name) {
                return '/public/images/gst_no_profile.jpg';
            } else {
                return '/user/profileimg/50/' . $data->chat_partner;
            }

        }

    }

    public function getMessagesFromThread() {

        //get needed variables
        $data['chat_id'] = $this->input->post('cid');
        $data['current_user'] = $this->input->post('id');
        $data['u_id'] = $this->input->post('id');

        //verify this user is a part of this message string. if not then dont display anything
        if($this->messages->verifyUser($data)) {

            //get the messages
            $messages = $this->messages->getChat($data['chat_id']);

            //formatting before sending to the front end
            foreach ($messages as $message) {

                //we need to decrypt the chat messages for display do it in a for each loop
                $message->message = htmlspecialchars(trim($this->decryptMessage($message->message)));

                //set the timestamp into a user readable format
                $message->date = date("n/j g:ia", strtotime($message->date));

                //in the case of the company. if the name is to long due shorten it
                $message->sender_first_name = $this->formatFirstName($message->sender_first_name);

                //format the image
                $message->profileImg = $this->processPictureUrl($message);

            }

            echo json_encode($messages);
        } else {
            echo 'error';
        }

    }

    //////////////Messaging End////////////////////////
}
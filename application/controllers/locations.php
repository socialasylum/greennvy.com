<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations extends CI_Controller {

    function Locations() {
        parent::__construct();
        $this->load->driver('cache');
        $this->load->model('dojos_model', 'dojos', true);
        $this->load->model('locations_model', 'locations', true);
        $this->load->model('location_hours_model', 'hours', true);
        $this->load->model('search_model', 'search', true);
        $this->load->model('profile_model', 'profile', true);
    }
    
    // $id = locations primary key
    public function index() {
    	
        try {
            $body['locations'] = $this->locations->getLocations(false);
            
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('locations/index', $body);
        $this->load->view('template/footer');
    }
    
    public function search($searchstr)
    {
        $body['doctorschecked'] = ($searchstr == 'doctors');
        $body['dispensarieschecked'] = ($searchstr == 'dispensaries');
        if (($searchstr == 'doctors') || ($searchstr == 'dispensaries'))
        {
            $body['searchterm'] = '';
        }
        $body['searchterm'] = $bodyListings['searchterm'] = (($searchstr == 'doctors') || ($searchstr == 'dispensaries')) ? '' : str_replace('-', ' ', $searchstr);
        $body['lat'] = $bodyListings['lat'] = $lat = $this->session->userdata('location')->lat;
        $body['lng'] = $bodyListings['lng'] = $lng = $this->session->userdata('location')->lng;
        $header['onload'] = "search.indexInit(" . urldecode($lat) . ", " . urldecode($lng) . ", true);";
        if ($searchstr == 'doctors') {
            $header['title'] = 'Search Marijuana Doctors';
        } else {
            $header['title'] = 'Search Marijuana Dispensaries';
        }
        $header['headscript'] = $this->functions->jsScript('search.js welcome.js');
        $header['googleMaps'] = true;
        try
        {
            $bodyListings['places'] = $this->search->searchLocations($body['searchterm'], $body['dispensarieschecked'], $body['doctorschecked']);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $body['initListings'] = $this->load->view('locations/listings', $bodyListings, true);
        $this->load->view('template/header', $header);
        $this->load->view('locations/index', $body);
        $this->load->view('template/footer');
    }
    
    public function profile($location_id) {
    	try {
            $body['location'] = $this->locations->getLocationById($location_id);
            $body['hours'] = $this->hours->getLocationHoursByLocationId($location_id);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('template/header', $header);
        $this->load->view('locations/profile', $body);
        $this->load->view('template/footer');
    }
    
    public function followers($lid){
    	$this->locations->getFollowers($lid);
    }
    
    public function addPhoto()
    {
        if ($_POST)
        {
            if ($_POST['isProfileImage'] > 0)
            {
                $this->locations->clearProfileImage($_POST['locationid']);
            }
            if ($_POST['isCover'] > 0)
            {
                $this->locations->clearCoverPhotos($_POST['locationid']);
            }
            if ($_POST['imageid'] > 0)
            {
                $this->locations->updateCaption($_POST['imageid'], $_POST['caption'], $_POST['isCover'], $_POST['isProfileImage']);
            }
            else
            {
                $this->locations->insertPhoto($_POST['locationid'], $_POST['photoFileName'], $_POST['isCover'], $_POST['caption'], $_POST['isProfileImage'], $_POST['isCover']);
            }
        }
    }
    
    public function addVideo()
    {
        if ($_POST)
        {
            if ($_POST['videoID'] > 0)
            {
                $this->locations->updateLocationVideo($_POST);
            }
            else
            {
                $this->locations->insertLocationVideo($_POST);
            }
        }
    }
    
    public function deleteVideo($videoid)
    {
        $this->locations->deleteVideo($videoid);
    }
    
    public function saveHours()
    {
        if ($_POST)
        {
            $this->hours->save($_POST);
        }
    }
    
    public function img($location, $size = 50, $id = 0)
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $location . DS;
        try {
            if (empty($location))
                throw new Exception("Location ID is empty");

            if (empty($id))
                $img = $this->locations->getLocationMainImage($location);
            else
                $img = $this->locations->getImageByID($location, $id);

            //$img = $this->users->getTableValue('profileimg', $userid);
            // no profile image is set - loads no profile img
            if (empty($img)) {
                $path = $_SERVER["DOCUMENT_ROOT"] . DS . 'public' . DS . 'images' . DS;
                $img = 'gst_no_profile.jpg';
            }

            $is = getimagesize($path . $img);

            if ($is === false)
                throw new exception("Unable to get image size for ({$path}{$img})!");

            $ext = PHPFunctions::getFileExt($img);
            list ($width, $height, $type, $attr) = $is;

            if ($width == $height) {
                $nw = $nh = $size;
            } elseif ($width > $height) {
                $scale = $size / $height;
                $nw = $width * $scale;
                $nh = $size;
                $leftBuffer = (($nw - $size) / 2);
            } else {
                $nw = $size;
                $scale = $size / $width;
                $nh = $height * $scale;
                $topBuffer = (($nh - $size) / 2);
            }

            $leftBuffer = $leftBuffer * -1;
            $topBuffer = $topBuffer * -1;

            if ($ext == "JPG")
                $srcImg = imagecreatefromjpeg($path . $img);
            if ($ext == "GIF")
                $srcImg = imagecreatefromgif($path . $img);
            if ($ext == "PNG")
                $srcImg = imagecreatefrompng($path . $img);

            $destImg = imagecreatetruecolor($size, $size); // new image
            imagecopyresized($destImg, $srcImg, $leftBuffer, $topBuffer, 0, 0, $nw, $nh, $width, $height);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
        header('Content-Type: image/jpg');
        imagejpeg($destImg);
        imagedestroy($destImg);
        imagedestroy($srcImg);
    }
    
    public function coverImg($locationid)
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $locationid . DS;
        $img = 'cover_' . $this->locations->getCoverImage($locationid);
        $path = (file_exists($path . $img)) ? $path . $img : $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'images' . DS . 'sub_bg.jpg';
        $ext = array_pop(explode('.', $path));
        $image = '';
        switch ($ext)
        {
            case 'jpg':
            case 'jpeg':
                $image = imagecreatefromjpeg($path);
                break;
            case 'gif':
                $image = imagecreatefromgif($path);
                break;
            case 'png':
                $image = imagecreatefrompng($path);
                break;
        }

        header("Content-Type: image/$ext");
        switch ($ext)
        {
            case 'jpg':
            case 'jpeg':
                $image = imagejpeg($image);
                break;
            case 'gif':
                $image = imagegif($image);
                break;
            case 'png':
                $image = imagepng($image);
                break;
        }
        imagedestroy($img);
        imagedestroy($image);
    }
    
    public function deleteImg($id)
    {
        $this->locations->deleteImg($id);
    }
    
    public function followLocation($locationid)
    {
        $this->locations->follow($locationid);
    }
    
    public function unfollowLocation($locationid)
    {
        $this->locations->unfollow($locationid);
    }
    
    public function testCover($imageid)
    {
        $this->db->where('id', $imageid);
        $this->db->select('fileName, locationid');
        $row = $this->db->get('locationImages')->first_row();
        $fileName = $row->fileName;
        $locationid = $row->locationid;
        //echo $locationid . '-' . $fileName;
        $this->locations->writeCoverImage($locationid, $fileName);
    }
}

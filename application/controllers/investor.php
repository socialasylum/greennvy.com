<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Investor extends CI_Controller {

    function Investor()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('legal/investor');
    }
}
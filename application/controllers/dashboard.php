<?php
error_reporting(E_ALL & ~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function Dashboard() {
        parent::__construct();
        $this->load->driver('cache');
        $this->load->model('deals_model', 'deals', true);
        $this->load->model('profile_model', 'profile', true);
        $this->load->model('dashboard_model', 'dashboard', true);
        $this->load->model('search_model', 'search', true);
        $this->load->model('reviews_model', 'reviews', true);
        $this->load->model('wall_model', 'wall', true);
        $this->load->helper('form');
        $this->load->helper('url');
    }
    
    public function index() {
        $this->functions->checkLoggedIn();
        $header['headscript'] = $this->functions->jsScript('search.js welcome.js dashboard.js');
        $header['googleMaps'] = true;
        $header['resizeimage'] = true;
        $header['dashboardStyle'] = true;
        $body['deals'] = $this->deals->getDealsByIp(10);
        try {
            $geoip = $this->search->grabGeoIp();
            $body['lat'] = $bodyListings['lat'] = $lat = $geoip->lat;
            $body['lng'] = $bodyListings['lng'] = $lng = $geoip->lng;
            $header['onload'] = "search.indexInit(" . urldecode($lat) . ", " . urldecode($lng) . ");";
            $bodyListings['places'] = $this->search->searchLocations();
            $body['initListings'] = $this->load->view('locations/listings', $bodyListings, true);
            $body['activity']['activity'] = $this->dashboard->recentActivity($this->session->userdata('userid'), 20);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
        $this->load->view('template/header', $header);
        $this->load->view('dashboard/index', $body);
        $this->load->view('template/footer');
    }
    
    public function uploadPostImg() {
        $logged_in = $this->functions->checkLoggedIn(false);

        if (!$logged_in)
            $this->functions->jsonReturn('ERROR', "You are not logged in!");
        
        if ($_FILES) {
            try {
                
                $path = 'public' . DS . 'uploads' . DS . 'userimgs' . DS . $this->session->userdata('userid') . DS;

                // ensures company uploads directory has been created
                $this->functions->createDir($path);
                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "gif|jpg|png";
                $config['max_size'] = "5120";
                $config['encrypt_name'] = true;

                // loads upload library
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file')) {
                    throw new Exception("Unable to upload post image!" . $this->upload->display_errors());
                }
                $uploadData = $this->upload->data();
                $this->functions->jsonReturn('SUCCESS', $uploadData['file_name']);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }
    
    public function test()
    {
        var_dump($this->dashboard->recentActivity($this->session->userdata('userid'), 20));
    }
}

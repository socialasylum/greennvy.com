<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Social extends CI_Controller {

    function Social()
    {
        parent::__construct();
    }

    public function index()
    {
	    $header['title'] = "Social Media";
	    $this->load->view('template/header', $header);
        $this->load->view('social/social');
        $this->load->view('template/footer');
    }
}
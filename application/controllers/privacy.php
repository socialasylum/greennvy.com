<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Privacy extends CI_Controller {

    function Privacy()
    {
        parent::__construct();
    }

    public function index()
    {
	    $header['title'] = "Privacy Policy";
	    $this->load->view('template/header', $header);
        $this->load->view('legal/privacy');
        $this->load->view('template/footer');
    }
}
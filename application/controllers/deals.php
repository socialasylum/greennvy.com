<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deals extends CI_Controller {

    function Deals() {
        parent::__construct();
        $this->load->driver('cache');
        $this->load->model('deals_model', 'deals', true);
        $this->load->model('menu_model', 'menu', true);
        $this->load->model('profile_model', 'profile', true);
        $this->load->model('dojos_model', 'dojos', true);
        $this->load->model('locations_model', 'locations', true);
        $this->load->model('reviews_model', 'reviews', true);
        $this->load->model('search_model', 'search', true);
        $this->load->helper('form');
        $this->load->helper('url');
    }

    // user that is not logged in will be redirected to this function
    public function view($location_id = null)
    {
        $body['info'] = $info = $this->locations->getLocationById($location_id);
        $body['menu'] = $this->menu->getMenu($location_id);
        $body['menuOptions'] = $this->menu->getMenuOptions();
        $body['deals'] = $this->deals->getDealsByLocation($location_id);

        $body['reviews'] = $this->search->getReviews($location_id);

        $body['assigned'] = $this->search->checkLocationAssigned($location_id);
        $body['images'] = $this->search->getLocationImages($location_id);
        $header['title'] = 'Get the Best Medical Marijuana Deals';
        //$body['menu_userlist'] = $this->load->view('partials/menu_userlist', $body, true);
        //$this->load->view('/template/header', $header);
        $this->load->view('/deals/front/index', $body);
        //$this->load->view('/template/footer');
    }

    // $id = locations primary key
    public function index() {
        $header['headscript'] = $this->functions->jsScript('deals.js');
        $header['resizeimage'] = true;
        $body['userid'] = $userid;
        $header['title'] = 'Get the Best Medical Marijuana Deals';
        try {
            $body['deals'] = $this->deals->getDealsByIp(3, true);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }


        $body['newest_deals'] = $this->deals->getNewDealsByIp();
        $body['expiring_deals'] = $this->deals->getExpiringDealsByIp();

        $this->load->view('template/header', $header);
        $this->load->view('deals/index', $body);
        $this->load->view('template/footer');
    }

    //params: deals.dealid, locations.id
    public function edit($dealid, $id) {
        if (!$dealid || !$id)
            header('Location: /deals/index/' . $id);

        $userid = $this->session->userdata('userid');
        $body['userid'] = $userid;
        $body['deals'] = $this->deals->getDealById($dealid);
        $body['location_id'] = $id;

        if (!empty($_POST)) {
            $params = $_POST;

            if (!empty($_FILES['userfile']['name'])) {
                $params['deal_image'] = $this->doUpload()['file_name'];
            }
            try {
                $this->deals->save($params);
                header('Location: /profile/vendoredit/' . $id . '?site-success=' . urlencode('Deal has been saved!'));
                exit;
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->session->set_flashdata('FAILURE', $e->getMessage());
            }
        }

        //$this->load->view('template/header', $header);
        $this->load->view('deals/edit', $body);
        //$this->load->view('template/footer');
    }

    public function add($id) {
        if (!$id)
            header('Location: /deals/index/' . $id);

        $body['userid'] = $this->session->userdata['userid'];
        $body['location_id'] = $id;
        if (!empty($_POST)) {
            $params = $_POST;

            if (!empty($_FILES['userfile']['name'])) {
                $params['deal_image'] = $this->doUpload()['file_name'];
            }
            try {
                $this->deals->save($params);
                header('Location: /profile/vendoredit/' . $id . '?site-success=' . urlencode('Deal has been saved!'));
                exit;
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);

                $this->session->set_flashdata('FAILURE', $e->getMessage());
            }
        }
        //$this->load->view('template/header', $header);
        $this->load->view('deals/add', $body);
        //$this->load->view('template/footer');
    }

    public function delete($dealid, $id) {
        if (!$dealid || !$id)
            header('Location: /deals/index/' . $id);

        $dealid = $this->deals->delete($dealid);

        $this->session->set_flashdata('SUCCESS', 'Your deal has been deleted!');
        header("Location: /deals/index/$id");
        exit;
    }

    private function doUpload() {
        $path = 'public' . DS . 'uploads' . DS . 'deals' . DS . $this->session->userdata('userid') . DS;

        // ensures company uploads directory has been created
        $this->functions->createDir($path);

        $config['upload_path'] = './' . $path;
        $config['allowed_types'] = "gif|jpg|png";
        $config['max_size'] = "5120";
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            return $this->upload->display_errors();
        } else {
            $data = array('upload_data' => $this->upload->data());

            return($data['upload_data']);
        }
    }

    public function setfeatured($dealid) {
        $num_rows_affected = $this->deals->setFeatured($dealid);
        if ($num_rows_affected > 0) {
            echo $dealid;
        } else {
            echo 0;
        }
        exit;
    }

    public function getfeatured() {
        $num_rows_affected = $this->deals->getFeatured($dealid);
        if ($num_rows_affected > 0) {
            echo $dealid;
        } else {
            echo 0;
        }
        exit;
    }

    public function dealimg($size = 50, $userid = 0, $file = null) {
        if (empty($userid))
            $userid = $this->session->userdata('userid');

        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'deals' . DS . $userid . DS;

        try {

            if (!empty($file))
                $img = $file;

            // checks if file exists
            if (!file_exists($path . $img))
                $img = null;

            // no profile image is set - loads no profile img
            if (empty($img)) {
                $path = $_SERVER["DOCUMENT_ROOT"] . DS . 'public' . DS . 'images' . DS;
                $img = 'gst_no_profile.jpg';
            }

            $is = getimagesize($path . $img);

            if ($is === false)
                throw new exception("Unable to get image size for ({$path}{$img})!");

            $ext = PHPFunctions::getFileExt($img);

            list ($width, $height, $type, $attr) = $is;

            if ($width == $height) {
                $nw = $nh = $size;
            } elseif ($width > $height) {
                $scale = $size / $height;
                $nw = $width * $scale;
                $nh = $size;
                $leftBuffer = (($nw - $size) / 2);
            } else {
                $nw = $size;
                $scale = $size / $width;
                $nh = $height * $scale;
                $topBuffer = (($nh - $size) / 2);
            }

            $leftBuffer = $leftBuffer * -1;
            $topBuffer = $topBuffer * -1;

            if ($ext == "JPG")
                $srcImg = imagecreatefromjpeg($path . $img);
            if ($ext == "GIF")
                $srcImg = imagecreatefromgif($path . $img);
            if ($ext == "PNG")
                $srcImg = imagecreatefrompng($path . $img);

            $destImg = imagecreatetruecolor($size, $size); // new image
            #imagecopyresized($destImg, $srcImg, 0, 0, $leftBuffer, $topBuffer, $nw, $nh, $width, $height);
            imagecopyresized($destImg, $srcImg, $leftBuffer, $topBuffer, 0, 0, $nw, $nh, $width, $height);
        } catch (Exception $e) {
            PHPFunctions::sendStackTrace($e);
        }
        header('Content-Type: image/jpg');
        imagejpeg($destImg);
        imagedestroy($destImg);
        imagedestroy($srcImg);
    }
}

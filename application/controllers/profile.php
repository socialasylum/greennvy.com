<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    function Profile() {
        parent::__construct();

        //$this->load->library('session');
        $this->load->driver('cache');
        $this->load->model('profile_model', 'profile', true);
        $this->load->model('dojos_model', 'dojos', true);
        $this->load->model('menu_model', 'menu', true);
        $this->load->model('user_model', 'user', true);
        $this->load->model('locations_model', 'locations', true);
        $this->load->model('location_hours_model', 'location_hours', true);
        $this->load->model('deals_model', 'deals', true);
        $this->functions->checkLoggedIn();
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function index($tab = 0) {
        $header['headscript'] = $this->functions->jsScript('profile.js');
        $header['googleMaps'] = true;
        $header['ckeditor'] = true;
        $header['autocomplete'] = true;
        $body['tab'] = $tab;
        try {
            $body['facebookID'] = $this->functions->getFacebookID($this->session->userdata('userid'));
            $body['listings'] = $this->profile->getUserAssignedLocations($this->session->userdata('userid'));
            $body['styles'] = $this->functions->getCodes(26, $this->config->item('bmsCompanyID'));
            $body['userinfo'] = $this->functions->getUserInfo($this->session->userdata('userid'));
            $body['userStyles'] = $this->dojos->getUserCodes($this->session->userdata('userid'), 26);
            $body['timezones'] = $this->functions->getTimezonesFull();
            $body['weights'] = $this->functions->getCodes(33);
            $body['genders'] = $this->functions->getCodes(34);
            $body['requests'] = $this->profile->getClaimRequests();
            $body['locRank'] = $this->locations->getLocationsRank();
            $body['companyLists'] = $this->locations->getFullList();
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
        $header['onload'] = "profile.indexInit(0,0);";
        $this->load->view('template/header', $header);
        $this->load->view('profile/index', $body);
        $this->load->view('template/footer');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function vendoredit($id = 0) {
        $header['headscript'] = $this->functions->jsScript('profile.js');
        $header['googleMaps'] = true;
        //$header['ckeditor'] = true;
        $header['onload'] = "profile.locationedit(0, 0);";
        $header['timepicker'] = true;
        $body['id'] = $id;
        $body['menu_options'] = $this->menu->getMenuOptions();
        $body['userid'] = $this->session->userdata['userid'];

        try {

            $body['menu_items'] = $this->menu->getMenu($id);
            $body['styles'] = $this->functions->getCodes(26, $this->config->item('bmsCompanyID'));
            $body['states'] = $this->functions->getStates();

            if (!empty($id)) {
                $body['info'] = $info = $this->dojos->getLocationInfo($id);
                $body['location_hours'] = $location_hours = $this->location_hours->getLocationHoursByLocationId($id);
                $body['assignedStyles'] = $this->dojos->getLocationCodes($id, 26);
                $body['defaultImg'] = $this->dojos->getLocationMainImage($id);
                $body['images'] = $this->dojos->getLocationImages($id, true);
                //get videos
                $body['videos'] = $this->locations->getLocationVideos($id);
                $header['onload'] = "profile.locationedit({$info->lat}, {$info->lng});";
            }
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $videoBody['showDelete'] = true;
        $body['videoModal'] = $this->load->view('profile/videomodal', $videoBody, true);
        $this->load->view('template/header', $header);
        $this->load->view('profile/locationedit_v2', $body);
        $this->load->view('template/footer');
    }

    public function deleteItem($menuid) {
        echo ($this->menu->deleteItem($menuid)) ? 'Success' : 'Error';
    }

    public function deleteDeal($menuid) {
        echo ($this->deals->delete($menuid)) ? 'Success' : 'Error';
        exit;
    }

    public function addMenuItem() {
        if ($_POST) {
            $item_array = array();
            foreach ($_POST as $k => $v) {
                if ($k != 'karateToken') {
                    $item_array[0][$k] = $v;
                }
            }
            $this->menu->saveMenu($item_array);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function savelisting() {
        if (!empty($_POST)) {
            set_time_limit(3600);
            try {
                $address = "{$_POST['address']} {$_POST['city']}, {$_POST['state']} {$_POST['postalCode']}";

                // geolocate address
                $ll = $this->functions->getAddressLatLng($address);

                if (empty($_POST['lat']))
                    $_POST['lat'] = $ll->lat;
                if (empty($_POST['lng']))
                    $_POST['lng'] = $ll->lng;

                //var_dump($_POST); exit;

                if (empty($_POST['id'])) {
                    // create listing
                    //$post_id = $this->profile->insertPost($_POST);
                    $location = $this->profile->insertLocation($_POST);

                    // assigns location to user
                    $this->profile->insertUserLocation($this->session->userdata('userid'), $location);


                    // add google Places location
                    $this->profile->setGoogleReference($location, $_POST);

                    // save thumbnail ID
                } else {
                    $location = $_POST['id'];
                    $this->profile->updateLocation($_POST);

                    // clears saved styles
                    $this->profile->clearLocationCodes($_POST['id'], 26);
                }

                $params = $_POST;
                $params['location_id'] = isset($_POST['id']) ? $_POST['id'] : null;
                //$this->location_hours->save($params); 
                // saves styles
                if (!empty($_POST['styles'])) {
                    foreach ($_POST['styles'] as $code) {
                        $this->profile->insertLocationCode($location, 26, $code);
                    }
                }

                // upload videos
                if (!empty($_POST['videoUrl'])) {
                    $cnt = 0;
                    foreach ($_POST['videoUrl'] as $videoUrl) {
                        $videoData = array();

                        // skips video if empty
                        if (empty($videoUrl))
                            continue;

                        $videoData['url'] = $videoUrl;

                        $videoData['id'] = $this->functions->getYoutubeVideoID($videoUrl);


                        // gets youtube data about video
                        $youtubeData = $this->functions->getYoutudeVideoData($videoData['id'], false);
                        $data = json_decode($youtubeData);

                        $videoData['title'] = $data->entry->title->{'$t'};
                        $videoData['thumbnail'] = $data->entry->{'media$group'}->{'media$thumbnail'}[0]->url;
                        $videoData['description'] = $data->entry->{'media$group'}->{'media$description'}->{'$t'};

                        // insert video post
                        // must now save post attachment
                        $videoPostData = array(
                            'location' => $location,
                            'url' => $videoData['url'],
                            'order' => $cnt,
                            'title' => $videoData['title'],
                            'thumbnail' => $videoData['thumbnail'],
                            'description' => $videoData['description'],
                            'videoID' => $videoData['id']
                        );

                        $videoID = $this->profile->insertLocationVideo($videoPostData);

                        $cnt++;
                    }
                }
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
            }
            PHPFunctions::jsonReturn('SUCCESS', 'Location updated successfully.', true, 0, array('locationid' => $location));
        }
    }

    public function dealSettings($id)
    {
        $body['deals'] = $this->deals->getDealsByLocation($id);
        $this->load->view('partials/profile_deals', $body);
    }
    
    public function profileSettings($id) {
        if ($id > 0)
        {
            $body['info'] = $this->locations->getLocationInfo($id);
        }
        $body['id'] = $id;
        $body['states'] = $this->functions->getStates();
        $this->load->view('partials/profile_settings', $body);
    }

    public function menuSettings($id) {
        $body['menu_items'] = $this->menu->getMenu($id);
        $this->load->view('partials/profile_menu', $body);
    }
    
    public function hourSettings($id)
    {
        $body['hours'] = $this->location_hours->getLocationHoursByLocationId($id);
        $body['id'] = $id;
        $this->load->view('partials/profile_hours', $body);
    }

    public function profileImages($id) {
        $body['images'] = $this->locations->getImages($id);
        $body['id'] = $id;
        $this->load->view('partials/profile_images', $body);
    }

    public function profileVideos($id) {
        $body['videos'] = $this->locations->getLocationVideos($id);
        $this->load->view('partials/profile_videos', $body);
    }

    public function deletelocation() {
        if ($_POST) {

            try {
                // get location info
                $info = $this->dojos->getLocationInfo($_POST['location']);

                if (!empty($info->googleReference)) {
                    // remove location from Google Places
                    $this->places->deleteLocation($info->googleReference);
                }

                $this->profile->setLocationDeleted($_POST['location']);

                $this->functions->jsonReturn('SUCCESS', 'Location has been deleted!');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function uploadLocationImgs() {
        $logged_in = $this->functions->checkLoggedIn(false);

        if (!$logged_in)
            $this->functions->jsonReturn('ERROR', "You are not logged in!");

        if ($_FILES) {
            try {

                $locationid = intval($_POST['locationid']);
                $path = 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $locationid . DS;

                // ensures company uploads directory has been created
                $this->functions->createDir($path);
                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "gif|jpg|png";
                $config['max_size'] = "5120";
                $config['encrypt_name'] = true;

                // loads upload library
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file')) {
                    throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                }
                $uploadData = $this->upload->data();
                $imageid = $this->locations->insertPhoto($locationid, $uploadData['file_name'], 0);
                $this->functions->jsonReturn('SUCCESS', $uploadData['file_name'], true, 0, array('imageid' => $imageid));
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function saveimgorder() {
        if ($_POST) {
            try {

                $cnt = 0;
                if (!empty($_POST['img'])) {
                    foreach ($_POST['img'] as $imgID) {
                        $this->profile->updateImgOrder($_POST['location'], $imgID, $cnt);
                        $cnt++;
                    }
                }
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function connectfb() {
        try {
            $this->profile->updateFacebookID($this->session->userdata('userid'), $_POST['facebookID']);
            $this->functions->jsonReturn('SUCCESS', 'Facebook account has been linked!');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function disconnectfb() {
        try {
            $this->profile->clearFacebookID($this->session->userdata('userid'));
            $this->functions->jsonReturn('SUCCESS', 'Facebook account has been unlinked!');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function deletevideo() {
        if ($_POST) {
            try {

                $this->profile->deleteVideo($_POST['location'], $_POST['id']);

                $this->functions->jsonReturn('SUCCESS', 'Video has been deleted!');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function deleteimg() {
        if ($_POST) {
            try {
                // gets fileName
                $fileName = $this->dojos->getImageByID($_POST['location'], $_POST['id']);

                $this->profile->deleteImage($_POST['location'], $_POST['id'], $fileName);

                $this->functions->jsonReturn('SUCCESS', 'Image has been deleted!');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function uploadavatar() {

        if ($_FILES) {

            try {
                // check andor create upload directory
                $path = 'public' . DS . 'uploads' . DS . 'avatars' . DS . $this->session->userdata('userid') . DS;

                // ensures company uploads directory has been created
                $this->functions->createDir($path);

                $config['upload_path'] = './' . $path;
                $config['allowed_types'] = "gif|jpg|png";
                $config['max_size'] = "5120";
                $config['encrypt_name'] = true;

                // loads upload library
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file')) {
                    throw new Exception("Unable to upload profile image!" . $this->upload->display_errors());
                }

                $uploadData = $this->upload->data();

                //$this->profile->sendAvatarToBMS($path, $uploadData['file_name']);

                $this->user->updateProfileImg($this->session->userdata('userid'), $uploadData['file_name']);
            } catch (Exception $e) {
                $data['status'] = 'FAIL';
                echo json_encode($data);
            }

            $data['status'] = 'SUCCESS';
            $data['user'] = $this->session->userdata('userid');
            echo json_encode($data);
        }
    }

    public function saveusersettings() {
        if ($_POST) {
            try {

                $emailChange = false;

                // checks if e-mail has changed 
                if (strtoupper($this->session->userdata('email')) !== strtoupper($_POST['email'])) {
                    // if changed, checks if email is available
                    $emailAvailable = $this->profile->checkEmailAvailable($_POST['email']);

                    if ($emailAvailable == false) {
                        $this->functions->jsonReturn('ALERT', 'E-mail Address is already in use!');
                    }

                    $emailChange == true;
                }

                $_POST['userid'] = $this->session->userdata('userid');

                // update user info
                $this->profile->updateUserInfo($_POST);

                // clear previously saved user styles
                $this->profile->clearUserCodes($this->session->userdata('userid'), 26);


                if (!empty($_POST['styles'])) {
                    foreach ($_POST['styles'] as $code) {
                        $this->profile->insertUserCode($this->session->userdata('userid'), 26, $code);
                    }
                }

                // updates email session data
                if ($emailChange == true) {
                    $this->session->set_userdata('email', $_POST['email']);
                }

                $this->session->set_userdata('firstName', $_POST['firstName']);
                $this->session->set_userdata('lastName', $_POST['lastName']);

                $this->functions->jsonReturn('SUCCESS', 'User settings have been saved!');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function savefbphoto() {
        try {
            $filename = $this->profile->downloadfbphoto($this->session->userdata('userid'), $_POST['url']);

            // check andor create upload directory
            $path = 'public' . DS . 'uploads' . DS . 'avatars' . DS . $this->session->userdata('userid') . DS;

            $this->profile->sendAvatarToBMS($path, $filename);

            $this->functions->jsonReturn('SUCCESS', 'Facebook photo has been saved!');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function claimAccept() {
        if($_POST){
            try {
                $this->profile->claimAccept($_POST);
                $this->functions->jsonReturn('SUCCESS');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function claimRemove() {
        if($_POST){
            try {
                $this->profile->claimRemove($_POST);
                $this->functions->jsonReturn('SUCCESS');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function claimDelete() {
        if($_POST){
            try {
                $this->profile->claimDelete($_POST);
                $this->functions->jsonReturn('SUCCESS');
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function getCompanyInfo() {
        if($_POST){
            try {
                $data = $this->locations->getRankInfo($_POST['id']);
                $this->functions->jsonReturn('SUCCESS', $data);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function updateCompanyRank() {
        if($_POST){
            try {
                $data = $this->locations->updateRank($_POST);
                $this->functions->jsonReturn('SUCCESS', $data);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    function Welcome() {
        parent::__construct();
        $this->load->driver('cache');
        $this->load->model('welcome_model', 'welcome', true);
    }

    public function index() {
        $header['headscript'] = $this->functions->jsScript('welcome.js');
        $header['onload'] = "welcome.indexInit();";
        $img = '/public/images/cannabinoid/cannabglanding.jpg';
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $location_info = json_decode(file_get_contents('http://freegeoip.net/json/' . $user_ip));
        header("Location: search?q=Dispensaries&location=" . urlencode($location_info->city . ', ' . $location_info->region_code));
    }

    public function landing() {
        $header['headscript'] = $this->functions->jsScript('welcome.js');
        $header['onload'] = "welcome.landingInit();";
        $header['showLocator'] = false;


        $body['login'] = (bool) $login;


        $this->load->view('template/header', $header);
        $this->load->view('welcome/landing');
        $this->load->view('template/footer');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function login() {


        try {
            $check = $this->welcome->checkLogin($_POST['user_email'], $_POST['user_pass']);

            if (empty($check))
                $this->functions->jsonReturn('ERROR', 'Invalid Username and/or Password');

            /*$companyAssigned = $this->functions->checkUserAssignedToCompany($check->id);

            if ($companyAssigned == false)
                $this->functions->jsonReturn('ERROR', 'Invalid Username and/or Password');*/

            // check if user is assigned to company

            $this->functions->setLoginSession($check->id);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }

        /*
          $auth = wp_authenticate($_POST['user_login'], $_POST['user_pass']);

          if (is_wp_error($auth))
          {
          $this->functions->jsonReturn('ERROR', 'Invalid Username and/or Password');
          }

          wp_set_auth_cookie($auth->ID, $_POST['rememberme']);
         */
        /*
          $data = array
          (
          'ID' => $auth->ID,
          'name' => $auth->first_name
          );

          $this->session->set_userdata($data);
         */


        /*
          $this->session->set_userdata('id', $auth->ID);
          $this->session->set_userdata('displayName', $current_user->display_name);
         */
        $this->functions->jsonReturn('SUCCESS');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function logout() {
        // logs user out of wordpress
        // wp_logout();

        $this->session->sess_destroy();

        header("Location: /?site-success=" . urlencode("You have logged out"));
        exit;
        // $this->functions->jsonReturn('SUCCESS', 'User has been logged out');
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function signup() {
        try {
            //$body['countries'] = $this->functions->getCountryList();
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }

        $this->load->view('welcome/signup', $body);
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function register() {
        if ($_POST) {
            try {
                // check if email is available
                $emailAvail = $this->functions->checkEmailAvailable($_POST['email']);

                if ($emailAvail === true) {

                    // create user
                    $userid = $this->welcome->createUser($_POST);
                    
                    //logs user in
                    $this->functions->setLoginSession($userid);
                    /*
                     * verification stuff
                     */
                    
                    $this->load->model('mandrill_model', 'mandrill', true);
                    $hash = $this->welcome->setVerifyHash($userid);
                    $this->mandrill->sendVerification($hash);
                    
                    $this->functions->jsonReturn('SUCCESS', 'Account has been created!');
                } else {
                    $this->functions->jsonReturn('ALERT', 'E-mail address is already in use!');
                }
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function fbreg() {
        try {
            // first check if an account is already associated with this facebook account
            $userid = $this->welcome->getFacebookID($_POST['data']['id']);

            if (!empty($userid)) {
                $companyAssigned = $this->functions->checkUserAssignedToCompany($userid);

                if ($companyAssigned == false) {
                    // assigns user to company
                    $this->welcome->assignUserToCompany($userid);
                }

                //if ($companyAssigned == false) this->functions->jsonReturn('ERROR', 'Invalid Username and/or Password');
                //wp_set_auth_cookie($user_id);

                $this->functions->setLoginSession($userid);

                $this->functions->jsonReturn('SUCCESS', 'You are logged in');
            }

            $passwd = uniqid($_POST['data']['email'], true);

            // check if email is available
            $emailAvail = $this->functions->checkEmailAvailable($_POST['data']['email']);

            if ($emailAvail === true) {
                $data = array
                    (
                    'email' => $_POST['data']['email'],
                    'firstName' => $_POST['data']['first_name'],
                    'lastName' => $_POST['data']['last_name'],
                    'user_pass' => $passwd,
                    'facebookID' => $_POST['data']['id']
                );

                // create user
                $userid = $this->welcome->createUser($data);

                //logs user in
                $this->functions->setLoginSession($userid);
                $this->functions->jsonReturn('SUCCESS', 'Account has been created!');
            } else {
                $this->functions->jsonReturn('ALERT', 'Username or email is already in use!');
            }
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }

    public function forgotpassword()
    {
        if ($_POST) {
            $this->load->model('mandrill_model', 'mandrill', true);
            try {
                // get user ID from email address
                $user = $this->welcome->getIDFromEmail($_POST['email']);

                if ($user !== false) {
                    $requestID = $this->welcome->insertPasswordResetRequest($user);
                    $this->mandrill->sendForgotPassword($_POST['email'], $user, $requestID);
                }

                $this->functions->jsonReturn('SUCCESS', "An e-mail will be sent to <strong>{$_POST['email']}</strong> with instructions on how to reset the password if there is an account associated with that e-mail address.", $requestID);
            } catch (Exception $e) {
                $this->functions->sendStackTrace($e);
                $this->functions->jsonReturn('ERROR', $e->getMessage());
            }
        }
    }

    public function resetpassword($userid, $requestID)
    {
        $this->load->model('user_model', 'user', true);
        $header['resetpassword'] = true;
        $this->load->view('template/header', $header);
        if (is_numeric($userid) && isset($requestID))
        {
            $body['userid'] = $userid;
            $body['requestID'] = $requestID;
            $body['requestCheck'] = $this->welcome->checkPasswordRequestID($userid, $requestID);
            $body['email'] = $this->user->getEmailFromUserID($userid);
        }
        $this->load->view('welcome/resetpassword', $body);
        $this->load->view('template/footer');
    }
    
    public function changepassword()
    {
        if ($_POST)
        {
            $this->welcome->changepw($_POST['userid'], $_POST['password']);
            $this->welcome->processPasswordReset($_POST['userid'], $_POST['requestID']);
            $this->functions->setLoginSession($_POST['userid']);
        }
    }

    /**
     * TODO: short description.
     *
     * @return TODO
     */
    public function phpinfo() {
        phpinfo();
    }

    public function geotargetlocation() {
        try {
            echo $_GET['lng'] . ' x ' . $_GET['lat'];
            $lng = floatval($_GET['lng']);
            $lat = floatval($_GET['lat']);
            $this->session->set_userdata('lng', $lng);
            $this->session->set_userdata('lat', $lat);
            print_r($_SESSION);
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
            $this->functions->jsonReturn('ERROR', $e->getMessage());
        }
    }
    
    public function test()
    {
        $this->load->model('user_model', 'user', true);
        echo $this->user->getEmailFromUserID(155);
    }
        
    public function sendVerificationEmail()
    {
        $this->load->model('mandrill_model', 'mandrill', true);
        $hash = $this->welcome->setVerifyHash($this->session->userdata('userid'));
        echo $hash;
        //$this->mandrill->sendVerification($hash);
    }
    
    public function verify($userid, $hash)
    {
        $body['result'] = $this->welcome->verifyAccount($userid, $hash);
        $this->load->view('template/header');
        $this->load->view('welcome/verify', $body);
        $this->load->view('template/footer');
    }
}

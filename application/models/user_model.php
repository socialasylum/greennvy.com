<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user_model extends CI_Model {

    /**
     * TODO: short description.
     *
     */
    function __construct() {
        parent::__construct();
    }

    public function getBlogs($userid, $page = 1, $category = 0) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $company = $this->config->item('bmsCompanyID');

        $mtag = "blogs-{$userid}-{$page}-{$category}-{$company}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $perPage = $this->config->item('blogsPerPage');

            $offset = ($page * $perPage) - $perPage;

            $this->db->select('id, publishDate, title, active');
            $this->db->from('blogs');
            $this->db->where('userid', $userid);
            $this->db->where('company', $company);
            $this->db->where('deleted', 0);
            $this->db->where('active', 1);

            if (!empty($category))
                $this->db->where('category', $category);

            $this->db->order_by('publishDate', 'DESC');
            $this->db->limit($perPage, $offset);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    public function followUser($userid, $followingUser) {
        $userid = intval($userid);
        $followingUser = intval($followingUser);

        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($followingUser))
            throw new Exception('Following user ID is empty!');

        $data = array(
            'datestamp'     => DATESTAMP,
            'userid'        => $userid,
            'locationid'    => 0,
            'followingUser' => $followingUser,
            'active'        => 1,
            'dateApproved'  => DATESTAMP
        );
        
        $this->db->insert('userFollow', $data);

        return $this->db->insert_id();
    }

    public function unfollowUser($userid, $followingUser) {
        $userid = intval($userid);
        $followingUser = intval($followingUser);

        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($followingUser))
            throw new Exception('Following user ID is empty!');

        $this->db->where('userid', $userid);
        $this->db->where('followingUser', $followingUser);
        $this->db->delete('userFollow');

        return true;
    }

    public function checkFollowingUser($followingUser, $userid = 0) {
        // defaults to current logged in user
        if (empty($userid))
            $userid = $this->session->userdata('userid');


        $userid = intval($userid);
        $followingUser = intval($followingUser);

        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($followingUser))
            throw new Exception('Following user ID is empty!');

        $mtag = "checkFollowingUser-{$followingUser}-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {

            $this->db->from('userFollow');
            $this->db->where('userid', $userid);
            $this->db->where('followingUser', $followingUser);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ((int) $data > 0)
            return true;

        return false;
    }

    public function getEmailFromUserID($userid)
    {
        $userid = intval($userid);
        $mtag = "getEmailFromUserID-{$userid}";
        $data = $this->cache->memcached->get($mtag);
        if (!$data)
        {
            $this->db->select('email');
            $this->db->where('id', $userid);
            $data = $this->db->get('users')->first_row()->email;
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }
    
    public function getFollowersCnt($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getFollowersCnt-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {

            $this->db->from('userFollow');
            $this->db->where('followingUser', $userid);
            $this->db->where('active', 1);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    public function getFollowers($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getFollowers-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $query_str = "
                SELECT
                        uf.userid
                    ,   uf.followingUser
                    ,   u.firstName
                    ,   u.lastName
                FROM userFollow uf
                INNER JOIN users AS u ON (u.id = uf.userid)
                WHERE active = 1
                    AND uf.followingUser = $userid
                    
            ";
            $query = $this->db->query($query_str);
            $data = $query->result();
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    public function getFollowing($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getFollowing-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $query_str = "
                SELECT
                        uf.userid
                    ,   uf.followingUser
                    ,   u.firstName
                    ,   u.lastName
                FROM userFollow uf
                INNER JOIN users AS u ON (u.id = uf.followingUser)
                WHERE active = 1
                    AND uf.userid = $userid
            ";
            $query = $this->db->query($query_str);
            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function getActivityCnt($userid){
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getActivityCnt-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if(!$data){

            $this->db->from('wallPosts');
            $this->db->where('userid', $userid);
            $this->db->where('parentPost', '0');

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function getFollowingCnt($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getFollowingCnt-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {

            $this->db->from('userFollow');
            $this->db->where('userid', $userid);
            $this->db->where('active', 1);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    public function getReviewCnt($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getReviewCnt-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {

            $this->db->from('locationReviews');
            $this->db->where('userid', $userid);
            //$this->db->where('active', 1);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
  
    public function getUploadCnt($userid)
    {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getUploadCnt-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->from('albumPhotos');
            $this->db->where('uploadedBy', $userid);
            $data = $this->db->count_all_results();
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    public function getUploads($userid)
    {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getUploads-{$userid}";
        
        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id, fileName, caption');
            $this->db->from('albumPhotos');
            $this->db->where('uploadedBy', $userid);
            $this->db->order_by('datestamp DESC');
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
    
    public function getReviews($userid) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $mtag = "getReviews-{$userid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $query_str = "
                SELECT
                        lr.id
                   ,    lr.location
                   ,	l.name as 'locationName'
                   ,    DATE_FORMAT(lr.datestamp, '%M/%d/%Y %H:%i:%s') as datestamp
                   ,    lr.name
                   ,    lr.email
                   ,    lr.comment
                   ,    lr.rating
                   ,    lr.userid
                FROM locationReviews lr
                INNER JOIN locations AS l ON (l.id = lr.location)
                WHERE lr.userid = $userid
            ";
            $query = $this->db->query($query_str);
            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }
      
    public function updateProfileImg($user, $filename) {
        $user = intval($user);

        if (empty($user))
            throw new Exception("User ID is empty!");

        $data = array('profileimg' => $filename);

        $this->db->where('id', $user);
        $this->db->update('users', $data);

        return true;
    }

    public function createPhotoAlbum($userid, $name, $stream = 0, $mobile = 0) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($name))
            throw new Exception('Album name is empty!');

        $data = array
            (
            'datestamp' => DATESTAMP,
            'company' => $this->config->item('bmsCompanyID'),
            'userid' => $userid,
            'name' => $name,
            'stream' => $stream,
            'mobile' => $mobile
        );

        $this->db->insert('userPhotoAlbums', $data);

        return $this->db->insert_id();
    }

    public function getAlbums($userid, $id = 0) {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');

        $company = $this->config->item('bmsCompanyID');


        $mtag = "userPhotoAlbums-{$userid}-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->select('id, datestamp, name');
            $this->db->from('userPhotoAlbums');
            $this->db->where('userid', $userid);
            $this->db->where('company', $company);

            if (!empty($id))
                $this->db->where('id', $id);

            $this->db->order_by('name');

            $query = $this->db->get();

            $results = $query->result();

            if (!empty($id))
                $data = $results[0];
            else
                $data = $results;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function getAlbumPhotos($album, $limit = false) {
        //$userid = intval($userid);
        $album = intval($album);

        //if (empty($userid)) throw new Exception('User ID is empty!');
        if (empty($album))
            throw new Exception('Album ID is empty!');

        $mtag = "getAlbumPhotos-{$album}-{$limit}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->select('id, datestamp, userid, fileName');
            $this->db->from('albumPhotos');
            $this->db->where('posted', 1);
            //$this->db->where('userid', $userid);
            $this->db->where('album', $album);

            if ($limit !== false)
                $this->db->limit($limit);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function insertAlbumPhoto($userid, $album, $fileName, $caption = null, $posted = 0, $postID = 0) {
        $data = array(
            'datestamp' => DATESTAMP,
            'userid' => $userid,
            'uploadedBy' => $this->session->userdata('userid'),
            'album' => $album,
            'fileName' => $fileName,
            'posted' => $posted,
            'postID' => $postID
        );

        if (!empty($caption))
            $data['caption'] = $caption;

        $this->db->insert('albumPhotos', $data);

        return $this->db->insert_id();
    }

    public function checkAlbumType($userid, $type = 'stream') {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($type))
            throw new Exception('Column Type ID is empty!');

        $mtag = "checkStreamPhotoAlbum-{$userid}-{$type}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {

            $this->db->from('userPhotoAlbums');
            $this->db->where('userid', $userid);
            $this->db->where($type, 1);

            $data = $this->db->count_all_results();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if ((int) $data > 0)
            return true;

        return false;
    }

    public function getAlbumTypeID($userid, $type = 'stream') {
        $userid = intval($userid);

        if (empty($userid))
            throw new Exception('User ID is empty!');
        if (empty($type))
            throw new Exception('Column Type ID is empty!');

        $mtag = "getAlbumTypeID-{$userid}-{$type}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {

            $this->db->select('id');
            $this->db->from('userPhotoAlbums');
            $this->db->where('userid', $userid);
            $this->db->where($type, 1);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function sendImgToBMS($path, $fileName, $userid) {
        $data = array
            (
            'userid' => $userid,
            'file' => "@./{$path}{$fileName}"
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->config->item('bmsUrl') . "user/albumphotoupload");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($ch);

        curl_close($ch);
    }

    public function savePhotoComment($p) {
        $data = array
            (
            'datestamp' => DATESTAMP,
            'userid' => $this->session->userdata('userid'),
            'photoID' => $p['photoID']
        );

        if (!empty($p['post']))
            $data['body'] = $p['post'];

        $this->db->insert('photoComments', $data);

        return $this->db->insert_id();
    }

    public function getPhotoComments($photoID) {
        $photoID = intval($photoID);

        if (empty($photoID))
            throw new Exception('Photo ID is empty!');

        $mtag = "photoComments-{$photoID}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->from('photoComments');
            $this->db->where('photoID', $photoID);
            $this->db->order_by('datestamp', 'desc');

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

}

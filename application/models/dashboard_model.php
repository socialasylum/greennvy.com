<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboard_model extends CI_model {
    
    function __construct() {
        parent::__construct();
        $this->load->model('deals_model', 'deals', true);
    }

    public function getDeals() {  	
    	$deals = new deals_model();
    	return $allMyDeals = $deals->getDeals($this->session->userdata('userid'));
    } 
    
    public function getDashboardSettings($dashboard_settings_id = null) {  	
    	
    	$query = $this->db->query('select * from dashboard_settings as d');   	
    	    
    	if($query->num_rows() > 0){  
    		return $query->result();
    	}

    	return null;
    } 
   
    public function save($params) {
    	$params = $this->cleanseParams($params);
    	
    	if(!empty($params['dealid'])) {
    		$deals = $this->getDealById($params['dashboard_id']);
    	
    		if(count($deals) > 0){
    	    	$this->db->where('dashboard', $params['dashboard_id']);
            	$this->db->update('dashboard', $params);
    		}
    	}
    	else {
    		$this->db->insert('dashboard', $params);
    	}
    	
    	return true;
    }
    
    public function recentActivity($userid, $limit = 20)
    {
        $query = "
            (
                SELECT
                        CONCAT(u.firstName, \" \", u.lastName) AS username
                    ,   '' AS location_name
                    ,   u.id AS userid
                    ,	wp.datestamp
                    ,   wp.body AS data_text
                    ,   wp.id AS postID
                    ,   NULL as filename
                    ,	1 AS datatype
                FROM userFollow uf
                INNER JOIN wallPosts AS wp ON (wp.userid = uf.followingUser)
                INNER JOIN users AS u ON (u.id = uf.followingUser)
                WHERE uf.userid = $userid
            )
            UNION
            (
                SELECT
                        CONCAT(u.firstName, \" \", u.lastName) AS username
                    ,   '' AS location_name
                    ,   u.id AS userid
                    ,	ap.datestamp
                    ,   ap.caption AS data_text
                    ,   0 AS postID
                    ,   ap.fileName as filename
                    ,	2 AS datatype
                FROM userFollow uf
                INNER JOIN albumPhotos AS ap ON (ap.userid = uf.followingUser)
                INNER JOIN users AS u ON (u.id = uf.followingUser)
                WHERE uf.userid = $userid
                    AND ap.postID = 0
            )
            UNION
            (
                SELECT
                        CONCAT(u.firstName, \" \", u.lastName) AS username
                    ,   '' AS location_name
                    ,   u.id AS userid
                    ,	wp.datestamp
                    ,   wp.body AS data_text
                    ,   wp.id as postID
                    ,   NULL as filename
                    ,	1 AS datatype
                FROM wallPosts wp
                INNER JOIN users AS u ON (u.id = wp.postingUser)
                WHERE wp.postingUser = $userid
            )
            UNION
            (
                SELECT
                        CONCAT(u.firstName, \" \", u.lastName) AS username
                    ,   '' AS location_name
                    ,   u.id AS userid
                    ,	ap.datestamp
                    ,   ap.caption AS data_text
                    ,   0 AS postID
                    ,   ap.fileName as filename
                    ,	2 AS datatype
                FROM albumPhotos ap
                INNER JOIN users AS u ON (u.id = ap.userid)
                WHERE ap.userid = $userid
                    AND ap.postID = 0
            )
            UNION
            (
                SELECT
                        '' AS username
                    ,   l.name AS location_name
                    ,   l.id AS userid
                    ,	d.created AS datestamp
                    ,   d.deal_name AS data_text
                    ,   0 AS postID
                    ,   NULL as filename
                    ,	3 AS datatype
                FROM userFollow uf
                INNER JOIN locations AS l ON (l.id = uf.locationid)
                INNER JOIN deals AS d ON (d.location_id = uf.locationid)
                WHERE uf.userid = $userid
            )
            UNION
            (
                SELECT
                        '' AS username
                    ,   l.name AS location_name
                    ,   l.id AS userid
                    ,	MAX(im.created) AS datestamp
                    ,   im.description AS data_text
                    ,   0 AS postID
                    ,   NULL as filename
                    ,	4 AS datatype
                FROM userFollow uf
                INNER JOIN locations AS l ON (l.id = uf.locationid)
                INNER JOIN item_menu AS im ON (im.locationid = uf.locationid)
                WHERE uf.followingUser = $userid
                GROUP BY l.id
                ORDER BY datestamp DESC
            )
            ORDER BY datestamp DESC
            LIMIT 20;
        ";
        $results = $this->db->query($query);
        return $results->result();
    }
}
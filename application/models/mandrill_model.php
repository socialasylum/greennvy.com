<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * TODO: short description.
 *
 * TODO: long description.
 *
 */
class mandrill_model extends CI_Model
{
    CONST API_KEY = '5pvZqjUx5I6aNRnbTjPXZA';
    CONST BASE_URL = 'https://mandrillapp.com/api/1.0/';
    private $mandrill;
    
    /**
     * TODO: short description.
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        require_once($_SERVER['DOCUMENT_ROOT'] . "public/Mandrill/Mandrill.php");
        $this->mandrill = new Mandrill(self::API_KEY);
    }
    
    public function sendForgotPassword($email, $userid, $requestID)
    {
        $msg = "<h1>Password Reset</h1><p><a href='http://" . $_SERVER['HTTP_HOST'] . "/welcome/resetpassword/$userid/" . urlencode($requestID) . "' target='_blank'>Click here to reset your password</a></p>";
        $message = array(
        'html' => $msg,
        'text' => 'Example text content',
        'subject' => 'Password Reset',
        'from_email' => 'no-reply@greennvy.com',
        'from_name' => 'Admin',
        'to' => array(
            array(
                'email' => $email,
                'name' => 'Matthew Downie',
                'type' => 'to'
            )
        )
        );
        return $this->mandrill->messages->send($message);
    }
    
    public function sendVerification($hash)
    {
        $message = array(
        'subject' => 'Activate Account',
        'from_email' => 'no-reply@greennvy.com',
        'from_name' => 'Admin',
        'to' => array(
            array(
                'email' => $this->session->userdata('email'),
                'name' => $this->session->userdata('firstName') . ' ' . $this->session->userdata('lastName'),
                'type' => 'to'
            )
        )
        );
        $template_content = array(
            array(
                'name' => 'username',
                'content' => $this->session->userdata('firstName') . ' ' . $this->session->userdata('lastName')
            ),
            array(
                'name' => 'activate-button',
                'content' => '<a class="mcnButton" title="Activate Account" href="http://greennvy.com/welcome/activate/' . $this->session->userdata('userid') . '/' . $hash . '" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Activate Account</a>'
            )
        );
        return $this->mandrill->messages->sendTemplate('activate-account', $template_content, $message);
    }
}
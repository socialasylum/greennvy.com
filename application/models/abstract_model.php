<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'search_model.php';

abstract class abstract_model extends search_model {

	protected function cleanseParams($params) {
    	foreach($params as $key=>$val) {  		
    		if(!property_exists($this, $key))
    		    unset($params[$key]);
    		elseif($params[$key] == '')
    		   $params[$key] = null;
    	}
    	
    	return $params;
    }
}
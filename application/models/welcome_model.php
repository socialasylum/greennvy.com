<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class welcome_model extends CI_Model
{
    /**
     * TODO: short description.
     *
     */
    function __construct ()
    {
        parent::__construct();
    }

    /**
     * TODO: short description.
     *
     * @param mixed $facebookID 
     *
     * @return TODO
     */
    public function getFacebookID ($facebookID)
    {
        if (empty($facebookID)) throw new Exception("Facebook ID is empty!");

        $mtag = "fbUserCheck-{$facebookID}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('users');
            $this->db->where('facebookID', $facebookID);

            $query = $this->db->get();

            $data = $query->result();

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
    
        if (empty($data)) return false;

        return $data[0]->id;
    }

    public function checkLogin ($email, $passwd, $passwordHashed = false, $facebookID = 0)
    {
        if ($passwordHashed == false) $passwd = sha1($passwd);

        $this->db->select('id, status, admin');
        $this->db->from('users');
        $this->db->where_in('status', array(1,3));

        $this->db->where('email', $email);
        $this->db->where('passwd', $passwd);

        $query = $this->db->get();

        $results = $query->result();

        // error_log($this->db->last_query());

        if (empty($results)) return false;

        return $results[0];
    }
    
    public function createUser ($p)
    {
    	if (empty($p['email'])) throw new Exception('E-mail Address is empty!');
    
	    $data = array
	    (
	    	'datestamp' => DATESTAMP,
	    	'email' => $p['email'],
	    	'passwd' => sha1($p['user_pass']),
	    	'firstName' => $p['firstName'],
	    	'lastName' => $p['lastName'],
	    	'status' => 1 	
	    );
	    
	    if (!empty($p['facebookID'])) $data['facebookID'] = $p['facebookID'];
	    
	    $this->db->insert('users', $data);
	    
	    $userid = $this->db->insert_id();
		
		// assign user to company
		$this->assignUserToCompany($userid);
		
		// insert default position
		$this->insertUserPosition($userid);
		
		// insert default department
		$this->insertUserDepartment($userid);
		
		return $userid;
    }

    public function changepw ($userid, $password)
    {
        $this->db->set('passwd', sha1($password));
        $this->db->where('id', $userid);
        return $this->db->update('users');
    }
    
	/*
	* when signing up assigns the initial position
	*/ 
	private function insertUserPosition ($userid)
	{
		$userid = intval($userid);
	    
	    if (empty($userid)) throw new Exception ("User id is empty!");
	
		$defaultPosition = $this->functions->getDefaultPosition();
		
		$data = array
	    (
	    	'userid' => $userid,
	    	'company' => $this->config->item('bmsCompanyID'),
	    	'position' => $defaultPosition	
	    );
	    
	    $this->db->insert('userCompanyPositions', $data);
	    
	    return $this->db->insert_id();
	}

	private function insertUserDepartment ($userid)
	{
		$userid = intval($userid);
	    
	    if (empty($userid)) throw new Exception ("User id is empty!");
	
		$defaultDepartment = $this->functions->getDefaultDepartment();
		
		$data = array
	    (
	    	'userid' => $userid,
	    	'company' => $this->config->item('bmsCompanyID'),
	    	'department' => $defaultDepartment	
	    );
	    
	    $this->db->insert('userCompanyDepartments', $data);
	    
	    return $this->db->insert_id();
	}
	
	public function assignUserToCompany ($userid)
	{
		$userid = intval($userid);
	    
	    if (empty($userid)) throw new Exception ("User id is empty!");
	    
		$data = array
		(
			'userid' => $userid,
			'company' => $this->config->item('bmsCompanyID'),
			'homeCompany' => 1
		);
		
		$this->db->insert('userCompanies', $data);
		
		return $this->db->insert_id();
	}
	
	 /**
     * Gets a user ID based upon the e-mail address
     *
     * @param mixed $email 
     *
     * @return INT - userid
     */
    public function getIDFromEmail ($email)
    {
        if (empty($email)) throw new Exception("Email is empty!");

        $mtag = "UserIDFromEmail-{$email}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data)
        {
            $this->db->select('id');
            $this->db->from('users');
            $this->db->where('deleted', 0);
            $this->db->where('email', $email);

            $query = $this->db->get();

            $results = $query->result();

            $data = $results[0]->id;

            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        if (empty($data)) return false;

        return $data;
    }

    /**
     * TODO: short description.
     *
     * @param mixed $user 
     *
     * @return TODO
     */
    public function insertPasswordResetRequest ($user)
    {
        if (empty($user)) throw new Exception("User ID is empty!");
        $requestID = uniqid(null, true);

        $data = array
            (
                'datestamp' => DATESTAMP,
                'userid' => $user,
                'company' => $this->config->item('bmsCompanyID'),
                'requestID' => $requestID,
            );

        $this->db->insert('passwordResets', $data);

        return $requestID;
    }
    
    /**
     * Checks if requestID is in table and active
     * 
     * @param $userid INT, userid
     * @param $requestID STRING, requestID to check in db
     * 
     * @return boolean
     */
    
    public function checkPasswordRequestID($userid, $requestID)
    {
        $this->db->select('id');
        $this->db->from('passwordResets');
        $this->db->where('active', 1);
        $this->db->where('requestID', $requestID);
        $this->db->where('userid', $userid);
        $query = $this->db->get();
        return ($query->num_rows() > 0);
    }
    
    public function processPasswordReset($userid, $requestID)
    {
        $this->db->set('active', 0);
        $this->db->set('processed', DATESTAMP);
        $this->db->where('userid', $userid);
        $this->db->where('requestID', $requestID);
        return $this->db->update('passwordResets');
    }
    
    public function verifyAccount($userid, $verifyHash)
    {
        $this->db->set('verified', 1);
        $this->db->set('verifyProcessed', DATESTAMP);
        $this->db->where('userid', $userid);
        $this->db->where('verifyHash', $verifyHash);
        $this->db->where('verified', 0);
        $this->db->update('verifyAccounts');
        return ($this->db->affected_rows() > 0);
    }
    
    public function setVerifyHash($userid)
    {
        $hash = sha1($userid . DATESTAMP);
        $data = array(
            'userid'        => $userid,
            'verifyHash'    => $hash,
            'verifySent'    => DATESTAMP
        );
        $this->db->insert('verifyAccounts', $data);
        return $hash;
    }
}

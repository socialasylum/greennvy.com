<?php

class messages_model extends CI_Model
{

    public function __Construct()
    {
        parent::__Construct();
    }

    /*
     * Gets the chat threads from the database
     * (I dont really like using the Codeingiter active record. I will be refactoring my calls to only use SQL for this since some can become quite complex
     */
    public function getChatThreads($user){

        //generate the query using a union on the same table (checking to see if they both sent or received the message as a part of a thread.
        $query = $this->db->query('SELECT c.chat_id, c.u_id_1 as user_id, c.u_id_2 as chat_partner, c.updated_time, u.firstName as partner_first_name, u.lastName as partner_last_name, "" as profileimg from chat_ids c INNER JOIN users u ON c.u_id_2 = u.id WHERE c.u_id_1 = "'.$this->db->escape_str($user).'"
        UNION ALL SELECT
            c.chat_id, c.u_id_2 as user_id, c.u_id_1 as chat_partner, c.updated_time, u.firstName as partner_first_name, u.lastName as partner_last_name, "" as profileimg from chat_ids c INNER JOIN users u on c.u_id_1 = u.id WHERE c.u_id_2 = "'.$this->db->escape_str($user).'"
        UNION ALL SELECT
            c.chat_id, c.u_id_1 as user_id, c.u_id_2 as chat_partner, c.updated_time, l.name as partner_first_name, "" as partner_last_name, (SELECT fileName from locationImages WHERE id = "'.$this->db->escape_str($user).'" ORDER BY datestamp DESC LIMIT 1) as profileimg FROM chat_ids c INNER JOIN userLocations ul ON c.u_id_2 = ul.location INNER JOIN locations l ON l.id = c.u_id_2 WHERE c.u_id_1 = "'.$this->db->escape_str($user).'"
        UNION ALL SELECT
            c.chat_id, c.u_id_2 as user_id, c.u_id_1 as chat_partner, c.updated_time, l.name as partner_first_name, "" as partner_last_name, (SELECT fileName from locationImages WHERE id = "'.$this->db->escape_str($user).'" ORDER BY datestamp DESC LIMIT 1) as profileimg FROM chat_ids c INNER JOIN userLocations ul ON c.u_id_1 = ul.location INNER JOIN locations l ON l.id = c.u_id_1 WHERE c.u_id_2 = "'.$this->db->escape_str($user).'"
        UNION ALL SELECT
            c.chat_id, c.u_id_1 as user_id, c.u_id_2 as chat_partner, c.updated_time, u.firstName as partner_first_name, u.lastName as partner_last_name, "" as profileimg FROM chat_ids c INNER JOIN userLocations ul ON c.u_id_1 = ul.location INNER JOIN locations l ON ul.location = l.id INNER JOIN users u on c.u_id_2 = u.id WHERE ul.userid = "'.$this->db->escape_str($user).'"
        UNION ALL SELECT
            c.chat_id, c.u_id_2 as user_id, c.u_id_1 as chat_partner, c.updated_time, u.firstName as partner_first_name, u.lastName as partner_last_name, "" as profileimg FROM chat_ids c INNER JOIN userLocations ul ON c.u_id_2 = ul.location INNER JOIN locations l ON ul.location = l.id INNER JOIN users u on c.u_id_1 = u.id WHERE ul.userid = "'.$this->db->escape_str($user).'"
        ORDER BY updated_time DESC');

        //get the last post of each thread then push it into the array for the ui
        if ($query->num_rows() > 0) {

            //put the results in the threads var
            $threads = $query->result();

            //we need to get the last message for the ui.
            foreach ($threads as $thread){

                //query for the message based on the chat ID
                $query = $this->db->query('SELECT message, recieved, sender from chat_messages WHERE chat_id = "'.$this->db->escape_str($thread->chat_id).'" ORDER BY date DESC LIMIT 1');

                //get the result object
                $result = $query->result();

                //if the chat partner and this users id do not the chats user_id field then its a company. append the info
                if($thread->user_id != $this->session->userdata('userid')){
                    $query = $this->db->query('SELECT name FROM locations where id = "'.$thread->user_id.'"');
                    $nameData = $query->row();
                    $thread->coName = $nameData->name;
                }

                //add the message to the output
                $thread->last_message = $result[0]->message;

                $thread->last_message_sender = $result[0]->sender;

                $thread->recieved = $result[0]->recieved;

            }

            //return the final data
            return $threads;
        } else {
            //if its blank then return nothing
            return null;
        }
    }


    /*
     * function for if we need to find a valid company
     */

    protected function findCompany($id) {

        $query = $this->db->query('SELECT id FROM chat_messages where sender = "'.$this->db->escape_str($id).'"');

        if($query->num_rows() == 0){
            return true;
        }
    }

    /*
     * returns basic name a profile image if needed
     */

    public function getInfo($data) {

        //get the data that is needed (if available)
        $findCo = $this->checkForCompany($data['chat_id']);

        if($findCo) {
            //get the company ID
            if ($this->findCompany($findCo->u_id_1)) {
                $company = $findCo->u_id_1;
            }

            if ($this->findCompany($findCo->u_id_2)) {
                $company = $findCo->u_id_2;
            }
        }

        //verify that this user is the company in question if so set the flag
        $query = $this->db->query('SELECT id FROM userLocations WHERE location = "'.$this->db->escape_str($company).'" AND userid = "'.$data['sender'].'"');

        if($query->num_rows() > 0) {
            $coSender = '1';
        }


        //if this returns a valid result there is a company present.
        if($company && $coSender == '1'){
            $query = 'SELECT name as sender_first_name, "" as sender_last_name, "1" as company, "' . $this->db->escape_str($company). '" as sender, (SELECT fileName from locationImages WHERE id = "' . $this->db->escape_str($company) . '" ORDER BY datestamp DESC LIMIT 1) as profileImg FROM locations WHERE id = "' . $this->db->escape_str($company) . '"';
        } else {
            $query = 'SELECT firstName as sender_first_name, lastName as sender_last_name, "" as profileImg, "'. $this->db->escape_str($data['sender']) .'" as sender FROM users WHERE id = "'.$this->db->escape_str($data['sender']).'"';
        }
        return $this->db->query($query)->row();

    }

    /*
     * Checks to see if a poster is a part of a company
     */

    public function checkForCompany($data) {

        $query = $this->db->query('SELECT u_id_1, u_id_2 FROM chat_ids WHERE chat_id = "'.$this->db->escape_str($data).'" AND COMPANY = "1"');

        if($query->num_rows() > 0 ) {
            //if there is a company return the row
            return $query->row();
        } else {
            //if there is not a company return false
            return false;
        }
    }

    /*
     * sets the messages to recieved on page load
     */
    public function setRecived($data, $user)
    {
        $this->db->query('UPDATE chat_messages SET recieved = "1" WHERE chat_id ="' . $this->db->escape_str($data) . '" AND sender !="' . $this->db->escape_str($user) . '"');
    }

    /*
     * gets the company image only
     */

    public function coImg($data){

        return $this->db->query('SELECT fileName from locationImages WHERE id = "'.$this->db->escape_str($data).'" ORDER BY datestamp DESC LIMIT 1')->row();

    }

     /*
     * get the needed chat list, this should be used for a first time load (through ajax or through landing on the page for the first time, should not be used for getting new messages)
     */

    public function getChat($data) {

        //because we are able to send messages as companies. We need to gather the ids and check to see if one of them contains the company id. If this query returns there is a valid company
        $findCo = $this->checkForCompany($data);

        if($findCo) {

            //create a query builder variable
            $messageQuery = '';

            //check if the first id is the company. if ID matches a sender in the messages table, it is not a company message
            if($this->findCompany($findCo->u_id_1)){
               $messageQuery .= 'SELECT m.message, m.sender, m.date, l.name as sender_first_name, "" as sender_last_name, (SELECT fileName from locationImages WHERE id = "'.$this->db->escape_str($findCo->u_id_1).'" ORDER BY datestamp DESC LIMIT 1) as profileimg from chat_messages m INNER JOIN userLocations ul on m.sender = ul.userid INNER JOIN locations l on ul.location = l.id WHERE  m.chat_id = "'.$this->db->escape_str($data).'" AND l.id = "'.$this->db->escape_str($findCo->u_id_1).'"';
            } else {
               $messageQuery .= 'SELECT m.message, m.sender, m.date, u.firstName as sender_first_name, u.lastName as sender_last_name , "" as profileimg from chat_messages m INNER JOIN users u on m.sender = u.id where m.chat_id = "'.$this->db->escape_str($data).'" AND m.sender = "'.$this->db->escape_str($findCo->u_id_1).'"';
            }

            //check if the second id is the company. if ID matches a sender in the messages table, it is not a company message
            if($this->findCompany($findCo->u_id_2)){
                $messageQuery .= ' UNION ALL SELECT m.message, m.sender, m.date, l.name as sender_first_name, "" as sender_last_name, (SELECT fileName from locationImages WHERE id = "'.$this->db->escape_str($findCo->u_id_2).'" ORDER BY datestamp DESC LIMIT 1) as profileimg from chat_messages m INNER JOIN userLocations ul on m.sender = ul.userid INNER JOIN locations l on ul.location = l.id WHERE  m.chat_id = "'.$this->db->escape_str($data).'" AND l.id = "'.$this->db->escape_str($findCo->u_id_2).'"';
            } else {
                $messageQuery .= ' UNION ALL SELECT m.message, m.sender, m.date, u.firstName as sender_first_name, u.lastName as sender_last_name , "" as profileimg from chat_messages m INNER JOIN users u on m.sender = u.id where m.chat_id = "'.$this->db->escape_str($data).'" AND m.sender = "'.$this->db->escape_str($findCo->u_id_2).'"';
            }

            //order it by date ASC
                $messageQuery .= ' ORDER BY date ASC';

        } else {
            $messageQuery = 'SELECT m.message, m.sender, m.date, u.firstName as sender_first_name, u.lastName as sender_last_name , "" as profileimg from chat_messages m INNER JOIN users u on m.sender = u.id where m.chat_id = "'.$this->db->escape_str($data).'" ORDER BY date ASC';
        }

        // query for the chat from the database, we want the oldest messages on top so do it by date ascending.
        $query = $this->db->query($messageQuery);

        //if there is data we should return it, check the number of rows
        if($query->num_rows() > 0){

            //before we return this we need to set messages as recieved so we do not pull these again with ajax updates
            $this->db->query('UPDATE chat_messages SET recieved = "1" WHERE chat_id ="'.$this->db->escape_str($data['chat_id']).'" AND sender !="'.$this->db->escape_str($this->session->userdata('userid')).'"');

            //return the data
            return $query->result();
        } else {

            //if there are no results return a null string
            return null;
        }

    }

    /*
     * Gets New Messages from the database
     */

    function get_new_messages($data)
    {

        //generate the query
        $query = $this->db->query('SELECT c.message, c.date, c.sender FROM chat_messages c WHERE chat_id="'.$this->db->escape_str($data['chat_id']).'" AND sender !='.$this->db->escape_str($data['current_user']).' AND recieved = "0" ORDER BY date ASC');

        //if we have results lets append needed data to it
        $results = $query->result();

        //check if we have any results
        if($results != null){

            //check to see if the sender is a company
            $isCompany = $this->checkForCompany($data['chat_id']);

            if($isCompany) {
                //get the company ID
                if ($this->findCompany($isCompany->u_id_1)) {
                    $company = $isCompany->u_id_1;
                }

                if ($this->findCompany($isCompany->u_id_2)) {
                    $company = $isCompany->u_id_2;
                }
            }


            //loop over each one to get the name of the individual or company
            foreach($results as $result){

                //verify that this user is the company in question
                $query = $this->db->query('SELECT id FROM userLocations WHERE location = "'.$this->db->escape_str($company).'" AND userid = "'.$this->db->escape_str($result->sender).'"');

                if($query->num_rows() > 0) {
                    $companySender = '1';
                }

                if($isCompany && $companySender == 1) {
                    //if the isCompany is true, generate the query to get that companies information
                    $queryString = 'SELECT name as sender_first_name, "" as sender_last_name, "'.$this->db->escape_str($company).'" as senderCompany, "1" as company, (SELECT fileName from locationImages WHERE id = "'.$this->db->escape_str($company).'" ORDER BY datestamp DESC LIMIT 1) as profileimg FROM locations where id = "'.$this->db->escape_str($company).'"';
                } else {
                    $queryString = 'SELECT firstName as sender_first_name, lastName as sender_last_name,  "" as profileimg  FROM users where id="'.$this->db->escape_str($result->sender).'"';

                }

                //build the new query
                $query = $this->db->query($queryString);

                //get the name of this user out of the query and append it to the result of the first query
                $name = $query->result();

                if($name[0]->company == '1'){
                    $result->company = '1';
                }

                if($name[0]->senderCompany){
                    $result->senderCompany = $name[0]->senderCompany;
                }
                $result->firstname = $name[0]->sender_first_name;
                $result->lastname = $name[0]->sender_last_name;
                $result->profileImg = $name[0]->profileimg;

                //now that we have the new messages set the received to 1 we don't want to pull these again
                $this->db->query('UPDATE chat_messages SET recieved = "1" WHERE sender !="'.$this->db->escape_str($data['current_user']).'" AND chat_id ="' . $this->db->escape_str($data['chat_id']) . '"');

            }

            //return the result json encode it
            return $results;
        } else {
            return false;
        }
    }

    /*
     * gets the users followers and dispensaries
     */
    function getFollowersAndDispensaries($id){

        //generate the query of users the current user is following
        $query = $this->db->query('SELECT f.followingUser, u.firstName, u.lastName from userFollow f INNER JOIN users u ON f.followingUser = u.id WHERE userid="'.$this->db->escape_str($id).'"');

        //if there are results then push them into the messagble array
        if($query->num_rows() > 0){

            //create a return array
            $messageable['users'] = $result = $query->result();
        }

        //generate the query for the despensories
        $query = $this->db->query('SELECT l.id, l.name,(SELECT fileName from locationImages WHERE id = l.id ORDER BY datestamp DESC LIMIT 1) as profile_image FROM locations l');

        //if there are results push the into the messagble array
        if($query->num_rows() > 0) {

            //create a return array
            $messageable['dispensories'] = $query->result();
        }

        return $messageable;

    }

    /*
     * sends a message to the database
     */
    function sendmessage($data){

        $this->db->query('INSERT INTO chat_messages (chat_id, message, sender, date ) VALUES ("'.$this->db->escape_str($data['chat_id']).'", "'.$this->db->escape_str($data['message']).'", "'.$this->db->escape_str($data['sender']).'", "'.date("Y-m-d H:i:s").'")');
        return 'yes';
    }

    /*
     * updates or inserts the chat thread
     */
    function updateChatThread($data){
        $check = $this->db->get_where('chat_ids', array('chat_id' => $data['chat_id']));

        if($check->num_rows() > 0){
            $this->db->query('UPDATE chat_ids SET updated_time = "'.date("Y-m-d H:i:s").'" WHERE chat_id = "'.$this->db->escape_str($data['chat_id']).'"');
        } else {
            $this->db->query('INSERT INTO chat_ids (chat_id, u_id_1, u_id_2, updated_time, company) VALUES ("'.$this->db->escape_str($data['chat_id']).'", "'.$this->db->escape_str($data['sender']).'", "'.$this->db->escape_str($data['reciever']).'", "'.date("Y-m-d H:i:s").'", "'.$this->db->escape_str($data['company']).'")');
        }
    }

    function verifyUser($data){

        //generate the query
        $query = $this->db->query('SELECT u_id_1 FROM chat_ids WHERE u_id_1 = "'.$this->db->escape_str($data['current_user']).'" AND chat_id ="' . $this->db->escape_str($data['chat_id']) . '" UNION ALL
         SELECT u_id_1 FROM chat_ids WHERE u_id_2 = "'.$this->db->escape_str($data['current_user']).'" AND chat_id ="' . $this->db->escape_str($data['chat_id']) . '"');

        //if there is data we should return it, check the number of rows
        if($query->num_rows() > 0){
            return $query->result();
        } else {
            return $query->result();
        }

    }



}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'abstract_model.php';

class locations_model extends abstract_model {

    public $id;
    public $dispensaryid;
    public $datestamp;
    public $createdBy;
    public $company;
    public $storeID;
    public $name;
    public $address;
    public $address2;
    public $address3;
    public $city;
    public $state;
    public $postalCode;
    public $territory;
    public $openDate;
    public $deleted;
    public $deletedBy;
    public $deleteDate;
    public $rent;
    public $utilities;
    public $description;
    public $addDescription;
    public $phone;
    public $fax;
    public $websiteUrl;
    public $email;
    public $lat;
    public $lng;
    public $salesTax;
    public $googleID;
    public $googleReference;
    public $formattedAddress;
    public $googleHTMLAddress;
    public $active;
    public $allowUpdate;
    public $lastUpdate;

    function __construct() {
        parent::__construct();
    }

    public function getLocationById($id = null) {
        if (is_null($id) || !is_numeric($id))
            return null;

        $info = $this->grabGeoIP();

        if (!is_null($id)) {
            $query = $this->db->query("
                SELECT
                        *
                FROM locations 
    	        where id = $id
            ");
            return $query->first_row();
        }
        return null;
    }

    public function getLocationsRank() {

        $this->db->select('*');
        $this->db->from('locations');
        $this->db->where('active', 1);
        $this->db->order_by('rank', 'desc');
        $this->db->limit('20');

        $query = $this->db->get();
        return $query->result();
    }

    public function getCoverImage($locationid) {
        $mtag = "locationCoverImage-{$locationid}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->select('fileName');
            $this->db->from('locationImages');
            $this->db->where('locationid', $locationid);
            $this->db->where('isCover', 1);
            $this->db->where('active', 1);
            $this->db->limit(1);
            $query = $this->db->get();
            $data = $query->first_row();
            if (empty($data)) {
                $this->db->select('fileName');
                $this->db->from('locationImages');
                $this->db->where('locationid', $locationid);
                $this->db->where('active', 1);
                $this->db->order_by('imgOrder DESC');
                $this->db->limit(1);
                $query = $this->db->get();
                $data = $query->first_row();
            }
            if (empty($data)) {
                $data = new stdClass();
                $data->fileName = false;
            }
            $this->cache->memcached->save($mtag, $data->fileName, $this->config->item('cache_timeout'));
            $data = $data->fileName;
        }

        return $data;
    }

    public function getLocations() {

        $query = $this->db->query('select * from locations');

        if ($query->num_rows() > 0)
            return( $query->result() );

        return null;
    }

    public function getMyLocations($userid) {
        $query = $this->db->query("select r.*, l.*, u.profileimg from locationReviews as r 
                                   join locations as l on(l.id = r.location)
                                   join users as u on(r.userid = u.id)
                                   where r.comment IS NOT NULL
                                   and u.id = $userid;");
        if ($query->num_rows() > 0) {
            var_dump($query->result());
        }

        return null;
    }

    public function save($params) {
        $params = $this->cleanseParams($params);

        if (!empty($params['id'])) {
            $deals = $this->getLocationById($params['id']);

            if (count($deals) > 0) {
                $this->db->where('id', $params['id']);
                $this->db->update('locations', $params);
            }
        } else {
            $this->db->insert('locations', $params);
        }

        return true;
    }

    public function getLocationMainImage($location)
    {
        $location = intval($location);

        if (empty($location))
            throw new Exception("Location ID is empty!");

        $mtag = "locationMainImg-{$location}";
        $data = $this->cache->memcached->get($mtag);
        $data = false;
        if (!$data) {
            $this->db->select('fileName');
            $this->db->from('locationImages');
            $this->db->where('locationid', $location);
            $this->db->where('active', 1);
            $this->db->order_by('profileImage DESC, id DESC, imgOrder DESC');
            $this->db->limit(1);
            $query = $this->db->get();
            $data = $query->first_row()->fileName;
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function writeCoverImage($locationid, $filename)
    {
        $path = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $locationid . DS . $filename;
        $coverPath = $_SERVER["DOCUMENT_ROOT"] . 'public' . DS . 'uploads' . DS . 'locationImages' . DS . $locationid . DS . 'cover_' . $filename;
        $ext = array_pop(explode('.', $filename));
        $image = '';
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                $image = imagecreatefromjpeg($path);
                break;
            case 'gif':
                $image = imagecreatefromgif($path);
                break;
            case 'png':
                $image = imagecreatefrompng($path);
                break;
        }
        for ($i = 0; $i < 40; $i++) {
            imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR);
        }
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
                $image = imagejpeg($image, $coverPath);
                break;
            case 'gif':
                $image = imagegif($image, $coverPath);
                break;
            case 'png':
                $image = imagepng($image, $coverPath);
                break;
        }
        imagedestroy($image);
    }

    public function getPhotoFilename($imageid) {
        $imageid = intval($imageid);
        $this->db->where('id', $imageid);
        $this->db->select('fileName');
        $query = $this->db->get('locationImages');
        return $query->first_row()->fileName;
    }

    public function getImageByID($location, $id) {
        $location = intval($location);
        $id = intval($id);

        if (empty($location))
            throw new Exception("Location ID is empty!");
        if (empty($id))
            throw new Exception("Image ID is empty!");

        $mtag = "locationImgByID-{$location}-{$id}";

        $data = $this->cache->memcached->get($mtag);

        if (!$data) {
            $this->db->select('fileName');
            $this->db->from('locationImages');
            $this->db->where('locationid', $location);
            $this->db->where('id', $id);
            $query = $this->db->get();
            $results = $query->result();
            $data = $results[0]->fileName;
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }

        return $data;
    }

    public function insertPhoto($locationid, $filename, $isCover, $caption = '', $isProfileImage = 0, $imgOrder = 10000)
    {   
        $locationid = intval($locationid);
        $filename = $this->db->escape($filename);
        $caption = $this->db->escape($caption);
        $query = "
            INSERT INTO locationImages
            (
                    locationid
                ,   userid
                ,   company
                ,   datestamp
                ,   fileName
                ,   imgOrder
                ,   caption
                ,   isCover
                ,   wasCover
                ,   profileImage
            )
            VALUES
            (
                    $locationid
                ,   " . $this->session->userdata('userid') . "
                ,   41
                ,   NOW()
                ,   $filename
                ,   $imgOrder
                ,   $caption
                ,   $isCover
                ,   $isCover
                ,   $isProfileImage
            )
        ";
        $this->db->query($query);
        $mtag = "locationMainImg-{$locationid}";
        $this->cache->memcached->delete($mtag);
        if ($isCover)
        {
            $this->writeCoverImage($locationid, $filename);
        }
        return $this->db->insert_id();
    }

    public function updateCaption($imageid, $caption, $isCover, $isProfileImage, $imgOrder = 10000)
    {
        $imageid = intval($imageid);
        $this->db->set('profileImage', $isProfileImage);
        $this->db->set('imgOrder', $imgOrder);
        $this->db->set('caption', $caption);
        $this->db->set('isCover', $isCover);
        if ($isCover > 0) {
            $this->db->set('wasCover', 1);
        }
        $this->db->where('id', $imageid);
        try {
            $this->db->update('locationImages');
            if ($isCover)
            {
                $this->db->where('id', $imageid);
                $this->db->select('fileName, locationid');
                $row = $this->db->get('locationImages')->first_row();
                $fileName = $row->fileName;
                $locationid = $row->locationid;
                $this->writeCoverImage($locationid, $fileName);
            }
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
    }

    public function clearCoverPhotos($locationid) {
        $locationid = intval($locationid);
        $this->db->set('isCover', 0);
        $this->db->where('locationid', $locationid);
        $this->db->update('locationImages');
    }
    
    public function clearProfileImage($locationid)
    {
        $locationid = intval($locationid);
        $this->db->set('profileImage', 0);
        $this->db->where('locationid', $locationid);
        $this->db->update('locationImages');
    }

    public function getImages($locationid) {
        $mtag = "locationImages-{$locationid}";
        $data = $this->cache->memcached->get($mtag);
        /*
         * Skipping memcache for a while to see if this resolves the issue
         */
        $data = false;
        if (!$data) {
            $this->db->from('locationImages');
            $this->db->where('locationid', $locationid);
            $this->db->where('active', 1);
            $this->db->order_by('imgOrder');
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }

    public function deleteImg($imageid) {
        $imageid = intval($imageid);
        $this->db->set('active', 0);
        $this->db->where('id', $imageid);
        try {
            $this->db->update('locationImages');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
    }

    public function insertLocationVideo($p) {
        $p['location'] = intval($p['location']);

        if (empty($p['location']))
            throw new Exception("Location ID is empty!");

        $videoID = array();
        preg_match('/([A-Za-z0-9]){11}/', $p['url'], $videoID);

        $data = array(
            'userid' => $this->session->userdata('userid'),
            'locationid' => $p['location'],
            'company' => $this->config->item('bmsCompanyID'),
            'datestamp' => DATESTAMP,
            'url' => $p['url'],
            'videoOrder' => $p['order'],
            'title' => $p['title'],
            'thumbnail' => $p['thumbnail'],
            'description' => $p['description'],
            'videoID' => $videoID[0]
        );

        $this->db->insert('locationYouTubeVideos', $data);

        return $this->db->insert_id();
    }

    public function getYouTubeEmbedURL($videoID) {
        return "https://youtube.com/embed/$videoID";
    }

    public function updateLocationVideo($p) {
        $videoid = intval($p['videoID']);
        $this->db->set('title', $p['title']);
        $this->db->set('description', $p['description']);
        $this->db->where('id', $videoid);
        try {
            $this->db->update('locationYouTubeVideos');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
    }

    public function deleteVideo($videoid) {
        $videoid = intval($videoid);
        $this->db->set('active', 0);
        $this->db->where('id', $videoid);
        try {
            $this->db->update('locationYouTubeVideos');
        } catch (Exception $e) {
            $this->functions->sendStackTrace($e);
        }
    }

    public function getLocationVideos($locationid) {
        $mtag = "locationVideos-{$locationid}";
        $data = $this->cache->memcached->get($mtag);
        if (!$data) {
            $this->db->from('locationYouTubeVideos');
            $this->db->where('locationid', $locationid);
            $this->db->where('active', 1);
            $this->db->order_by('videoOrder');
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }

    public function getUploadCount($locationid) {
        $mtag = "locationUploadCount-{$locationid}";
        $data = $this->cache->memcached->get($mtag);
        if (!$data) {
            $sql = "
                SELECT
                        COUNT(*) as total
                FROM gstDEV.locationImages li,
                (
                        SELECT
                                COUNT(*) as total
                        FROM gstDEV.locationYouTubeVideos
                        WHERE locationid = $locationid
                            AND active = 1
                ) lytv
                WHERE li.locationid = $locationid
                    AND active = 1;
            ";
            $query = $this->db->query($sql);
            $data = $query->first_row()->total;
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }

    public function follow($locationid) {
        $sql = "
            REPLACE INTO userFollow
            (
                    datestamp
                ,   userid
                ,   locationid
                ,   followingUser
                ,   active
                ,   dateApproved
            )
            VALUES
            (
                    NOW()
                ,   0
                ,   $locationid
                ,   " . $this->session->userdata('userid') . "
                ,   1
                ,   NOW()
            )
        ";
        $this->db->query($sql);
    }

    public function unfollow($locationid) {
        $sql = "
            REPLACE INTO userFollow
            (
                    datestamp
                ,   userid
                ,   locationid
                ,   followingUser
                ,   active
                ,   dateApproved
            )
            VALUES
            (
                    NOW()
                ,   0
                ,   $locationid
                ,   " . $this->session->userdata('userid') . "
                ,   0
                ,   NOW()
            )
        ";
        $this->db->query($sql);
    }

    public function isFollowing($locationid) {
        $mtag = "locationIsFollowing-{$locationid}";
        $data = $this->cache->memcached->get($mtag);
        if (!$data) {
            $sql = "
                SELECT
                        COUNT(*) AS isFollowing
                FROM userFollow
                WHERE followingUser = " . $this->session->userdata('userid') . "
                    AND locationid = $locationid
                    AND active = 1
            ";
            $query = $this->db->query($sql);
            $data = ($query->first_row()->isFollowing > 0);
            $this->cache->memcached->save($mtag, $data, $this->config->item('cache_timeout'));
        }
        return $data;
    }

    public function getFullList() {
        $this->db->select('id, name');
        $this->db->from('locations');
        $this->db->order_by('name', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getRankInfo($id) {
        $this->db->select('id, name, formattedAddress, rank');
        $this->db->from('locations');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->first_row();
    }

    public function updateRank($data) {
        $update = array(
            'rank' => $data['rank']
        );
        $this->db->where('id', $data['id']);
        $this->db->update('locations', $update);
        return true;
    }

}

DROP TABLE IF EXISTS `claimListings`;

CREATE TABLE `claimListings` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `datestamp` datetime DEFAULT NULL,
    `listingID` INT(10) UNSIGNED DEFAULT 0,
    `name` VARCHAR(150) DEFAULT NULL,
    `phone` VARCHAR(150) DEFAULT NULL,
    `email` VARCHAR(150) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

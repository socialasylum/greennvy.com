/**
 * Created by SA MBP1 on 11/20/2014.
 */
var base = window.location.hostname;


/*
 |------------------------------------------------------------------------
 |create a random string
 |------------------------------------------------------------------------
 */
 function makeid()
 {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 50; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
 }

/*
 |-------------------------------------------------------------------------
 | Funtion to trigger the refresh event
 |-------------------------------------------------------------------------
 */
var refresh;

function beginRefresh()
{
    refresh = setInterval(function()
    {
        var timestring = new Date().getTime();
        $('#chatmessage').load(base+'chat/index/'+ timestring);
    }, 2000);
}
/*
 |----------------------------------------------------------------------------
 | function to Stop refreshing
 |----------------------------------------------------------------------------
 */

function endrefresh()
{
    clearInterval(refresh);
}

/*-------------------------------------------------------------------------
 * spit a string to an ammount of words
 * -----------------------------------------------------------------------
 */

function trim_words(theString, numWords) {
    expString = theString.split(/\s+/,numWords);
    theNewString=expString.join(" ");
    return theNewString;
}

/*
 |------------------------------------------------------------------------
 | uppercase first letter in a string
 |------------------------------------------------------------------------
 */

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/*
 |-------------------------------------------------------------------------
 | Displaying the online users popup by clicking the open chat button
 |-------------------------------------------------------------------------
 */
jQuery(function( $ ){
    var chat_btn = $("#chat-btn");
    var chat_area = $("#chat-area");
    var users_online = $("#users");

    chat_btn.attr( "href", "javascript:void( 0 )" ).click(function()
    {
        chat_area.hide();
        users_online.toggle();
        $("#messagediv").html('');
        endrefresh();
        var timestring = new Date().getTime();
        $('#onlineusers').load(base+'users/index/'+ timestring);
        chat_btn.blur();
        return (false);
    });
    $(document).click(function(event){
        if (users_online.is( ":visible" ) && !$( event.target ).closest( "#users" ).size()){
            users_online.hide();
        }

    });
});

/*
 |---------------------------------------------------------------------------
 | onload functions
 |---------------------------------------------------------------------------
 */
$(document).ready(function() {

    $('#newName').hide();

    //items for chat page
    $('#chatpage').on('submit', function(e){
        e.preventDefault();
        var text = $("#message").val();
        var token = $("#csrftoken").val();
        var chat = $('#cid').val();
        var name = $('#sender').val();
        var to = $('#to').val();
        var co = $('#co').val();
        $.post('http://'+base+'/messages/sendchat', {message: text, karateToken: token, cid: chat, reciever: to, co: co }, function(result){
            var returnData = jQuery.parseJSON(result);
            if (returnData.result == 'success'){
               $("#chatPanel").append('<div class="message_mid"><div class="icon_box-1"><span class="fa-stack fa-2x"><img src="'+returnData.img+'" width="50" height="50"/></span></span></div><div class="mid_message_left"><div class="mid_title">'+returnData.name+'</div><div class="inbox_date">'+returnData.timestamp+'</div><div class="clear"></div><div class="mid_message_text">'+text+'</div></div></div>');
               $("#message").val('');
			   var shortText = jQuery.trim(text).substring(0, 20).split(" ").slice(0, -1).join(" ") + "...";
			   var flag = 1;
			   $('.box_inner').each(function(){
					if($(this).data('chat-id') == chat) {
						$(this).find('.message_text').html(shortText);
						$(this).find('.inbox_date').html(returnData.timestamp);
						flag = 0;
					} 
			   });
			   if(flag == 1)
			    {
					var toName = $('.toName').html();
					var toId =  $('#to').val();
					$('.chatlist').prepend('<div class="box_inner" data-chat-id="'+chat+'"><div class="icon_box-1"><img src="'+ returnData.recimg +'" width="50" height="50"/></div><div class="message_left"><div class="inbox_title"><a href="#" data-chat_id="" class="chat_thread">'+trim_words(toName, 2)+'</a></div><div class="inbox_date" data-chat-id-time="">'+returnData.timestamp+'</div><div class="clear"></div><div class="message_text active" data-chat-id-message="">'+shortText+'</div></div></div>');
				}
			}
        });
    });

    //checks for new messages
    var checkMessages = function(){
        var token = $("#csrftoken").val();
		var chatId = $("#cid").val();
		var uid = $("#uid").val();
        var from = $('.toName').html();
        $.post('http://'+base+'/messages/getNewChatMessages', {id: chatId, karateToken: token, uid : uid}, function(result) {
            if (result.length != 0) {
                var messageReturn = jQuery.parseJSON(result);
                jQuery.each(messageReturn, function() {
                    var shortText = jQuery.trim(this.message).substring(0, 20).split(" ").slice(0, -1).join(" ") + "...";
                    $("#chatPanel").append('<div class="message_mid"><div class="icon_box-1"><span class="fa-stack fa-2x"><img src="'+ this.profileImg +'" width="50" height="50"/></span></div><div class="mid_message_left"><div class="mid_title">' + this.firstname +' '+ this.lastname+'</div><div class="inbox_date">' + this.date + '</div><div class="clear"></div><div class="mid_message_text">' + this.message + '</div></div></div>');
					$('.box_inner').each(function(){
						if($(this).data('chat-id') == chatId) {
                            $(this).find('.message_text').html(shortText);
							$(this).find('.inbox_date').html(returnData.timestamp);
						}
					});
                });

            }
        });
    };

    //switches threads on the page
    $('.chat_thread').on('click', function(e){

        $('.message_mid').fadeOut('fast');

        //changes the chat id
        var newId = $(this).data("chat_id");
		var newUid = $(this).data('chat-u-id');
        $('input#cid').val(newId);
		$('input#uid').val(newUid);

        var name = $(this).text();

        $('.toName').text(name.toUpperCase());

        //adds and removess the actived class
        $('.chat_thread').removeClass('active');
        $(this).addClass('active');

        var token = $("#token").val();
        $.post('http://'+base+'/messages/getMessagesFromThread', {karateToken: token, cid: newId, id: newUid}, function(result){
            if(result != 'error'){
			
				if( $('#newName').is(':visible') ) {
				    $('.toName').toggle();
					$('#newName').toggle();
				}
				
                var messages = jQuery.parseJSON(result);

                $('.message_mid').fadeOut('fast');
                jQuery.each(messages, function(){
                    $("#chatPanel").append('<div class="message_mid"><div class="icon_box-1"><span class="fa-stack fa-2x"><img src="'+ this.profileImg +'" width="50" height="50"/></span></div><div class="mid_message_left"><div class="mid_title">'+ this.sender_first_name.capitalize() +' '+ this.sender_last_name.capitalize() +'</div><div class="inbox_date">'+ this.date +'</div><div class="clear"></div><div class="mid_message_text">'+ this.message +'</div></div></div>');
                });

            }
        });
    });

    //gets new messages
    setInterval(checkMessages, 5000);

    /*
     * This will remove or show a named div based on what is input in a text field. This template should be used for a single item.
     */
    $('#userSearch').keyup(function() {

        //set the vars
        var value = $(this).val();
        var exp = new RegExp('^' + value, 'i');


        $('.box_inner').each(function() {
            if(exp.test($('.chat_thread', this).text())) {
                $(this).slideDown('slow');
            } else {
                if($(this).is(":visible")){
                    $(this).slideUp('slow');
                }
            }
        });
    });

    $('.compose_butt').on('click', function(e){
        $('.message_mid').fadeOut('fast');
        $('.toName').toggle();
        $('#newName').toggle();
		$('#newMessageTo').val('');
    });

    /*
     * Returns a list of messageable people
     */
    $("#newMessageTo").autocomplete({
        source: autoCompleteData,
        select: function(event, ui){
            var id = makeid();
            $('#newName').val(ui.item.label);
            $('.toName').html(ui.item.label);
			$('#newMessageTo').val('');
            $('#co').val(ui.item.co);
            $('.toName').toggle('hide_name');
            $('#newName').toggle();
            $('#cid').val(id);
            $('#to').val(ui.item.value)
			
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item){
        var inner_html = '<a><div class="list_name_container">' + item.photo + '<div class="label">' + item.label + '</div></div></a>';
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append(inner_html)
            .appendTo( ul );
    };



});


var profile = {};

profile.indexInit = function (lat, lng)
{
    if ($('#previewMap').exists())
    {
        if (lat == 0)
            lat = undefined;
        if (lng == 0)
            lng = undefined;

        gm.initialize('previewMap', lat, lng, 3, false);

        gm.loadSavedMarkers(false, false, false, '#savedMarkers');
    }

    var hash = window.location.hash;

    if(hash.length > 1) {
        var hash = hash.slice(1);
        $('.tab-pane').removeClass('active');
        $('#'+hash).addClass('active');
    }

    $('#listCreateBtn').removeAttr('disabled', 'disabled');

    $('#listCreateBtn').click(function (e) {
        $(this).prepend(global.spinner);
        $(this).attr('disabled', 'disabled');
        window.location = '/profile/vendoredit';
    });

    if ($('#connectFBbtn').exists())
    {
        $('#connectFBbtn').click(function (e) {
            fb.login($(this), false, true);
        });
    }

    if ($('#unlinkFBbtn').exists())
    {
        $('#unlinkFBbtn').click(function (e) {
            profile.unlinkFB();
        });
    }

    $('#saveSettingsBtn').click(function (e) {
        profile.checkUserSettingsForm();
    });

    $('#uploadImgBtn').click(function (e) {
        profile.checkImgUploaderForm();
    });

    // $('.well').affix();

    CKEDITOR.replace('bio');

    if($(".image_upload_profile").exists()) {
        $('#fileUploadContainer .dragButton').html('');

        $('#fileUploadContainer')
            .unbind('dragenter')
            .unbind('dragover')
            .unbind('drop')
            .bind('dragenter', profile.dragOver)
            .bind('dragover', profile.ignoreDrag)
            .bind('drop', profile.dropImage);
        $('#fileUploadContainer .dragButton').on('click', function() {
            $('#photoFile').click();
        });
        $("#photoFile").change(function () {
            profile.handFileUploadImage($(this)[0].files[0]);
        });
    }
};


///profile image upload code

profile.dropImage = function (e)
{
    profile.ignoreDrag(e);
    var dt = e.originalEvent.dataTransfer;
    var files = dt.files;
    profile.handFileUploadImage(files, $('#fileUploadContainer'));
};

profile.handFileUploadImage = function (files, obj)
{
    for (var i = 0; i < files.length; i++)
    {
        var fd = new FormData();
        fd.append('file', files[i]);
        fd.append('userid', $('#id').val());
        fd.append('karateToken', global.CSRF_hash);
        fd.append('locationid', $('#locationid').val());
        var progressbar = profile.setProgressBar();
        $(progressbar).find('.fileNameTxt').text(files[i].name);
        profile.sendImageFileToServer(fd, progressbar);
    }
    $('#fileUploadContainer').removeClass('highlight');
};


profile.sendImageFileToServer = function (formData, progressbar)
{
    $.ajax({
        url: '/profile/uploadavatar',
        progress: function (e) {
            //make sure we can compute the length
            if (e.lengthComputable)
            {
                //calculate the percentage loaded
                var pct = (e.loaded / e.total) * 100;

                var pctDisplay = $.number(pct);

                //log percentage loaded
                //console.log(' PERCENT: ' + pct);
                progressbar.find('.progress-bar').attr('aria-valuenow', pct);
                progressbar.find('.progress-bar').css('width', pct + '%');
                progressbar.find('.progress-bar').text(pctDisplay + '%');
                //console.log(per + '%');

                if (pct >= 100)
                {
                    profile.hideProgressbar(progressbar);
                }

            }
            //this usually happens when Content-Length isn't set
            else
            {
                console.warn('Content Length not reported!');
            }
        },
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            if (data.status == 'SUCCESS')
            {
                d = new Date();
                $('#modal-form #fileUploadContainer').css('background', 'none');
                $('.user_profile_image').attr('src', '/user/profileimg/20/'+data.user+'?'+ d.getTime());
                $('.current_profile_image').attr('src', '/user/profileimg/240/'+data.user+'?'+ d.getTime());
            }
        }
    });
};

//profile image upload end

profile.locationedit = function (lat, lng)
{
    if ($('#previewMap').exists())
    {
        if (lat == 0)
            lat = undefined;
        if (lng == 0)
            lng = undefined;

        gm.initialize('previewMap', lat, lng, 12, false);

        if ($('#marker').exists())
        {
            gm.loadSavedMarkers(false, false, false, '#savedMarkers');
        }
    }

    $('#cancelBtn').click(function (e) {
        if (confirm("Are you sure you wish to cancel?"))
        {
            $(this).find('i').removeClass('fa-times-circle');
            $(this).find('i').addClass('fa-spin');
            $(this).find('i').addClass('fa-spinner');
            $(this).attr('disabled', 'disabled');
            window.location = '/profile';
        }
    });

    $('#closeVideoModalBtn').click(function ($e) {
        $('#videoPreviewModal').modal('hide');
    });

    $('#imgSortable').sortable({
        stop: function (event, ui) {
            var sorted = $(this).sortable('serialize');

            profile.saveImgOrder(sorted);
        }

    });

    $('#deleteLocBtn').click(function (e) {
        profile.deleteLocation();
    });

    //CKEDITOR.replace('description');

    $('.close-reveal-modal, .sign_cancel').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.reveal-modal').fadeOut('slow');
    });
    $('#saveMenuItem').on('click', function (e) {
        e.preventDefault();
        $.post('/profile/addMenuItem', $('#modal-form').serialize(), function (data) {
            $('#myModal').fadeOut('slow');
            $('a.active').click();
        });
    });
    $('#saveImage').on('click', function (e) {
        e.preventDefault();
        var postVars = {
            'locationid': $('#locationid').val(),
            'photoFileName': $('#photoFileName').val(),
            'caption': $('#caption').val(),
            'imageid': $('#imageid').val(),
            'isCover': ($('#isCover').prop('checked')) ? 1 : 0,
            'isProfileImage': ($('#isProfileImage').prop('checked')) ? 1 : 0,
            'karateToken': global.CSRF_hash
        };
        $('#imageModal').fadeOut('slow');
        $.post('/locations/addPhoto', postVars, function (data) {
            $('a.active').click();
        });
        navMove();
    });
    $('#saveVideo').on('click', function (e) {
        e.preventDefault();
        var postVars = {
            'location': $('#locationid').val(),
            'url': $('#modal-video-form #url').val(),
            'title': $('#modal-video-form #title').val(),
            'videoID': $('#modal-video-form #videoid').val(),
            'order': 0,
            'description': $('#modal-video-form #description').val(),
            'thumbnail': '',
            'karateToken': global.CSRF_hash
        };
        $.post('/locations/addVideo', postVars, function (data) {
            $('#videoModal').fadeOut('slow');
            $('a.active').click();
        });
    });
    $('#fileUploadContainer')
        .unbind('dragenter')
        .unbind('dragover')
        .unbind('drop')
        .bind('dragenter', profile.dragOver)
        .bind('dragover', profile.ignoreDrag)
        .bind('drop', profile.drop);
    $('#fileUploadContainer .dragButton').on('click', function() {
        $('#photoFile').click();
    });
    $("#photoFile").change(function () {
        profile.basicFileUpload($(this)[0].files[0]);
    });
    $('a.active').click();
    if (location.hash !== '')
    {
        navMove();
    }
    else
    {
        location.hash = 'dealSettings';
    }
};

window.onhashchange = navMove;

function navMove()
{
    $('.left_list ul li a').removeClass('active');
    $(location.hash).addClass('active');
    global.ajaxLoader('#ajaxSwap');
    var url = $(location.hash).attr('rel');
    $.get(url, {}, function (data) {
        $('#ajaxSwap').fadeOut('slow', function () {
            $(this).html(data);
            $(this).fadeIn('slow');
            $('a.big-link').unbind('click');
            $('a.big-link').on('click', function (e) {
                e.preventDefault();
                var outer = $(this).parent().parent();
                $(outer).find('td').each(function (index, value) {
                    switch (index)
                    {
                        case 0:
                            $('#myModal input[name="description"]').val($(value).text());
                            break;
                        case 1:
                            $('#myModal input[name="per_g"]').val($(value).text());
                            break;
                        case 2:
                            $('#myModal input[name="per_eighth"]').val($(value).text());
                            break;
                        case 3:
                            $('#myModal input[name="per_quarter"]').val($(value).text());
                            break;
                        case 4:
                            $('#myModal input[name="per_half"]').val($(value).text());
                            break;
                        case 5:
                            $('#myModal input[name="per_oz"]').val($(value).text());
                            break;
                        case 6:
                            $('#myModal input[name="per_each"]').val($(value).text());
                            break;
                    }
                });
                $('#myModal input[name="menuid"]').val($(outer).find('input[name="menuid"]').val());
                $('#myModal select').val($(outer).find('input[name="item_type"]').val());
                $('#' + $(this).attr('data-reveal-id')).fadeIn('slow');
            });
            $('#add_new_deal a, #add_new_deal').unbind('click').on('click', function (e) {
                e.preventDefault();
                $('#modal-deals-form input [type="text"]').val('');
                $('#modal-deals-form select').val('weekly');
                $('#imageDealDiv img').remove();
                $('#dealsModal').fadeIn('slow');
            });
            $('#saveDeal').unbind('click').on('click', function () {
                var locationid = $('#modal-deals-form input[name="location_id"]').val();
                var dealid = $('#modal-deals-form input[name="dealid"]').val();
                var target = (dealid > 0) ? '/deals/edit/' + dealid + '/' + locationid : '/deals/add/' + locationid;
                $('#modal-deals-form').attr('action', target).submit();
            });
            $('#dealsDiv .big-link').unbind('click').on('click', function(e) {
                e.preventDefault();
                var modal = $('#' + $(this).attr('data-reveal-id'));
                var current = $(this).parent().parent();
                $('#modal-deals-form input[name="dealid"]').val($(current).find('input[name="dealid"]').val());
                $('#modal-deals-form textarea').val($(current).find('input[name="deal_description"]').val());
                $('#modal-deals-form input[name="deal_name"]').val($(current).find('.deal_name').text().trim());
                $('#modal-deals-form input[name="expiration_date"]').val($(current).find('.expiration_date').text());
                $('#modal-deals-form input[name="retail_price"]').val($(current).find('input[name="retail_price"]').val());
                $('#modal-deals-form input[name="discount_price"]').val($(current).find('input[name="discount_price"]').val());
                $('#modal-deals-form input[name="start_date"]').val($(current).find('input[name="start_date"]').val());
                $('#modal-deals-form input[name="end_date"]').val($(current).find('input[name="end_date"]').val());
                $('#modal-deals-form select').val($(current).find('input[name="repeat"]').val());

                $(modal).fadeIn('slow');
                $('#imageDealDiv img').remove();
                if( $(current).find('input[name="deal_image"]').val().length != 0) {
                    $('#imageDealDiv').append('<img src="/public/uploads/deals/' + $(current).find('input[name="userid"]').val() + '/' + $(current).find('input[name="deal_image"]').val() + '" width="200" />');
                }
                $(modal).fadeIn('slow'); 
            });
            $('#add_new_menu_item a, #add_new_menu_item').unbind('click').on('click', function (e) {
                e.preventDefault();
                $('#myModal input[type="text"]').val('');
                $('#myModal input[name="menuid"]').val('0');
                $('#myModal select').val('0');
                $('#myModal').fadeIn('slow');
            });
            $('#add_new_image a, #add_new_image').unbind('click').on('click', function (e) {
                e.preventDefault();
                $('#modal-form #fileUploadContainer').css("background", "url('/public/images/photo-drag.png') no-repeat center transparent");
                $('#fileUploadContainer .dragButton').html('');
                $('#modal-form #caption').val('');
                $('#modal-form #imageid').val('0');
                $('#modal-form #isCover').prop('checked', false);
                $('#imageModal').fadeIn('slow');
            });
            $('#add_new_video a, #add_new_video').unbind('click').on('click', function (e) {
                e.preventDefault();
                $('#modal-video-form #caption').val('');
                $('#modal-video-form #videoid').val('0');
                $('#videoModal').fadeIn('slow');
            });
            $('.delete-item').on('click', function (e) {
                e.preventDefault();
                var outer = $(this).parent().parent();
                var dealid = $(outer).find('input[name="dealid"]').val();
                $.get('/profile/deleteDeal/' + dealid, {}, function (data) {
                    $(outer).fadeOut('slow');
                });
            });
            $('.delete-deal').on('click', function (e) {
                e.preventDefault();
                var outer = $(this).parent().parent();
                $(outer).fadeOut('slow');
                var menuid = $(outer).find('input[name="dealid"]').val();
                $.get('/profile/deleteDeal/' + menuid, {}, function (data) {

                });
            });
            if ($('#saveBtn').exists())
            {
                $('#saveBtn').unbind('click');
                $('#saveBtn').click(function (e) {
                    profile.checkListingForm();
                });
            }
            $('.location-single-image i.profile-delete').unbind('click').on('click', function() {
                var current = $(this).parent();
                var parts = $(current).find('img').attr('src').split('/');
                var id = parts[5];
                $.post('/locations/deleteImg/' + id, { 'karateToken': global.CSRF_hash }, function(data) {
                    $(current).slideUp('slow');
                });
            });
            $('.location-single-image i.profile-edit').unbind('click').on('click', function() {
                var current = $(this).parent();
                $('#modal-form #fileUploadContainer').css('background', 'none');
                var parts = $(current).find('img').attr('src').split('/');
                $('#fileUploadContainer .dragButton').html('<img src="/locations/img/' + $("#locationid").val() + '/256/' + parts[5] + '" width="256" />');
                $('#modal-form #caption').val($(current).find('h2').text());
                $('#modal-form #imageid').val(parts[5]);
                $('#modal-form #isCover').prop('checked', ($(current).find('input[name="isCover"]').val() > 0));
                $('#imageModal').fadeIn('slow');
            });
            $('.videoRow .icons i.profile-delete').unbind('click').on('click', function() {
                var current = $(this).parent().parent();
                var id = $(current).find('input[name="videoid"]').val();
                $.post('/locations/deleteVideo/' + id, { 'karateToken': global.CSRF_hash }, function(data) {
                    $(current).slideUp('slow');
                });
            });
            $('.videoRow .icons i.profile-edit').unbind('click').on('click', function() {
                var current = $(this).parent().parent();
                var id = $(current).find('input[name="videoid"]').val();
                $('#modal-video-form #videoid').val(id);
                $('#modal-video-form #title').val($(current).find('.videoTitle').text());
                $('#modal-video-form #description').val($(current).find('.videoDescription').text());
                $('#url').parent().parent().css('display', 'none');
                $('#videoModal').fadeIn('slow');
            });
            $('#saveHours').unbind('click').on('click', function() {
                var button = $(this);
                $(button).prop('disabled', true);
                $.post('/locations/saveHours', $('#saveHoursForm').serialize(), function(data) {
                    global.renderAlert("Hours have been saved!", 'alert-success', 'hoursDiv', true);
                    $(button).prop('disabled', false);
                });
            });
            $('#hoursDiv .form-control').timepicki();

            // Validate URLs
            $(".validateURL").each(function() {
                $(this).blur(function () {
                    var nameRegex = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
                    var url = $(this).val();

                    if (!(nameRegex.test(url)) || (url == '')) {
                        $(this).parent().find(".red").hide();
                        $(this).parent().append("<p class='red'>Please enter a valid URL.</p>");

                    } else if (url == '') {
                        $(this).addClass("red");
                    } else {
                        $(this).parent().find(".red").hide();
                        return false;
                    }
                });
            });

            $('.drop_spot')
                .unbind('dragenter')
                .unbind('dragover')
                .unbind('drop');
            $('.drop_spot')
                .bind('dragenter', profile.dragOverSettings)
                .bind('dragover', profile.ignoreDrag)
                .bind('drop', profile.dropSettings);
            $('.drop_spot').unbind('click').on('click', function() {
                $('#profileImageUpload').click();
            });
        });
    });
};

profile.deleteLocation = function ()
{
    if (confirm("Are you sure you wish to delete this location?"))
    {
        $.post('/profile/deletelocation', {location: $('#id').val(), karateToken: global.CSRF_hash}, function (data) {
            if (data.status == 'SUCCESS')
            {
                window.location = '/profile?site-success=' + escape(data.msg);
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
            }
        }, 'json');
    }
}

profile.checkImgUploaderForm = function ()
{
    if ($('input[type="file"]').val() == '')
    {
        global.renderAlert("Please select a picture to upload!");
        return false;
    }

    $('#uploadImgBtn i').removeClass('fa-cloud-upload');
    $('#uploadImgBtn i').addClass('fa-spinner');
    $('#uploadImgBtn i').removeClass('fa-spin');

    $('#avatarUploadForm').submit();


}

profile.saveImgOrder = function (sorted)
{
    $.post('/profile/saveimgorder', sorted + '&' + $.param({'location': $('#id').val(), 'karateToken': global.CSRF_hash}), function (data) {
        if (data.status == 'SUCCESS')
        {
            //global.renderAlert(data.msg, 'alert-danger');
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
        }
    }, 'json');
}

profile.addVideo = function (b)
{
    var html = $(b).parent().parent().parent().html();
    $('.videoContainer').prepend("<div class='vc'>" + html + "</div>");

    profile.updateVideoIcons();

};

profile.updateVideoIcons = function ()
{
    $('.videoContainer button i').removeClass('fa-plus');
    $('.videoContainer button').removeClass('btn-info').addClass('btn-danger');
    $('.videoContainer button').attr('onclick', "profile.delVideo(this);");
    $('.videoContainer button i').addClass('fa-trash-o');
    $('.videoContainer button i').first().addClass('fa-plus');
    $('.videoContainer button').first().removeClass('btn-danger').addClass('btn-info');
    $('.videoContainer button').first().attr('onclick', 'profile.addVideo(this)');
    $('.videoContainer input').first().attr('placeholder', '');
};

profile.delVideo = function (b)
{
    $(b).parent().parent().parent().html('');
}

profile.addImg = function (b)
{
    var html = $(b).parent().parent().parent().html();
    $('#imageUploadContainer').prepend("<div class='imgContainer'>" + html + "</div>");
    profile.updateImgBtn();
};


profile.updateImgBtn = function ()
{
    $('#imageUploadContainer button i').removeClass('fa-plus');
    $('#imageUploadContainer button').removeClass('btn-info').addClass('btn-danger');
    $('#imageUploadContainer button').attr('onclick', "profile.delImg(this);");
    $('#imageUploadContainer button i').addClass('fa-trash-o');
    $('#imageUploadContainer button i').last().addClass('fa-plus');
    $('#imageUploadContainer button').last().removeClass('btn-danger').addClass('btn-info');
    $('#imageUploadContainer button').last().attr('onclick', 'profile.addImg(this)');
};


profile.delImg = function (b)
{
    $(b).parent().parent().parent().html('');
};

profile.addItem = function (b)
{
    var html = $(b).parent().parent().html();
    var id = parseInt($(b).parent().parent().find(".input-group.col-md-6 :first").attr('name').split('[')[1].slice(3, -1));
    $(b).parent().parent().parent().prepend('<div>' + html.replace(/menu\[new\d{1,10}/g, 'menu[new' + (parseInt(id) + 1)) + '</div>');
    $(b).replaceWith("<button type='button' class='btn btn-danger pull-right' onclick=\"profile.delItem(this);\"><i class='fa fa-trash-o'></i></button>");
};

profile.delItem = function (b)
{
    var parent = $(b).parent().parent();
    var html = $(parent).html();
    var isNew = (html.search(/menu\[new\d{1,10}/g) >= 0) ? true : false;
    if (isNew)
    {
        $(parent).remove();
    }
    else
    {
        $(parent).find("input[name*='active']").val(0);
        $(parent).slideUp('slow');
    }
};

profile.checkListingForm = function ()
{
    var locationid = $('#locationForm input [name="id"]').val();
    if ($('#name').val() == '')
    {
        global.renderAlert('Please enter the business name!');
        $('#name').focus();
        $('#name').effect('highlight');
        return false;
    }

    if ($('#address').val() == '')
    {
        global.renderAlert('Please enter the address!');
        $('#address').focus();
        $('#address').effect('highlight');
        return false;
    }

    if ($('#city').val() == '')
    {
        global.renderAlert('Please enter the city!');
        $('#city').focus();
        $('#city').effect('highlight');
        return false;
    }

    if ($('#state :selected').val() == '')
    {
        global.renderAlert('Please select a state!');
        $('#state').focus();
        $('#state').effect('highlight');
        return false;
    }

    if ($('#postalCode').val() == '')
    {
        global.renderAlert('Please enter the postal code!');
        $('#postalCode').focus();
        $('#postalCode').effect('highlight');
        return false;
    }

    $('#saveBtn').find('i').removeClass('fa-save');
    $('#saveBtn').find('i').addClass('fa-spin');
    $('#saveBtn').find('i').addClass('fa-spinner');
    $('#saveBtn').attr('disabled', 'disabled');
    $.post('/profile/savelisting', $('#locationForm').serialize(), function (data) {
        if (locationid > 0)
        {
            window.location = '/profile/vendoredit/' + locationid + '#profileSettings';
        }
        else
        {
            navMove();
        }
    }, 'json');
};


profile.unlinkFB = function ()
{
    if (confirm("Are you sure you wish to unlink your Facebook account?"))
    {
        $('#unlinkFBbtn').attr('disabled', 'disabled');

        $.getJSON("/profile/disconnectfb", function (data) {
            if (data.msg == 'SUCCESS')
            {
                window.location = '/profile?site-success=' + data.msg;
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger');
                $('#unlinkFBbtn').removeAttr('disabled');
            }
        });
    }
}

profile.editlisting = function (id)
{
    window.location = '/profile/vendoredit/' + id;
}

profile.viewListing = function (id)
{
    window.location = '/search/info/' + id;
}

profile.previewVideo = function (location, id, title, videoID)
{
    // unbinds delete button
    $('#deleteVideoBtn').unbind('click');

    // assigns new onclick
    $('#deleteVideoBtn').click(function ($e) {
        if (confirm("Are you sure you wish to delete this video?"))
        {
            $(this).prepend(global.spinner);
            $(this).attr('disabled', 'disabled');
            // $('#videoPreviewModal').modal('hide');
            profile.deleteVideo(location, id);

            $('#closeVideoModalBtn').attr('disabled', 'disabled');
        }
    });

    // changes title
    $('#videoPreviewModal h3.video-title').text(title);

    $('#videoPreviewModal iframe').attr('src', "https://www.youtube.com/v/" + videoID + "?version=3&f=videos&app=youtube_gdata");

    $('#videoPreviewModal').modal('show');
}

profile.deleteVideo = function (location, id)
{
    $.post('/profile/deletevideo', {location: location, id: id, karateToken: global.CSRF_hash}, function (data) {
        if (data.status == 'SUCCESS')
        {
            $('#videoPreviewModal').modal('hide');
            global.renderAlert(data.msg, 'alert-success');
            $('#videoThumb_' + id).empty();

            $('#deleteVideoBtn').html("<i class='fa fa-trash-o'></i>");
            $('#deleteVideoBtn').removeAttr('disabled');
            $('#closeVideoModalBtn').removeAttr('disabled');
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'videoAlert');
        }
    }, 'json');
}

profile.deleteImage = function (b, location, id)
{

    if (confirm("Are you sure wish to delete this image?"))
    {
        $.post('/profile/deleteimg', {location: location, id: id, karateToken: global.CSRF_hash}, function (data) {
            if (data.status == 'SUCCESS')
            {
                global.renderAlert(data.msg, 'alert-success');

                $(b).parent().empty();
            }
            else
            {
                global.renderAlert(data.msg, 'alert-danger', 'videoAlert');
            }
        }, 'json');
    }
}

profile.checkUserSettingsForm = function ()
{
    if ($('#firstName').val() == '')
    {
        $('#firstName').effect('highlight');
        $('#firstName').focus();
        global.renderAlert("Please enter your first name!");
        return false;
    }

    if ($('#lastName').val() == '')
    {
        $('#lastName').effect('highlight');
        $('#lastName').focus();
        global.renderAlert("Please enter your last name!");
        return false;
    }

    if ($('#email').val() == '')
    {
        $('#email').effect('highlight');
        $('#email').focus();
        global.renderAlert("Please enter your e-mail address!");
        return false;
    }

    if ($('#timezone :selected').val() == '')
    {
        $('#timezone').effect('highlight');
        $('#timezone').focus();
        global.renderAlert("Please select which timezone you are located in!");
        return false;
    }

    // updates textarea with latest content from CKeditor before saving
    var str = CKEDITOR.instances['bio'].getData();
    $('#bio').val(str);

    $.post('/profile/saveusersettings', $('#userForm').serialize(), function (data) {
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg);
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');
        }
    }, 'json');
};

profile.editEvents = function (location)
{
    window.location = "/events/index/" + location;
};

profile.setProgressBar = function ()
{
    var phtml = $('#fileUploadProgressHtml').html();
    $('#uploadProgressContainer').append(phtml).effect('highlight');
    return $('#uploadProgressContainer .fileProgressBar').last();
};

profile.hideProgressbar = function (progressbar)
{
    window.setTimeout(function () {
        progressbar.hide('highlight');
    }, 2000);
};

profile.ignoreDrag = function (e)
{
    e.originalEvent.stopPropagation();
    e.originalEvent.preventDefault();
};

profile.dragOver = function (e)
{
    profile.ignoreDrag(e);
    $('#fileUploadContainer').addClass('highlight');
};

profile.dragOut = function (e)
{
    profile.ignoreDrag(e);
};

profile.drop = function (e)
{
    profile.ignoreDrag(e);
    var dt = e.originalEvent.dataTransfer;
    var files = dt.files;
    profile.handFileUpload(files, $('#fileUploadContainer'));
};

profile.handFileUpload = function (files, obj)
{
    for (var i = 0; i < files.length; i++)
    {
        var fd = new FormData();
        fd.append('file', files[i]);
        fd.append('userid', $('#id').val());
        fd.append('karateToken', global.CSRF_hash);
        fd.append('locationid', $('#locationid').val());
        var progressbar = profile.setProgressBar();
        $(progressbar).find('.fileNameTxt').text(files[i].name);
        profile.sendFileToServer(fd, progressbar);
    }
    $('#postBtn').removeAttr('disabled');
    $('#fileUploadContainer').removeClass('highlight');
};

profile.basicFileUpload = function (file)
{
    var fd = new FormData();
    fd.append('file', file);
    fd.append('userid', $('#id').val());
    fd.append('karateToken', global.CSRF_hash);
    fd.append('locationid', $('#locationid').val());
    var progressbar = profile.setProgressBar();

    $(progressbar).find('.fileNameTxt').text(file.name);

    profile.sendFileToServer(fd, progressbar);

    //profile.uploadedImgs.push(file.name);

    $('#postBtn').removeAttr('disabled');
    $('#fileUploadContainer').removeClass('highlight');
};

profile.sendFileToServer = function (formData, progressbar)
{
    $.ajax({
        url: '/profile/uploadLocationImgs',
        progress: function (e) {
            //make sure we can compute the length
            if (e.lengthComputable)
            {
                //calculate the percentage loaded
                var pct = (e.loaded / e.total) * 100;

                var pctDisplay = $.number(pct);

                //log percentage loaded
                //console.log(' PERCENT: ' + pct);
                progressbar.find('.progress-bar').attr('aria-valuenow', pct);
                progressbar.find('.progress-bar').css('width', pct + '%');
                progressbar.find('.progress-bar').text(pctDisplay + '%');
                //console.log(per + '%');

                if (pct >= 100)
                {
                    profile.hideProgressbar(progressbar);
                }

            }
            //this usually happens when Content-Length isn't set
            else
            {
                console.warn('Content Length not reported!');
            }
        },
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            if (data.status == 'SUCCESS')
            {
                $('#modal-form #fileUploadContainer').css('background', 'none');
                $('#fileUploadContainer .dragButton').html('<img src="/public/uploads/locationImages/' + $("#locationid").val() + '/' + data.msg + '" width="256" />');
                $('#imageid').val(data.imageid);
                $('#photoFileName').val(data.msg);
            }
        }
    });
};
profile.setProgressBarSettings = function ()
{
    var phtml = $('#fileUploadProgressHtml').html();
    $('.outer_image').prepend(phtml).effect('highlight');
    return $('.outer_image .fileProgressBar').first();
};

profile.dragOverSettings = function (e)
{
    profile.ignoreDrag(e);
    $('.outer_image').addClass('highlight');
};

profile.dropSettings = function (e)
{
    profile.ignoreDrag(e);
    var dt = e.originalEvent.dataTransfer;
    var files = dt.files;
    profile.handFileUploadSettings(files, $('.outer_image'));
};

profile.handFileUploadSettings = function (files, obj)
{
    for (var i = 0; i < files.length; i++)
    {
        var fd = new FormData();
        fd.append('file', files[i]);
        fd.append('karateToken', global.CSRF_hash);
        fd.append('locationid', $('#locationid').val());
        var progressbar = profile.setProgressBarSettings();
        $(progressbar).find('.fileNameTxt').text(files[i].name);
        profile.sendFileToServerSettings(fd, progressbar);
    }
    $('.drop_spot').removeClass('highlight');
};

profile.basicFileUploadSettings = function (file)
{
    var fd = new FormData();
    fd.append('file', file);
    fd.append('karateToken', global.CSRF_hash);
    fd.append('locationid', $('#locationid').val());
    var progressbar = profile.setProgressBarSettings();
    $(progressbar).find('.fileNameTxt').text(file.name);
    profile.sendFileToServerSettings(fd, progressbar);
    $('.drop_spot').removeClass('highlight');
};

profile.sendFileToServerSettings = function (formData, progressbar)
{
    $.ajax({
        url: '/profile/uploadLocationImgs',
        progress: function (e) {
            //make sure we can compute the length
            if (e.lengthComputable)
            {
                //calculate the percentage loaded
                var pct = (e.loaded / e.total) * 100;

                var pctDisplay = $.number(pct);

                //log percentage loaded
                //console.log(' PERCENT: ' + pct);
                progressbar.find('.progress-bar').attr('aria-valuenow', pct);
                progressbar.find('.progress-bar').css('width', pct + '%');
                progressbar.find('.progress-bar').text(pctDisplay + '%');
                //console.log(per + '%');

                if (pct >= 100)
                {
                    profile.hideProgressbar(progressbar);
                }

            }
            //this usually happens when Content-Length isn't set
            else
            {
                console.warn('Content Length not reported!');
            }
        },
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            if (data.status == 'SUCCESS')
            {
                $('.drag_file').html('<img src="/public/uploads/locationImages/' + $("#locationid").val() + '/' + data.msg + '" width="240" height="240" />');
                $('#imageName').val(data.msg);
            }
        }
    });
};

function claimAccept(id, user) {

    var token = $('#claimAccept-'+id+'-'+user).data('token');

    $.post("/profile/claimAccept", {user: user, location: id, karateToken: token}, function (data) {
        if (data.status == 'SUCCESS')
        {
            $('#claimAccept-'+id+'-'+user).attr("onclick", "claimRemove("+id+", "+user+")");
            $('#claimAccept-'+id+'-'+user).html('Remove');
            $('#claimAccept-'+id+'-'+user).attr('id', "claimRemove-"+id+"-"+user);
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'claimAlert');
        }
    }, 'json');
}

function claimDelete(id, user) {

    var token = $('#claimDelete-'+id+'-'+user).data('token');

    $.post("/profile/claimDelete", {user: user, location: id, karateToken: token}, function (data) {
        if (data.status == 'SUCCESS')
        {
            $('#claim-'+id+'-'+user).slideUp('fast');
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'claimAlert');
        }
    }, 'json');
}

function claimRemove(id, user) {

    var token = $('#claimRemove-'+id+'-'+user).data('token');

    $.post("/profile/claimRemove", {user: user, location: id, karateToken: token}, function (data) {
        if (data.status == 'SUCCESS')
        {
            $('#claimRemove-'+id+'-'+user).attr("onclick", "claimAccept("+id+", "+user+")");
            $('#claimRemove-'+id+'-'+user).html('Accept');
            $('#claimRemove-'+id+'-'+user).attr('id', "claimAccept-"+id+"-"+user);
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'claimAlert');
        }
    }, 'json');
}

profile.getCoRank = function() {

    var companyQueryVal = $("#companyQuery").val();

    $.post("/profile/getCompanyInfo", {id: companyQueryVal, karateToken: global.CSRF_hash}, function (data){
        if(data.status == 'SUCCESS')
        {
            $('.company_name_fill').text(data.msg.name);
            $('.company_address_fill').text(data.msg.formattedAddress);

            var rankSelect = [];
            for(var i = 0; i < 10; i++){
                if(data.msg.rank == i){
                    rankSelect[i] = 'selected';
                } else {
                    rankSelect[i] = '';
                }
            }

            var htmlList = '<select class="form-control" onchange="profile.upDateRank('+ companyQueryVal +', $(this).val())"><option ' + rankSelect['0'] + '>0</option><option  ' + rankSelect['1'] + '>1</option><option  ' + rankSelect['2'] + '>2</option><option  ' + rankSelect['3'] + '>3</option><option  ' + rankSelect['4'] + '>4</option><option  ' + rankSelect['5'] + '>5</option><option  ' + rankSelect['6'] + '>6</option><option  ' + rankSelect['7'] + '>7</option><option  ' + rankSelect['8'] + '>8</option><option  ' + rankSelect['9'] + '>9</option></select>';

            $('.company_rank_fill').html(htmlList);

        }
    }, 'json');

}

profile.upDateRank = function (id, value) {
    $.post('/profile/updateCompanyRank', {id: id, rank: value, karateToken: global.CSRF_hash}, function (data){
        if(data.status == 'SUCCESS')
        {
            $('#rankStatus').html('<div class="alert alert-success text-center">Rank Updated Successfully</div></b>');
        } else {
            $('#rankStatus').html('<div class="alert alert-warning text-center">There was an error with your request</div></b>');
        }
    }, 'json');

}


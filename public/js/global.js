var global = {}

global.CSRF_token = 'token';
global.CSRF_hash = '';

global.bmsUrl;

global.spinner = "<i class='fa fa-spinner fa-spin'></i> ";


global.alertTimeoutSeconds = 3000; // milliseconds until alerts clear
global.alertTimeout;

global.userid = 0;
global.logged_in = false;


global.footerHeight = 50; // number of pixels high footer should be
global.mainContainerHeightType = 'min-height';

global.debug = true;

// global onload
$(function () {

    // if hidden input exists for CSRF token, gets value
    if ($('#' + global.CSRF_token).exists())
    {
        global.CSRF_hash = $('#' + global.CSRF_token).val();
    }

    if ($('#bmsUrl').exists())
    {
        global.bmsUrl = $('#bmsUrl').val();
    }

    $('#headerLoginBtn, #loginXSBtn, #loginBigButton').click(function (e) {
        $('#loginModal').modal('show');
        $('#user_login').focus();
    });

    $('#headerSignUpBtn').on('click', function() {
        $('#regModal').modal('show');
    });

    $('#submitRegBtn').click(function (e) {
        global.checkRegisterForm();
    });

    global.setRegEnter();

    $('#modalFBbtn, #bigFBbtn').click(function (e) {
        fb.login($(this), true);
    });

    $('#submitLoginBtn').click(function (e) {
        global.userlogin();
    });

    $('#signupLink').click(function (e) {
        global.loadSignup();
    });

    if ($('#locatorLink').exists())
    {
        if ($('#lat').val() == 0 && $('#lng').val() == 0)
        {
            //global.getLocation();
        }
    }

    if ($('#loginModal').exists())
    {
        $('#loginModal').on('shown.bs.modal', function (e) {
            global.setLoginEnter();
        });

        $('#loginModal').on('hide.bs.modal', function (e) {
            $(window).unbind('keypress');
        });
    }

    global.smSearch();

    $('#logoutXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/welcome/logout';
    });

    $('#accountXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/profile';
    });

    $('#profileXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/user/index/' + global.userid;
    });

    //mobile nav
    $('#dispXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/locations/search/dispensaries';
    });
    $('#doctorsXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/locations/search/doctors';
    });
    $('#dealsXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/deals';
    });
    $('#reviewsXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/reviews';
    });
    $('#messagesXSBtn').click(function (e) {
        $(this).attr('disabled', 'disabled');
        $(this).find('i').removeClass('fa-sign-out');
        $(this).find('i').addClass('fa-spinner');
        $(this).find('i').removeClass('fa-spin');

        window.location = '/messages';
    });
    $('#main-search #q, #main-search #location, #xs-search #q, #xs-search #location').focus(function () {
        $(this).select();

    }).mouseup(function (e) {
        e.preventDefault();
    });

    //console.log(window.location.pathname);


    //global.adjustLayout();

    $(window).resize(function () {
        //global.adjustLayout();
    });

    global.setClearAlertTimeout();
    
});

$.fn.preload = function () {
    this.each(function () {
        $('<img/>')[0].src = this;
    });
}


/**
 * functions dynamically adjust elements on page
 */
global.smSearch = function ()
{
    $('.main-content').hover(function (e) {
        $('#smAccountInfo').hide();
        $('#navUserIcon').removeClass('navHovered');

        $('#smSearch').hide();
        $('#navSearchIcon').removeClass('navHovered');

        if ($('.indexElements').exists())
        {
            $('.indexElements').removeAttr('style');
        }

    });


    $('#navSearchIcon').click(function (e) {
        $('#smSearch').show();


        if ($('.indexElements').exists())
        {
            $('.indexElements').css('margin-top', '130px');
        }

        $('#smAccountInfo').hide();

        $('#navSearchIcon').addClass('navHovered');
        $('#navUserIcon').removeClass('navHovered');

        $("html, body").animate({scrollTop: 0}, "slow");
    });

    $('#navSearchIcon').mouseover(function () {
        $('#smSearch').show();

        if ($('.indexElements').exists())
        {
            $('.indexElements').css('margin-top', '130px');
        }

        $('#smAccountInfo').hide();

        $('#navSearchIcon').addClass('navHovered');
        $('#navUserIcon').removeClass('navHovered');

        $("html, body").animate({scrollTop: 0}, "slow");
    });

    $('#navUserIcon').click(function (e) {
        $('#smAccountInfo').show();

        if ($('.indexElements').exists())
        {
            $('.indexElements').css('margin-top', '100px');
        }

        $('#smSearch').hide();

        $('#navSearchIcon').removeClass('navHovered');
        $('#navUserIcon').addClass('navHovered');

        $("html, body").animate({scrollTop: 0}, "slow");
    });

    $('#navUserIcon').mouseover(function () {
        $('#smAccountInfo').show();

        if ($('.indexElements').exists())
        {
            $('.indexElements').css('margin-top', '100px');
        }

        $('#smSearch').hide();

        $('#navSearchIcon').removeClass('navHovered');
        $('#navUserIcon').addClass('navHovered');

        $("html, body").animate({scrollTop: 0}, "slow");
    });

}

global.getLocation = function (autoSearch)
{
    if (autoSearch == undefined)
    {
        autoSearch = false;
    }

    /*
     * For mobile users, we will use browser location
     * Desktop users, we will use GeoIP
     */

    $.get('/search/checkGeo', {}, function(data) {
        if (!data.status)
        {
            if (navigator.geolocation)
            {
                // disable location btn and make it spin
                //$('#locatorLink').attr('disabled', 'disabled');
                $('#locatorLink i, #xs-search #xsLocatorBtn i').addClass('fa-spin');
                $('#locatorLink, #xs-search #xsLocatorBtn').addClass('locatorNoCursor');

                navigator.geolocation.getCurrentPosition(function (loc) {
                    global.setLocation(loc, autoSearch);
                }, global.unableToFindLoc,
                {
                    timeout: 3000
                });
            }
        }
    }, 'json');
};

global.detectmob = function() {
    if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
        )
    {
        return true;
    }
    else
    {
        return false;
    }
}

global.setLocation = function (loc, autoSearch)
{
    if (autoSearch == undefined)
    {
        autoSearch = false;
    }
    $('#lat').val(loc.coords.latitude);
    $('#lng').val(loc.coords.longitude);

    $.get('/search/saveGeoCode', { 'lat': loc.coords.latitude, 'lng': loc.coords.longitude }, function (data) {
        //window.location = '/' + data;
    });
    // re-enable location btn stop spinning
    $('#locatorLink i, #xs-search #xsLocatorBtn i').removeClass('fa-spin');
    $('#locatorLink, #xs-search #xsLocatorBtn').removeClass('locatorNoCursor');
    //$('#locatorLink').removeAttr('disabled');
};

global.unableToFindLoc = function ()
{
    $.get('/search/getGeoIP/true', {}, function(data) {
        if (data === 'false')
        {
            global.renderAlert('Unable to find location');
            $('#locatorLink i').removeClass('fa-spin');
            $('#locatorLink').removeClass('locatorNoCursor');
        }
        else
        {
            var citystate = (data.city + '-' + data.state).replace(' ', '-').toLowerCase();
            window.location = '/' + citystate;
        }
    }, 'json');
};

/*
 * renders a site wide alert
 *
 * @param String msg - msg to be displayed
 * @param String type (optional) - type of message to be displayed, default is blank, alternate types: 'alert-success', 'alert-error' or 'alert-info'
 * @param String id (optional) - specifcy custom div ID to display the error
 * @param boolean prepend (optional) - true to prepend the alert inside of the div instead if replacing it
 */

global.renderAlert = function (msg, type, id, prepend)
{

    clearTimeout(global.alertTimeout);

    var header = "Alert!";

    if (id == undefined)
    {
        id = "site-alert";

        $("html, body").animate({scrollTop: 0}, "slow");
    }

    if (msg == '' || msg == undefined)
    {
        $("#" + id).html('');
        return;
    }

    if (type == undefined)
    {
        type = 'alert-warning';
    }


    // patch for bootstrap 3
    if (type == 'alert-error')
        type = 'alert-danger';

    //$("#" + id).html("<div class='ui-widget'><div class='ui-state-error ui-corner-all' style=\"padding: 0 .7em;\"><p><span class='ui-icon ui-icon-alert' style=\"float: left; margin-right: .3em;\"></span><strong>Alert:</strong> "+msg+"</p></div></div>");

    if (type == 'alert-danger')
        header = "<i class='fa fa-times-circle-o'></i> Error";
    if (type == 'alert-info')
        header = "<i class='fa fa-exclamation-circle'></i> Information";
    if (type == 'alert-success')
        header = "<i class='fa fa-thumbs-up'></i> Success";

    if (prepend)
    {
        $('#' + id).prepend("<div class='alert " + type + " animated fadeIn'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4>" + header + "</h4> " + msg + "</div>");
    }
    else
    {
        $('#' + id).html("<div class='alert " + type + " animated fadeIn'><button type='button' class='close' data-dismiss='alert'>&times;</button><h4>" + header + "</h4> " + msg + "</div>");
    }

    global.setClearAlertTimeout();

    return true;
};

global.setClearAlertTimeout = function ()
{
    global.alertTimeout = setTimeout(global.clearAlerts, global.alertTimeoutSeconds);
};

global.clearAlerts = function ()
{
    // finds all alert divs and hides them after a specific time
    $('body').find('.alert').each(function(index, item){
        if ($(item).hasClass('alert-info'))
        {
            // does not clear info alerts
        }
        else
        {
            $(item).addClass('animated fadeOut');
            $(item).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
                $(this).removeAttr('class'); // clears all classes
                $(this).empty();
            });
        }
    //console.log($(item).attr('class'));
    });
    //global.setClearAlertTimeout();
};


global.userlogin = function ()
{
    if ($('#user_email').val() == '')
    {
        global.renderAlert('Please enter your E-mail Address!', undefined, 'loginAlert');
        $('#user_email').focus();
        $('#user_email').effect('highlight');
        return false;
    }

    if ($('#user_pass').val() == '')
    {
        global.renderAlert('Please enter your password!', undefined, 'loginAlert');
        $('#user_pass').focus();
        $('#user_pass').effect('highlight');
        return false;
    }

    // $.post("/wordpress/wp-login.php", $('#loginform').serialize(), function(data){
    $.post("/welcome/login", $('#loginform').serialize(), function (data) {
        if (data.status == 'SUCCESS')
        {
            window.location = '/dashboard';
        }
        else
        {
            global.renderAlert(data.msg, undefined, 'loginAlert');
        }
    }, 'json');
};

global.setLoginEnter = function ()
{
    // sets email textfield to focus so they can start typing right away
    $('#loginModal #user_email').focus();

    $(window).bind('keypress', function (e) {
        var code = e.keyCode || e.which;

        if (code == 13)
        {
            //console.log('ENTER PRESSED');
            global.userlogin();
        }
    });
};

global.setForgotPasswordEnter = function ()
{
    $(window).unbind('keypress');

    // sets email textfield to focus so they can start typing right away
    $('#loginModal #user_email').focus();

    $(window).bind('keypress', function (e) {
        var code = e.keyCode || e.which;

        if (code == 13)
        {
            //console.log("FORGOT ENTER!");
            global.checkForgotPasswordForm();
        }
    });
}

global.setRegEnter = function ()
{
    $(window).unbind('keypress');

    $(window).bind('keypress', function (e) {
        var code = e.keyCode || e.which;

        if (code == 13)
        {
            //console.log("FORGOT ENTER!");
            global.checkRegisterForm();
        }
    });
};

global.styleSelect = function (sel)
{
    $('#main-search #q').val($(sel).text());
};

global.loadForgotPassword = function ()
{

    $('#loginModal .modal-title').html("FORGOT PASSWORD");
    $('#passwordFormGroup, #fbLoginFormGroup, #loginModal a').addClass('animated fadeOut');

    $('#submitLoginBtn').text('Submit');

    $('#forgotPWText').show().addClass('animated fadeIn');

    $('#passwordFormGroup, #fbLoginFormGroup, #loginModal a').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e)
    {
        $(this).removeAttr('class'); // clears all classes
        $(this).empty();
    });

    //unbind save click
    $('#submitLoginBtn').unbind("click");

    $('#submitLoginBtn').click(function (e) {
        global.checkForgotPasswordForm();
    });

    global.setForgotPasswordEnter();
};

global.checkForgotPasswordForm = function ()
{
    if ($('#user_email').val() == '')
    {
        global.renderAlert('Please enter your e-mail address!', undefined, 'loginAlert');
        $('#email').focus();
        $('#email').effect('highlight');
        return false;
    }

    $.post('/welcome/forgotpassword', {email: $('#user_email').val(), karateToken: global.CSRF_hash}, function (data) {
        if (data.status == 'SUCCESS')
        {
            $('#loginModal #user_email').val(''); // clears e-mail field

            global.renderAlert(data.msg, 'alert-success', 'loginAlert');
        }
        else
        {
            global.renderAlert(data.msg, 'alert-error', 'loginAlert');
        }
    }, 'json');
};


global.checkRegisterForm = function ()
{
    if ($('#firstName_reg').val() == '')
    {
        global.renderAlert('Please enter your first name!', undefined, 'loginAlert');
        $('#firstName').focus();
        $('#firstName').effect('highlight');
        return false;
    }

    if ($('#lastName_reg').val() == '')
    {
        global.renderAlert('Please enter your last name!', undefined, 'loginAlert');
        $('#lastName').focus();
        $('#lastName').effect('highlight');
        return false;
    }

    if ($('#email_reg').val() == '')
    {
        global.renderAlert('Please enter your email address!', undefined, 'loginAlert');
        $('#email').focus();
        $('#email').effect('highlight');
        return false;
    }

    if ($('#user_pass_reg').val() == '')
    {
        global.renderAlert('Please enter your password!', undefined, 'loginAlert');
        $('#user_pass').focus();
        $('#user_pass').effect('highlight');
        return false;
    }

    $.post("/welcome/register", $('#regform').serialize(), function (data) {
        if (data.status == 'SUCCESS')
        {
            window.location = '/profile?site-success=' + escape(data.msg);
        }
        else if (data.status == 'ALERT')
        {
            global.renderAlert(data.msg, undefined, 'loginAlert');
        }
        else
        {
            global.renderAlert('Please enter your password!', 'alert-danger', 'loginAlert');
        }

    }, 'json');
};

global.ajaxLoader = function (divId, size)
{

    if (size == undefined)
        size = 80;

    var html = "<canvas id='dyn-ajax-loader'></canvas>";

    //global.log($node, true);

    $(divId).html(html);

    $node = $(divId).find('canvas#dyn-ajax-loader');

    $node.ClassyLoader({
        animate: true,
        percentage: 99,
        width: size,
        diameter: 30,
        fontFamily: 'Open Sans',
        fontSize: '16px',
        height: size
    });

    $node.setPercent(99);

    return $node;
};

global.log = function (msg, jsonStringify)
{
    if (jsonStringify == undefined)
        jsonStringify = false;

    if (global.debug)
    {
        var m = (jsonStringify) ? JSON.stringify(msg) : msg;

        console.log(m);

    }

}

var search = {
    'lat': '',
    'lng': '',
    'searchSort': 'distance-asc'
};

search.indexInit = function (lat, lng, fullScreen)
{
    search.lat = ($('#avgLat').val() != '0') ? $('#avgLat').val() : undefined;
    search.lng = ($('#avgLng').val() != '0') ? $('#avgLng').val() : undefined;
    fullScreen = fullScreen || false;
    search.bindWindowScroll();
    search.setWellWidth();

    $('#mapWell .well').affix();

    if ($('#previewMap').exists())
    {
        if (fullScreen)
        {
            $('#previewMap').height($(window).height() - 105);
        }
        $('#mapListings').height($('#previewMap').height());
        gm.initialize('previewMap', search.lat, search.lng, 12, false);
        search.loadListingMarkers();
        gm.loadSavedMarkers(false, false, false, '#savedMarkers');
        //gm.autoCenter();
    }

    $('a#searchlink').on('click', function () {
        var location = $('#location').val().replace(/,/g, '');
        location = location.replace(/\s/g, '-').toLowerCase();
        $(this).attr('href', location);
        return true;
    });

    gm.bindListings();

    $('.sort').on('click', function (e) {
        e.preventDefault();
        var sortObj = $(this).find('i');
        if (sortObj.length == 0)
        {
            $('.sort i').remove();
            $(this).append(' <i class="fa fa-caret-down"></i>');
            $('#listContent .row .listing .listRight').each(function () {

            });
        }
        else
        {
            if ($(sortObj).hasClass('fa-caret-down'))
            {
                $(sortObj).removeClass('fa-caret-down').addClass('fa-caret-up');
            }
            else
            {
                $(sortObj).removeClass('fa-caret-up').addClass('fa-caret-down');
            }
        }
    });

    $('#searchSort li a').on('click', function(e) {
        e.preventDefault();
        $('#searchDropDown').html($(this).text() + ' <span class="caret"></span>');
        search.searchSort = $(this).attr('href');
        search.reloadMap();
    });

    $('#dispensaries').on('click', function() {

        $("#dispensaries").prop('checked', true);

        $("#doctors").removeAttr('checked');
        search.reloadMap();
    });

    $('#doctors').on('click', function() {

        $("#doctors").prop('checked', true);

        $("#dispensaries").removeAttr('checked');
        search.reloadMap();
    });
    
    $('#locationSearchButton').on('click', function() {
        search.reloadMap();
    });



    //patch for search box
    $("#searchTerm").keyup(function(event){
        if(event.keyCode === 13){
            $("#locationSearchButton").click();
        }
    });

    // executes function on resizing of window
    $(window).resize(function () {
        search.setWellWidth();
    });
};

search.reloadMap = function()
{
    $('#searchOverlay').css({
        'top': $("#mapListings").position().top + 'px',
        'width': $("#mapListings").width() + 'px',
        'height': $('#mapListings').height() + 'px'
    }).fadeTo('fast', .7);
    var sort = search.searchSort.split('-');
    var postVars = {
        'searchTerm': $('#searchTerm').val(),
        'dispensaries': $('#dispensaries').prop('checked'),
        'doctors': $('#doctors').prop('checked'),
        'orderby': sort[0],
        'order': sort[1],
        'karateToken': global.CSRF_hash
    };
    $.post('/search/searchResults', postVars, function(data) {
        $('#mapListings').html(data);
        search.lat = $('#avgLat').val();
        search.lng = $('#avgLng').val();
        gm.clearAllPoints();
        gm.clearAllMarkers();
        gm.clearData();
        search.loadListingMarkers();
        gm.loadSavedMarkers(false, false, false, '#savedMarkers');
        gm.reCenter(search.lat, search.lng);
        $('#searchOverlay').fadeOut('slow');
    }, 'html');
};

search.setWellWidth = function ()
{
    // gets container width

    var cwidth = $('#content-container').width();

    //console.log("Container Width: " + cwidth);

    if (cwidth <= 768)
    {
        $('#mapWell .well').css('width', '100%');
    }
    else
    {
        var col8Width = $('.col-md-7').width();
        //console.log("Col-8 Width: " + col8Width);

        //var width = $('#mapWell .well').width();
        var width = cwidth - col8Width;

        $('#mapWell .well').css('width', width);
    }


}

search.bindWindowScroll = function ()
{
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            $(window).unbind('scroll');
            //console.log("near bottom!");
            search.loadMoreListings();

        }
    });
}

function callUploadsPage(){
    global.ajaxLoader('#ajaxSwap');
    $.get('/search/uploads_content/' + $('#locationid').val(), {}, function (data) {
        $('#ajaxSwap').fadeOut('slow', function () {
            $(this).html(data);
            $(this).fadeIn('slow', function () {
                $('#top-left-nav').css('top', (($('#carousel').height() / 2) - 37) + 'px');
                $('#top-right-nav').css('top', '-' + (($('#carousel').height() / 2) + 37) + 'px');
                $('#bottom-left-nav').css('top', (($('#carousel2').height() / 2) - 37) + 'px');
                $('#bottom-right-nav').css('top', '-' + (($('#carousel2').height() / 2) + 37) + 'px');
            });
            $('#carousel').owlCarousel({
                'items': 2,
                'pagination': false
            });
            $('#carousel2').owlCarousel({
                'items': 3,
                'pagination': true,
                'autoPlay' : 5000
            });
            $('#carousel3').owlCarousel({
                'items': 2,
                'pagination': true
            });
            $('#carousel4').owlCarousel({
                'items': 2,
                'pagination': true
            });
            $('#top-left-nav').click(function () {
                $('#carousel').trigger('owl.prev');
            });
            $('#top-right-nav').click(function () {
                $('#carousel').trigger('owl.next');
            });
            $('#bottom-left-nav').click(function () {
                $('#carousel2').trigger('owl.prev');
            });
            $('#bottom-right-nav').click(function () {
                $('#carousel2').trigger('owl.next');
            });
        });
    });
}

search.viewlisting = function (id)
{
    window.location = '/search/info/' + id + '?lat=' + $('#lat').val() + '&lng=' + $('#lng').val() + '&q=' + escape($('#q').val()) + '&location=' + escape($('#location').val());
}

search.infoInit = function (lat, lng)
{
    var src = $('#display-img-src').val();

    //$('#displayImg').backstretch(src);
    search.lat = lat;
    search.lng = lng;
    if ($('#previewMap').exists())
    {
        if (lat == 0)
            lat = undefined;
        if (lng == 0)
            lng = undefined;

        gm.initialize('previewMap', lat, lng, 8, false);
        gm.loadSavedMarkers(false, false, false, '#savedMarkers', false, false);
    }
    $('a#uploadsTabButton').on('click', function (e) {
        e.preventDefault();
        $('.user_list ul li a').removeClass('active');
        $(this).addClass('active');
        var hash = $(this).attr('href');
        history.pushState(null, null, hash);
        callUploadsPage();
    });

    if ($('#googleRefreshBtn').exists())
    {
        $('#googleRefreshBtn').click(function (e) {
            search.manualUpdateFromGoogle();
        });
    }

    if ($('#claimBtn').exists())
    {
        $('#claimBtn').click(function (e) {
            $('#claimModal').modal('show');
        });
    }

    $('#submitClaim').click(function (e) {
        search.checkClaimForm();
    });

    search.renderLocationImgs();

    $(window).resize(function () {
        search.renderLocationImgs();
    });
    
    $('#followBtn').on('click', function(e) {
        e.preventDefault();
        if ($(this).text().trim() == 'follow')
        {
            locations.followLocation();
        }
        else
        {
            locations.unfollowLocation();
        }
    });
};

search.renderLocationImgs = function ($node)
{
    var rowHeight = 300;

    if ($(window).width() <= 768)
        rowHeight = 200
    if ($(window).width() <= 320)
        rowHeight = 100

    // no element to render images
    if ($('#location-photo-container') == undefined && $node == undefined)
        return false;

    if ($node == undefined)
        $node = $('#location-photo-container');
    console.log(rowHeight);
    $node.justifiedGallery({
        rowHeight: rowHeight,
        sizeRangeSuffixes: {
            'lt100': '',
            'lt240': '',
            'lt320': '',
            'lt500': '',
            'lt640': '',
            'lt1024': ''
        },
        margins: 15
    });

}

search.checkReview = function ()
{
    if ($('#reviewName').exists())
    {
        if ($('#reviewName').val() == '')
        {
            global.renderAlert('Please enter your name!', undefined, 'reviewAlert');
            $('#reviewName').focus();
            $('#reviewName').effect('highlight');
            return false;
        }
    }

    if ($('#reviewEmail').exists())
    {
        if ($('#reviewEmail').val() == '')
        {
            global.renderAlert('Please enter your e-mail address!', undefined, 'reviewAlert');
            $('#reviewEmail').focus();
            $('#reviewEmail').effect('highlight');
            return false;
        }
    }

    if (parseInt($('#rating').val()) == 0)
    {
        global.renderAlert('Please select a rating', undefined, 'reviewAlert');
        $('#reviewStars').effect('highlight');
        return false;
    }


    if ($('#reviewDesc').val() == '')
    {
        global.renderAlert('Please enter a review.', undefined, 'reviewAlert');
        $('#reviewDesc').focus();
        $('#reviewDesc').effect('highlight');
        return false;
    }

    var disabledFields = $('#reviewForm').find(':input:disabled').removeAttr('disabled');

    $('#reviewBtn').attr('disabled', 'disabled');

    $.post("/search/savereview", $('#reviewForm').serialize(), function (data) {

        if (data.status == 'SUCCESS')
        {
            if (data.reviewHTML === undefined)
            {
                window.location = '/search/info/' + $('#location').val() + '?site-success=' + escape(data.msg);
                location.reload();
            }
            else
            {
                $(data.reviewHTML)
                    .css('display', 'none')
                    .appendTo('#reviewDiv')
                    .fadeIn('slow');
                $('#reviewBtn').removeAttr('disabled');
                disabledFields.attr('disabled', 'disabled');
            }
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger', 'reviewAlert');
            $('#reviewBtn').removeAttr('disabled');
            disabledFields.attr('disabled', 'disabled');
        }
    }, 'json');

}

search.manualUpdateFromGoogle = function (b)
{
    if (!confirm("Are you sure you wish to update this location from Google Places?"))
    {
        return false;
    }

    $('#googleRefreshBtn i').addClass('fa-spin');
    $('#googleRefreshBtn').attr('disabled', 'disabled');

    $.getJSON('/search/upatefromgoogle/' + $('#id').val(), function (data) {
        if (data.status == 'SUCCESS')
        {
            global.renderAlert(data.msg, 'alert-success');

            setTimeout(function () {
                location.reload();
            }, 2000);

            //$(location).attr('href') + '&site-success=' + data.msg;
        }
        else
        {
            global.renderAlert(data.msg, 'alert-danger');

            // resets button to default state
            $('#googleRefreshBtn i').removeClass('fa-spin');
            $('#googleRefreshBtn').removeAttr('disabled');
        }
    });
}

search.setRating = function (rating)
{

    if (rating == undefined)
        rating = $('#rating').val();

    for (var i = 1; i <= 5; i++)
    {

        if (i > rating)
        {
            $('#rating_star_' + i).removeClass('setRating');
        }
        else
        {
            $('#rating_star_' + i).addClass('setRating');
        }
    }
    ;

}

search.loadListingMarkers = function ()
{
    $('#listContent').find('input').each(function (index, item) {
        if ($(item).attr('type') == 'hidden' && $(item).attr('class') == 'gmMarker' && $(item).attr('loaded') == '0')
        {
            //console.log(index)
            // loads point
            gm.addPoint(new google.maps.LatLng($(item).attr('lat'), $(item).attr('lng')), false, $(item).attr('radius'), false, false, $(item).attr('color'), $(item).attr('opacity'), $(item).attr('title'), $(item).attr('contentString'), true, $(item).attr('rank'), $(item).attr('doctor'));

            // sets loaded to true
            $(item).attr('loaded', '1');
            //next_page_token = $(item).val();
            //console.log($(item).val());
        }
    });

    //gm.autoCenter();
}

search.loadMoreListings = function ()
{
    var next_page_token;
    // show loading panel
    $('#loadingPanel').show('highlight');

    $('#listContent').find('input').each(function (index, item) {
        if ($(item).attr('type') == 'hidden' && $(item).attr('id') == 'next_page_token')
        {
            next_page_token = $(item).val();
            //console.log($(item).val());
        }
    });

    //$.get('/dojos/listings/'+ $('#tail').val() + '?lat=' + $('#lat').val() + '&lng=' + $('#lng').val() + '&q=' + escape($('#q').val()), function(data){
    /*$.get('/search/listings/?next_page_token=' + escape(next_page_token) + '&lat=' + $('#lat').val() + '&lng=' + $('#lng').val(), function (data) {
     $('#listContent').append(data);
     
     // calculates last hidden ID
     //dojos.getTailID();
     
     // hide loading panel
     $('#loadingPanel').hide('highlight');
     
     // check if there is a next_page_token, if so re-enable scroll functionality
     $('#listContent').find('input').each(function (index, item) {
     if ($(item).attr('type') == 'hidden' && $(item).attr('id') == 'next_page_token')
     {
     next_page_token = $(item).val();
     //console.log($(item).val());
     }
     });
     
     if (next_page_token !== '')
     {
     search.bindWindowScroll();
     }
     
     search.loadListingMarkers();
     });*/
};


search.deactivate = function (b, location)
{
    if (confirm("Are you sure wish to deactive this listing?"))
    {

        $(b).attr('disabled', 'disabled');

        $.post('/search/deactiveLocation', {location: location, sportsToken: global.CSRF_hash}, function (data) {
            if (data.status == 'SUCCESS')
            {
                global.renderAlert(data.msg, 'alert-success');

                // hide HTML element

                //alert($(b).parent().parent().parent().html());

                $(b).parent().parent().parent().addClass('animated fadeOut');

                $(b).parent().parent().parent().one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e)
                {
                    $(this).removeAttr('class'); // clears all classes
                    $(this).empty();
                });
            }
            else
            {
                $(b).removeAttr('disabled');
                global.renderAlert(data.msg, 'alert-danger');
            }
        }, 'json');
    }
}

search.checkClaimForm = function()
{
    if ($('#claim_name').val() == '')
    {
        global.renderAlert('Please enter your full name!', undefined, 'claimAlert');
        $('#claim_name').focus();
        $('#claim_name').effect('highlight');
        $('#claim_name').attr("placeholder", "Please enter your name");
        return false;
    }

    if ($('#claim_phone').val() == '')
    {
        global.renderAlert('Please enter a contact number!', undefined, 'claimAlert');
        $('#claim_phone').focus();
        $('#claim_phone').effect('highlight');
        $('#claim_phone').attr("placeholder", "Please enter your phone number");
        return false;
    }

    if ($('#claim_position').val() == '')
    {
        global.renderAlert('Please enter your position in the business!', undefined, 'claimAlert');
        $('#claim_position').focus();
        $('#claim_position').effect('highlight');
        $('#claim_position').attr("placeholder", "Please enter your position");
        return false;
    }

    $('#submitClaim').attr('disabled', 'disabled');

    var name = $('#claim_name').val();
    var phone = $('#claim_phone').val();
    var position = $('#claim_position').val();
    var comment = $('#claim_comment').val();
    var token = $('#karateToken').val();
    var id = $('#id').val();

        $.post("/search/saveclaim", {
            name: name,
            phone: phone,
            position: position,
            comments: comment,
            karateToken: token,
            id: id
        }, function (data) {
            if (data.status == 'SUCCESS') {
                $('#claimLocation').modal('hide');
                global.renderAlert(data.msg, 'alert-success');
                $('#claimForm')[0].reset();
            }
            else {
                global.renderAlert(data.msg, 'alert-danger', 'claimAlert');
            }

            $('#submitClaim').removeAttr('disabled');
        }, 'json');

    return false;
}

search.writeReview = function ()
{
    // goes to first tab
    $('#infoTabs li:eq(0) a').tab('show');
    window.location = '#write_review';
};

search.photoAdjust = function (size)
{
    if (size == undefined)
        size = 235;
    $('#photos').find('img').each(function (index, item) {
        $(item).attr('height', size);
        $(item).attr('width', size);
    });
};

search.loadGettingStartedModal = function ()
{
    $('#getStartedModal').modal('show');
};

gm.bindListings = function ()
{
    $('.map_addr').unbind('click').on('click', function(e) {
        var id = $(this).attr('id').split('_')[1];
        gm.clearAllMarkers();
        gm.map.setCenter(gm.markers[id].getPosition());
        gm.markers[id].infowindow.open(gm.map, gm.markers[id]);
    });
};

$(document).ready(function () {
    $('.user_list ul li a, .user_icon_list ul li a').not('#uploadsTabButton, #sendMessage').on('click', function (e) {
        e.preventDefault();
        $('.user_list ul li a, .user_icon_list ul li a').removeClass('active');
        $(this).addClass('active');
        console.log($(this).attr('id'));
        var url = $(this).attr('rel');
        var hash = $(this).attr('href');
        history.pushState(null, null, hash);
        doAjax(url);
    });

    function doAjax(url) {
        global.ajaxLoader('#ajaxSwap');
        $.get(url, {}, function (data) {
            $('#ajaxSwap').fadeOut('slow', function () {
                $(this).html(data);
                if ($('#reviewBtn').exists())
                {
                    $('#reviewBtn').click(function (e) {
                        search.checkReview();
                    });
                    $('#reviewStars i').hover(function (e) {
                        search.setRating($(this).attr('value'));
                    });
                    $('#reviewStars').mouseout(function () {
                        search.setRating();
                    });
                    $('#reviewStars i').click(function (e) {
                        var rating = parseInt($(this).attr('value'));
                        $('#rating').val(rating);
                        for (var i = 1; i <= 5; i++)
                        {
                            if (i > $(this).attr('value'))
                            {
                                $('#rating_star_' + i).removeClass('setRating');
                            }
                            else
                            {
                                $('#rating_star_' + i).addClass('setRating');
                            }
                        }
                    });
                    $('#reviewForm').on('submit', function (e) {
                        e.preventDefault();
                        search.checkReview();
                        $('#reviewDesc').val('');
                    });
                }
                $('#reviewStars').mouseout(function () {
                    search.setRating();
                });
                $('#reviewStars i').click(function (e) {
                    var rating = parseInt($(this).attr('value'));
                    $('#rating').val(rating);
                    for (var i = 1; i <= 5; i++)
                    {
                        if (i > $(this).attr('value'))
                        {
                            $('#rating_star_' + i).removeClass('setRating');
                        }
                        else
                        {
                            $('#rating_star_' + i).addClass('setRating');
                        }
                    }
                    ;
                });
                $(this).fadeIn('slow', function () {
                    $('.right-nav-btn-uploads').css('top', (($(this).parent().parent().height() / 2) - 37) + 'px');
                    $('.left-nav-btn-uploads').css('top', '-' + (($(this).parent().parent().height() / 2) + 37) + 'px');
                    //$('#bottom-left-nav').css('top', (($('#carousel2').height() / 2) - 37) + 'px');
                    //$('#bottom-right-nav').css('top', '-' + (($('#carousel2').height() / 2) + 37) + 'px');
                    if ($('#previewMap').exists())
                    {
                        gm.initialize('previewMap', search.lat, search.lng, 8, false);
                        gm.loadSavedMarkers(false, false, false, '#savedMarkers', false);
                    }
                });
                $('#carousel6').owlCarousel({
                    'items': 3,
                    'pagination': false
                });
                $('#carousel7').owlCarousel({
                    'items': 3,
                    'pagination': false
                });
                $('#carousel8').owlCarousel({
                    'items': 3,
                    'pagination': false
                });
            });
        });
    }

    var url_hash = window.location.hash;

    if(url_hash){

        $('.user_list ul li a').removeClass('active');
        $('.user_icon_list ul li a').removeClass('active');

        $('.user_list ul li a, .user_icon_list ul li a').each(function() {
            if($(this).data('link-command') == url_hash) {

                $(this).addClass('active');
                var url = $(this).attr('rel');

                if(url_hash != '#uploads') {
                    doAjax(url);
                } else {

                    callUploadsPage();

                }
            }
        });
    }
    if ($('.banner_logo').exists())
    {
        $('.banner_logo').sizeandcrop({
            width:330,
            height:325,
            vertical:"top",
            zoom: true
        });
    }
});



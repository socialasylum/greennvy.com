var dashboard = {
    'postImages': new Array()
};

$(document).ready(function() {
    $('#share_status').keydown(function(e) {
        if (e.keyCode === 13)
        {
            dashboard.submitPost();
        }
    });
    $('div.share')
        .bind('dragenter', dashboard.dragOver)
        .bind('dragover', dashboard.ignoreDrag)
        .bind('drop', dashboard.drop);
    $('#imageAdd').on('click', function (data) {
        $('#imageManual').click();
    });
    $('#submitPost').on('click', function(e) {
        e.preventDefault();
        dashboard.submitPost();
    });
    $('#imageManual').on('change', function (e) {
        dashboard.basicFileUpload(e.target.files[0]);
    });

    var res = $("#carouselDashboardBottom").width();

    var itemAmount;

    if(res >= 1500){
        itemAmmount = 5;
    } else if (res >= 750) {
        itemAmmount = 4;
    } else {
        itemAmmount = 3;
    }

    $('#carouselDashboard').owlCarousel({
        'items': itemAmmount,
        'pagination': true,
        'autoPlay' : 5000
    });

});

dashboard.submitPost = function ()
{
    var postVars = {
        'userid': $('#userid').val(),
        'post': $('#share_status').val(),
        'images': dashboard.postImages,
        'parentPost': 0,
        'karateToken': global.CSRF_hash
    };
    $.post('/wall/savepost', postVars, function(data) {
        $('#share_status').val('');
        dashboard.postImages = new Array();
        $('.dashboardImg').remove();
        $('#activityPost').after('<span class="new_post">' + data.html + '</span>');
        $(".new_post").find('.recnt_box').css('width', '80%');
        $(".new_post").find('img').css('margin-left', '15px');
        $(".new_post").find('.pull-right').remove();
        $('#comment_area').remove();
        $('#like_').remove();
        $('#like_string_').remove();
    }, 'json');
};

dashboard.setProgressBar = function ()
{
    var phtml = $('#fileUploadProgressHtml').html();
    $('#uploadProgressContainer').append(phtml).effect('highlight');
    return $('#uploadProgressContainer .fileProgressBar').last();
};

dashboard.hideProgressbar = function (progressbar)
{
    window.setTimeout(function () {
        progressbar.hide('highlight');
    }, 2000);
};

dashboard.ignoreDrag = function (e)
{
    e.originalEvent.stopPropagation();
    e.originalEvent.preventDefault();
};

dashboard.dragOver = function (e)
{
    dashboard.ignoreDrag(e);
    $('#fileUploadContainer').addClass('highlight');
};

dashboard.dragOut = function (e)
{
    dashboard.ignoreDrag(e);
};

dashboard.drop = function (e)
{
    dashboard.ignoreDrag(e);
    var dt = e.originalEvent.dataTransfer;
    var files = dt.files;
    dashboard.handFileUpload(files, $('#fileUploadContainer'));
};

dashboard.handFileUpload = function (files, obj)
{
    for (var i = 0; i < files.length; i++)
    {
        var fd = new FormData();
        fd.append('file', files[i]);
        fd.append('userid', $('#userid').val());
        fd.append('karateToken', global.CSRF_hash);
        var progressbar = dashboard.setProgressBar();
        $(progressbar).find('.fileNameTxt').text(files[i].name);
        dashboard.sendFileToServer(fd, progressbar);
    }
    $('#postBtn').removeAttr('disabled');
    $('#fileUploadContainer').removeClass('highlight');
};

dashboard.basicFileUpload = function (file)
{
    var fd = new FormData();
    fd.append('file', file);
    fd.append('userid', $('#userid').val());
    fd.append('karateToken', global.CSRF_hash);
    var progressbar = dashboard.setProgressBar();
    $(progressbar).find('.fileNameTxt').text(file.name);
    dashboard.sendFileToServer(fd, progressbar);
    $('#fileUploadContainer').removeClass('highlight');
};

dashboard.sendFileToServer = function (formData, progressbar)
{
    $.ajax({
        url: '/dashboard/uploadPostImg',
        progress: function (e) {
            //make sure we can compute the length
            if (e.lengthComputable)
            {
                //calculate the percentage loaded
                var pct = (e.loaded / e.total) * 100;

                var pctDisplay = $.number(pct);

                //log percentage loaded
                //console.log(' PERCENT: ' + pct);
                progressbar.find('.progress-bar').attr('aria-valuenow', pct);
                progressbar.find('.progress-bar').css('width', pct + '%');
                progressbar.find('.progress-bar').text(pctDisplay + '%');
                //console.log(per + '%');

                if (pct >= 100)
                {
                    dashboard.hideProgressbar(progressbar);
                }

            }
            //this usually happens when Content-Length isn't set
            else
            {
                console.warn('Content Length not reported!');
            }
        },
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        type: 'POST',
        success: function (data)
        {
            if (data.status == 'SUCCESS')
            {
                $('#activityPost').append('<div class="dashboardImg"><div class="close" onclick="dashboard.removePostImage(this);"><i class="fa fa-times clearfix"></i></div><img src="/public/uploads/userimgs/' + global.userid + '/' + data.msg + '" width="256" /></div>');
                dashboard.postImages.push(data.msg);              
            }
        }
    });
};

dashboard.removePostImage = function (obj)
{
    var outerContainer = $(obj).parent();
    var imageName = $(outerContainer).find('img').attr('src').split('/').pop();
    var index = dashboard.postImages.indexOf(imageName);
    dashboard.postImages.splice(index, 1);
    $(obj).parent().fadeOut('slow');
};
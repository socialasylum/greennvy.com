var base = window.location.hostname;

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 50; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}



function sendMessage() {
    var text = $("#message").val();
    var token = $("#karateToken").val();
    var cid = makeid();
    var to = $('#to').val();
    var co = $('#co').val();
    $.post('http://' + base + '/messages/sendchat', {
        message: text,
        karateToken: token,
        cid: cid,
        reciever: to,
        co: co
    }, function (result) {
        var returnData = jQuery.parseJSON(result);
        if (returnData.result == 'success') {
            $('#messageModal').modal('hide');
            $('#message').val('');
        } else {
            $('#messageAlert').html("There was an error sending your message.");
        }
    });
}

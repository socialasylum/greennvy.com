##Velocity

**Docs**  
http://VelocityJS.org

**Bower**  
Package name is *velocity*.

**NPM**  
Package name is *velocity-animate*.

**Quickstart**  
`<script src="//cdn.jsdelivr.net/jquery.velocity/0.0.22/jquery.velocity.min.js"></script>`

###**Updates**

- *Coming soon: jQuery dependency removal and Zepto support.*
- Velocity was awarded Stripe's Open Source grant: https://stripe.com/blog/open-source-retreat-grantees
- UI Pack: http://velocityjs.org/#uiPack
- Spring physics: http://velocityjs.org/#easing
- Progress monitoring: http://velocityjs.org/#progress
- Syntactic sugar: https://github.com/julianshapiro/velocity/issues/76

###**Resources**

- **Speed:** http://davidwalsh.name/css-js-animation

- **Codecast:** https://www.youtube.com/watch?v=MDLiVB6g2NY&hd=1

- **Workflow:** http://css-tricks.com/improving-ui-animation-workflow-velocity-js

- **Stack Overflow (support):** http://stackoverflow.com/questions/tagged/velocity.js

###**Comparisons**

- **Famo.us** is a full-fledged *mobile app framework* built around a physics engine.

- **GSAP** is a fast, multi-purpose *animation platform*. It inspired me to pursue the development of Velocity.

- **Velocity** is a very fast and lightweight tool for dramatically improving *UI animation performance and workflow*.

====

[MIT License](LICENSE). © Julian Shapiro (http://twitter.com/shapiro).

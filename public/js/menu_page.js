/*
 * This JS is a template for form fields and autoComplete data for text boxes.
 */

$("#productSearchText").autocomplete({
    source: autoCompleteData
}).autocomplete("widget").addClass("fixed-height");

/*
 * This will remove or show a named div based on what is input in a text field. This template should be used for a single item.
 */
$('#productSearchText').keyup(function() {

	//set the vars
    var value = $(this).val();
    var exp = new RegExp('^' + value, 'i');
	
	//if there is another filter field (in this case drop down) clear it
	$('#productSearchType').val(0);

    $('.product_list_item .menu_border_top').each(function() {
       if(exp.test($('.menu_title', this).text())) {
            $(this).slideDown('slow');
        } else {
			if($(this).is(":visible")){
				$(this).slideUp('slow');
			}
        }
    });
});


/*
 * If a item is clicked during auto complete this will hide the items that are not needed
 */

$('.ui-corner-all').click(function(){

    var value = $('#productSearchText').val();

    //if there is another filter field (in this case drop down) clear it
    $('#productSearchType').val(0);

    $('.product_list_item .menu_border_top').each(function() {
        if($('.menu_title', this).text() == value) {
            $(this).slideDown('slow');
        } else {
            if($(this).is(":visible")){
                $(this).slideUp('slow');
            }
        }
    });

});

/*
 * This will remove or show a named div based on a drop down box. This template should be used for a single item.
 */
$('#productSearchType').change(function() {

	//set the vars
	var value = $(this).val();
	
	//if there is another filter field (in case text) clear it
	$('#productSearchText').val('');
	
	$('.product_list_item .menu_border_top').each(function() {
		if($(this).attr('data-item-type') == value){
			$(this).slideDown('slow');
		} else if(value == "0") {
			$(this).slideDown('slow');
		} else {
			$(this).slideUp('slow');
		}
	});
});

/*
 * Standard Form Reset Button for div fields
 */
$('#resetSearch').click(function(event){
	event.preventDefault();
	$('#productSearchText').val('');
	$('#productSearchType').val(0);
	
	$('.product_list_item .menu_border_top').each(function() {
		$(this).slideDown('slow');
	});
});

//variables for ascending and decending order
var name_ascending = false;
var gram_ascending = false;
var eighth_ascending = false;
var quarter_ascending = false;
var half_ascending = false;
var oz_ascending = false;
var each_ascending = false;

//sorts lists divs on name
$('.menu_name').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (name_ascending ==
        convertToLower($(a).find('.menu_title').html()) < convertToLower($(b).find('.menu_title').html())
        ) ? 1 : -1;
    });
    name_ascending  = name_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});

//sorts asending or decending for the price of an gram
$('.g_headline').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (gram_ascending ==
        (convertToNumber($(a).find('.g_price').html()) <
        convertToNumber($(b).find('.g_price').html()))) ? 1 : -1;
    });
    gram_ascending  = gram_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});


//sorts asending or decending for the price of an eighth
$('.eighth_headline').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (eighth_ascending ==
        (convertToNumber($(a).find('.eighth_price').html()) <
        convertToNumber($(b).find('.eighth_price').html()))) ? 1 : -1;
    });
    eighth_ascending  = eighth_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});

//sorts asending or decending for the price of an quarter
$('.quarter_headline').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (quarter_ascending ==
        (convertToNumber($(a).find('.quarter_price').html()) <
        convertToNumber($(b).find('.quarter_price').html()))) ? 1 : -1;
    });
    quarter_ascending  = quarter_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});

//sorts asending or decending for the price of an half
$('.half_headline').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (half_ascending ==
        (convertToNumber($(a).find('.half_price').html()) <
        convertToNumber($(b).find('.half_price').html()))) ? 1 : -1;
    });
    half_ascending  = half_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});

//sorts asending or decending for the price of an half
$('.oz_headline').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (oz_ascending ==
        (convertToNumber($(a).find('.oz_price').html()) <
        convertToNumber($(b).find('.oz_price').html()))) ? 1 : -1;
    });
    oz_ascending  = oz_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});

//sorts asending or decending for the price of each
$('.each_headline').on('click', function(){

    var sorted = $('.menu_items').sort(function(a,b){
        return (each_ascending ==
        (convertToNumber($(a).find('.each_price').html()) <
        convertToNumber($(b).find('.each_price').html()))) ? 1 : -1;
    });
    each_ascending  = each_ascending  ? false : true;

    $('.product_list_item').html(sorted);
});


//strips the $ sign out of the html data when sorting my price
var convertToNumber = function(value){
    if (value == 'n/a'){
        value = '0';
    }
    return parseFloat(value.replace('$',''));
}

//converts text to lower case for testing
var convertToLower = function(value){
    return value.toLocaleLowerCase();
}
 
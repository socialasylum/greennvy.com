$(document).ready(function(){
	
	$('.featured').on('click', function(e){ 
		e.preventDefault();
		var dealid = e.currentTarget.id;
		
		var request = $.ajax({
			url: "/deals/setfeatured/"+dealid,
			type: "GET",
			//data: { dealid : dealid },
			dataType: "html"
		});
			 
		request.done(function( msg ) {
			console.log( msg );
			$('.featured').css('color', '#000');
			$('#'+dealid).css('color', 'gold');
		});
			 
		request.fail(function( jqXHR, textStatus ) {
			console.log( "Request failed: " + textStatus );
		});
	});
	
	$('.check-all').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
	
	$('.edit').on('click', function(e){
		e.preventDefault();
		
		var link = e.currentTarget.href;
		$(function ()    {
		    $('<div>').dialog({
		        modal: true,
		        open: function ()
		        {
		            $(this).load(link);
		        },         
		        height: 600,
		        width: 400,
		        //title: 'Edit Deals',
		        show: { effect: "slideDown", duration: 800 }
		        
		    });
		});
		
	});
	
	$('.addw').on('click', function(e){
		e.preventDefault();
		
		var link = e.currentTarget.href;
		$(function ()    {
		    $('<div>').dialog({
		        modal: true,
		        open: function ()
		        {
		            $(this).load(link);
		        },         
		        height: 600,
		        width: 400,
		        //title: 'Add A New Deal',
		        show: { effect: "slideDown", duration: 800 }
		        
		    });
		});
		
	});
    $('#carousel6').owlCarousel({
        'items': 4,
        'pagination': true,
        'autoPlay' : 5000
    });

    $('#carousel3').owlCarousel({
        'items': 4,
        'pagination': true,
        'autoPlay' : 5000
    });

});


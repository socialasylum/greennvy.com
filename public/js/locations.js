var locations = {};

locations.followLocation = function()
{
    $.post('/locations/followLocation/' + $('#locationid').val(), { 'karateToken': global.CSRF_hash }, function(data) {
        $('#followBtn').text('unfollow');
        $('#followBtn').unbind('click').on('click', function(e) {
            e.preventDefault();
            locations.unfollowLocation();
        });
    });
};

locations.unfollowLocation = function()
{
    $.post('/locations/unfollowLocation/' + $('#locationid').val(), { 'karateToken': global.CSRF_hash }, function(data) {
        $('#followBtn').text('follow');
        $('#followBtn').unbind('click').on('click', function(e) {
            e.preventDefault();
            locations.followLocation();
        });
    });
};